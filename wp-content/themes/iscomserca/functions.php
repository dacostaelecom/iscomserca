<?php
function wp_hide_update() {
        global $current_user;
        //get_currentuserinfo();
        wp_get_current_user();
 
        if ($current_user->ID != 1) { // solo el admin lo ve, cambia el ID de usuario si no es el 1 o añade todso los IDs de admin
            remove_action( 'admin_notices', 'update_nag', 3 );
        }
}
add_action('admin_menu','wp_hide_update');


function incluir_jquery() 
{
	wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'incluir_jquery');

function iscomserca_add_caps(){
    // Analista de Sistema
    $role = get_role('administrator');
    $role->add_cap('manage_iscomserca');
    
    // Coordinador
    $role = get_role('editor');
    $role->add_cap('edit_iscomserca');
    
    // Tecnico
    $role = get_role('author');
    $role->add_cap('auth_iscomserca');
    
    // Coordinador
    $role = get_role('contributor');
    $role->add_cap('cola_iscomserca');
}
add_action('admin_init', 'iscomserca_add_caps');

// Eliminar elementos del menú principal
function remove_menus(){
 	global $current_user;
        
        if($current_user->ID !== 1){
	 	remove_menu_page( 'index.php' );                  //Dashboard
	   	remove_menu_page( 'jetpack' );                    //Jetpack* 
	   	remove_menu_page( 'edit.php' );                   //Posts
	   	remove_menu_page( 'upload.php' );                 //Media
	   	remove_menu_page( 'edit.php?post_type=page' );    //Pages
	   	remove_menu_page( 'edit-comments.php' );          //Comments
	   	remove_menu_page( 'themes.php' );                 //Appearance
	   	remove_menu_page( 'plugins.php' );                //Plugins
	   	remove_menu_page( 'users.php' );                  //Users
	   	remove_menu_page( 'tools.php' );                  //Tools
	   	remove_menu_page( 'options-general.php' );        //Settings
	}
   	
  }
  add_action( 'admin_menu', 'remove_menus' );

  // Eliminar todos los enlaces de la barra de administración
function remove_admin_bar_links() {
    global $wp_admin_bar, $current_user;

    if($current_user->ID !== 1){
	    $wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
	    $wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
	    $wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
	    $wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
	    $wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
	    $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
	    $wp_admin_bar->remove_menu('site-name');        // Remove the site name menu
	    $wp_admin_bar->remove_menu('view-site');        // Remove the view site link
	    $wp_admin_bar->remove_menu('updates');          // Remove the updates link
	    $wp_admin_bar->remove_menu('comments');         // Remove the comments link
	    $wp_admin_bar->remove_menu('new-content');      // Remove the content link
	    //$wp_admin_bar->remove_menu('my-account');       // Remove the user details tab
    }
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

// Agrega una imagen al logo
function my_login_logo() { ?>
  <style type="text/css">
    #login h1 a, .login h1 a {
    background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/cropped-ICS-3-1-180x180.png);
      height: 84px;
      width: 84px;
      background-size: cover;
      background-repeat: no-repeat;
    }
  </style>
<?php }//end my_login_logo()
add_action( 'login_enqueue_scripts', 'my_login_logo' );
// Incluir Bootstrap CSS
function bootstrap_css() {
	//wp_enqueue_style( 'bootstrap_css', get_stylesheet_directory_uri() . '/css/bootstrap.min.css', array(), '4.3.1'); 
        wp_register_style('bootstrap_css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '4.3.1');
        wp_enqueue_style( 'bootstrap_css');
}
add_action( 'wp_enqueue_scripts', 'bootstrap_css');

// Funcion que redirecciona a la pagina principal
function my_login_logo_url() {
  return home_url();
}//end my_login_logo_url()
add_filter( 'login_headerurl', 'my_login_logo_url' );
function my_login_logo_url_title() {
  return 'Iscomserca';
}//end my_login_logo_url_title()
add_filter( 'login_headertext', 'my_login_logo_url_title' );

add_action('phpmailer_init','send_smtp_email');
function send_smtp_email( $mailer )
{
  if(!is_object($mailer)){
      $mailer = (object) $mailer;
  }
  $mailer->isSMTP();
  $mailer->SMTPDebug = 0;
  $mailer->SMTPAuth = true;
  $mailer->Host = "smtp.gmail.com"; // your SMTP server
  $mailer->Port = "587";
  //$mailer->SMTPDebug = 2; // write 0 if you don't want to see client/server communication in page
  $mailer->CharSet  = "utf-8";
  $mailer->Username = "dacostaelecom@gmail.com";
  $mailer->Password = "15287442230682&&";
  $mailer->SMTPSecure = "tls";
  $mailer->isHTML(true);
}

add_action('wp_mail_failed', 'log_mailer_errors', 10, 1);
function log_mailer_errors( $wp_error ){
  $fn = ABSPATH . '/mail.log'; // say you've got a mail.log file in your server root
  $fp = fopen($fn, 'a');
  fputs($fp, "Mailer Error: " . $wp_error->get_error_message() ."\n");
  fclose($fp);
}

/*if ( !current_user_can( 'edit_users' ) ) {
  add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
  add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
}*/

function custom_colors() {
   echo '<style type="text/css">#wphead{background:#069}</style>';
}

add_action('admin_head', 'custom_colors');
