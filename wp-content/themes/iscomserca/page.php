<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package edsBootstrap
 */

get_header(); ?>

<!-- Section: Page Header -->
<section class="section-page-header page">
    <div class="container-fluid">
<?php if (!is_front_page()): ?>
        <div class="row">
            <!-- Page Title -->
            <div class="col-md-12">
                <?php the_title( '<h1 class="title">', '</h1>' ); ?>
                <?php if ( function_exists( 'the_subtitle' ) ) { ?>
                <div class="subtitle"><?php the_subtitle();?></div>
                <?php }?>
            </div>
            <!-- /Page Title -->

        </div>
<?php else: ?>
        <div class="row-fluid">
            <div class="col-md-12"><!-- embed-responsive embed-responsive-16by9">-->
    <!--<video autoplay class="embed-responsive-item">
        <source src="<?php echo get_stylesheet_directory_uri().'/vid/iscomserca-video.mp4'; ?>" type="video/mp4" />
    </video>-->

              <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="500">
                <!-- Indicators -->
                <!-- <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                  <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  <div class="item active">
                    <!--<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/iscom-00.png" alt="Chania" />
                  </div>

                  <div class="item">-->
                    <img src="http://iscomserca.com.ve/wp-content/uploads/2018/05/slider-copia.png<?php //echo get_stylesheet_directory_uri(); /img/iscom-01.png ?>" alt="Bienvenido" />
                  </div>

                  <!--<div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-02.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-03.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-04.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-05.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-06.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-07.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-08.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-09.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-10.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-11.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-12.png" alt="Chania" />
                  </div>

                  <div class="item">
                    <img src="<?php //echo get_stylesheet_directory_uri(); ?>/img/iscom-13.png" alt="Chania" />
                  </div>-->
                </div>
              </div>
            </div>
        </div>
<?php endif; ?>
    </div>
</section>
<!-- /Section: Page Header -->
<!-- Main -->
<main class="main-container">
    <div class="container">
    	<div class="row">
     	<?php if( get_theme_mod( 'edsbootstrap_show_page_sidebar','0') == 1 ):?>
		<div class="col-md-9">
        <?php else: ?>
		<div class="col-md-12">
		<?php endif;?>
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</div>
        <?php if( get_theme_mod( 'edsbootstrap_show_page_sidebar','0') == 1 ):?>
        <!-- Blog Sidebar -->
        <div class="col-md-3">
        	<?php echo get_sidebar();?>
        </div>
        <!-- Blog Sidebar -->
        <?php endif;?>
		</div>
    </div>
</main>
<!-- /Main -->
<?php

get_footer();
