<?php

/*
 * Shortcode to list categories, details attribute allows to show a subcategory list for each category
 */
/*add_action( 'init', 'my_add_shortcodes' );

function my_add_shortcodes()
{

}*/
// Formularo de Inicio de Sesión del Cliente
function iscomsercaLogin() 
{
    if (is_user_logged_in())
    {
        global $wpdb;
        $query = "SELECT guid FROM $wpdb->posts WHERE post_content LIKE ";
        $query .= "'[iscomserca-estado]' AND post_type = 'page'";
        $page = $wpdb->get_row($query, ARRAY_A);
        return '<a href="'.$page['guid'].'" target="_blank" style="padding:10px">Consultar tu equipo.</a>';
    }
    else
    {
        return wp_login_form(array('echo' => false ));
    }
}
add_shortcode('iscomserca-login', 'iscomsercaLogin');

// Formulario de Status de Equipo del Cliente
function iscomsercaEstado() 
{
    // Cliente inicio sesión
    if(is_user_logged_in())
    {
        $cu = wp_get_current_user();
        $id_user = $cu->ID;
        global $wpdb;
        $queryUltimoEquipo = "SELECT descripcion, serial, estado FROM ".$wpdb->prefix."icsc_equipo "
                . "WHERE cliente_id = %d ORDER BY id DESC";
        $ultimoEquipo = $wpdb->get_results($wpdb->prepare($queryUltimoEquipo,array($id_user)), ARRAY_A);
        
        if($ultimoEquipo){
            $estados = array(
                            1=>"Recibido",
                            2=>"Esperando repuestos",
                            3=>"En reparación",
                            4=>"Reparado",
                            5=>"Irreparable",
                            6=>"Para repuestos",
                            7=>"Reparado y entregado",
                            8=>"Entregado pero no reparado",
                            9=>"Perdido"
            );
            
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
            <div class = "container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row" style="text-align: center;">
                            <label style="font-size: 16px;">Tus Equipos</label>
                        </div>
                        <div class="row">
                            <button type="button" class="btn btn-primary btn-md btn-block" disabled="disabled">Enviar <span class="glyphicon glyphicon-envelope"></span></button>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="alert alert-info" role="alert">
                                <div class="row" style="text-align: center; font-size: 16px;"><label>Contactos</label></div>
                                <div class="row" style="padding: 3px;"><label id="conTel">Tel:</label></div>
                                <div class="row" style="padding: 3px;"><label id="conCel">Cel:</label></div>
                                <div class="row" style="padding: 3px;"><label id="conCor">Correo:'.$cu->user_email.'</label></div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="col-md-7">
                        <div class="panel-group" id="accordion">
                        <!-- Primer Accordion -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title" style="text-align:center;">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="text-decoration: none;">
                                        Ultimo Equipo
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" style="padding: 4px;">
                                <div class="panel-body">
                                    <div class="row">
                                        <label>Modelo:</label><span id="lblModelo">'.($ultimoEquipo[0]['descripcion']?$ultimoEquipo[0]['descripcion']:"").'</span>
                                    </div>
                                    <div class="row">
                                        <label>Serial:</label><span id="lblEstadoEquipo">'.($ultimoEquipo[0]['serial']?$ultimoEquipo[0]['serial']:"").'</span>
                                    </div>
                                    <div class="row">
                                        <label>Estado del Equipo:</label><span id="lblEstadoEquipo">'.$estados[$ultimoEquipo[0]['estado']].'</span>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Fin de Primer Accordion -->

                        <!-- Segundo Accordion -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title" style="text-align:center;">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="text-decoration: none;">
                                        Otro Equipo
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <label>Modelo:</label><span id="lblModelo">'.($ultimoEquipo[1]['descripcion']?$ultimoEquipo[1]['descripcion']:"").'</span>
                                    </div>
                                    <div class="row">
                                        <label>Serial:</label><span id="lblEstadoEquipo">'.($ultimoEquipo[1]['serial']?$ultimoEquipo[1]['serial']:"").'</span>
                                    </div>
                                    <div class="row">
                                        <label>Estado del Equipo:</label><span id="lblEstadoEquipo">'.$estados[$ultimoEquipo[1]['estado']].'</span>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- Fin de Segundo Accordion -->

                    </div>
                </div>
                    <!--<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                    Collapsible Group Item #3
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>-->
                </div>
            </div> <!-- end container -->
            <br/>
            <!-- Latest compiled and minified JavaScript -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
            <script>
                $(document).on("ready", function () {
                    //alert("Hola");
                    /*$.ajax({
                        url: \'\',
                        type: \'POST\',
                        dataType: \'json\',
                        data: {
                            clienteId: 
                        }
                    }).done(function (data) {
                        alert("Enviado")
                    });*/
                });
            </script>';
            return $html;
        }
        else{
            $html = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
                    <div class="container">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="alert alert-info" role="alert">
                                    <div class="row" style="font-size: 12px;padding: 3px;"><label>No posees equipos en la empresa.</label></div>
                                </div>
                            </div>
                        </div>
                    </div>    
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>';
            return $html;
        }
    }
    // Cliente no inicio sesión
    else
    {
        return wp_login_form(array('echo'=>false));
    }
    //return 'P&aacute;gina para el cliente.';
}
add_shortcode('iscomserca-estado', 'iscomsercaEstado');
?>
