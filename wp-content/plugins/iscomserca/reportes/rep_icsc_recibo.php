<?php
global $wp;
//$url_actual = home_url(add_query_arg(array(), $wp->request));
//echo dirname( __FILE__ );
//echo plugin_basename( dirname(__FILE__) );
//$ruta1 = getcwd();
$ruta1 = __DIR__;
//echo $ruta1;
$ruta2 = str_replace('\\','/', $ruta1);
$ruta2.='/lib/tcpdf/tcpdf.php';

$ruta3 = str_replace('\\', '/', $ruta1);
$ruta3 .= '/clases/conexion.php';
//echo $ruta2;
//exit(0);

require_once $ruta2;
require_once $ruta3;



class ImprimirRecibo{

public $codigo;

public function traerImpresionRecibo(){

// TRAEMOS INFORMACIÓN DEL EQUIPO
//$wpdb = new WP_Query();


$valorRecibo = date("Y").str_pad($this->codigo, 6, "0", STR_PAD_LEFT);

$cnx = Conexion::conectar();

$query = 
 "SELECT "
 . "CONCAT(n.meta_value, ' ', a.meta_value) AS nombre, "
 . "cd.meta_value AS cedula, "
 . "e.fecha_ingresado AS fecha_ingresado, "
 . "(SELECT CONCAT(tn.meta_value, ' ', ta.meta_value) "
 . "FROM wp_users cl "
 . "INNER JOIN wp_usermeta tn ON tn.user_id = cl.ID AND tn.meta_key LIKE 'first_name' "
 . "INNER JOIN wp_usermeta ta ON ta.user_id = cl.ID AND ta.meta_key LIKE 'last_name' "
 . "WHERE "
 . "cl.ID = e.tecnico_id) AS tecnico, "
 . "di.meta_value AS direccion, "
 . "ce.meta_value AS celular, "
 . "cl.user_email AS correo, "
 . "e.descripcion AS descripcion, "
 . "(SELECT m.nombre FROM wp_icsc_marca m WHERE m.id = e.marca_id) AS marca, "
 . "e.serial AS serial, "
 . "e.cargador AS cargador, "
 . "e.falla AS falla, "
 . "e.observacion AS observacion,"
 . "e.estado AS estado "
 . "FROM "
 . "wp_users cl "
 . "INNER JOIN wp_usermeta n ON n.user_id = cl.ID AND n.meta_key LIKE 'first_name' "
 . "INNER JOIN wp_usermeta a ON a.user_id = cl.ID AND a.meta_key LIKE 'last_name' "
 . "INNER JOIN wp_usermeta cd ON cd.user_id = cl.ID AND cd.meta_key LIKE 'cedula' "
 . "INNER JOIN wp_usermeta di ON di.user_id = cl.ID AND di.meta_key LIKE 'direccion' "
 . "INNER JOIN wp_usermeta ce ON ce.user_id = cl.ID AND ce.meta_key LIKE 'celular' "
 . "INNER JOIN wp_icsc_equipo e ON e.cliente_id = cl.ID "
 . "WHERE "
 . "e.id = :id";

$stmt = $cnx->prepare($query);
$stmt->bindParam(":id", $this->codigo, PDO::PARAM_INT);
$stmt->execute();
$cliente = $stmt->fetch();

/*$cliente = $wpdb->get_results($wpdb->prepare($query, array($this->codigo), ARRAY_A));*/
$fecha = substr($cliente["fecha_ingresado"],0,-8);
$posee_cargador = "No";
if($cliente["cargador"] === "1"){
$posee_cargador = "Sí";    
}

// PREPARAR LOS PARAMETROS PARA LA CREACIÓN DEL ARCHIVO
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->startPageGroup();
$pdf->AddPage();

// ---------------------------------------------------------
// BLOQUE 1
$bloque1 = <<<EOF
     <table>
		
		<tr>
			
			<td style="width:540px; height: 80px; text-align: center;"><br/><img src="../image/logo-recibo.jpg"></td>

		</tr>
        
                <tr>
                    <td style="background-color:white; width:540px">
				
				<div style="font-size:8px; text-align:right; text-align:center;">
					
					<b>Dirección:</b> Av. 3h entre calles 78 y 79 Edif. Ginebra Local # 4. <b>Teléfonos:</b> 0261-9966662/ 0424-6545244

				</div>

			</td>
                    
                </tr>
                <tr>
                    <td style="background-color:white; width:540px">
				
				<div style="font-size:8px; text-align:right; text-align:center;">
					
					<b>E-mail:</b> iscomserca01@gmail.com, iscomserca@gmail.com.

				</div>

			</td>
                    
                </tr>
                <tr>
                    <td style="background-color:white; width:270px">
				
				<div style="font-size:9px; text-align:right; text-align:center;">
					
					<br><b>TÉCNICO:</b> $cliente[tecnico]

				</div>

                    </td>
                    <td style="background-color:white; width:270px;">
				
				<div style="font-size:9px; text-align:right; text-align:center;">
					
					<br/><b>RECIBO:</b> Nº $valorRecibo

				</div>

                    </td>
                    
                </tr>
     </table>           
EOF;

$pdf->writeHTML($bloque1, false, false, false, false, '');

// ---------------------------------------------------------

$bloque2 = <<<EOF
        <table>
		
		<tr>
			
			<td style="width:540px"><img src="images/back.jpg"></td>
		
		</tr>

	</table>
        
        <table style="font-size:8px; padding:3px;" border="1">
	
		<tr>
		
			<td style="background-color:white; width:270px;">
                            <b>NOMBRE:</b> $cliente[nombre]
			</td>
                        
                        <td style="background-color:white; width:135px;">
                            <b>CARGADOR:</b> $posee_cargador
			</td>
                        
                        <td style="background-color:white; width:135px;">
                            <b>SERIAL:</b> $cliente[serial]
			</td>

		</tr>
        
                <tr>
		
			<td style="background-color:white; width:270px;">
                            <b>C.I.:</b> $cliente[cedula]
			</td>
                        
                        <td style="background-color:white; width:270px;">
                            <b>MARCA:</b> $cliente[marca]
			</td>
                        
		</tr>
        
                <tr>
		
			<td style="background-color:white; width:270px;">
                            <b>FECHA:</b> $fecha
			</td>
                        
                        <td style="background-color:white; width:270px;">
                            <b>FALLA:</b> $cliente[falla]
			</td>
                        
		</tr>
        
                <tr>
		
			<td style="background-color:white; width:270px;">
                            <b>DIRECCIÓN:</b> $cliente[direccion]
			</td>
                        
                        <td style="background-color:white; width:270px;">
                            <b>STATUS:</b> Recibido
			</td>
                        
		</tr>
        
                <tr>
		
                        <td style="background-color:white; width:270px;">
                            <b>TELÉFONO:</b> $cliente[celular]
			</td>
        
			<td style="background-color:white; width:270px;" rowspan="3">
                            <b>OBSERVACIÓN:</b> $cliente[observacion]
			</td>
                        
                </tr>
        
                <tr>
		
			<td style="background-color:white; width:270px;">
                            <b>CORREO:</b> $cliente[correo]
			</td>
                        
                </tr>
        
                <tr>
		
			<td style="background-color:white; width:270px;">
                            <b>EQUIPO:</b> $cliente[descripcion]
			</td>
                        
                </tr>
        
                <tr>
		
			<td style="background-color:white; width:540px; text-align: center;">
                            Acepto los Términos y condiciones establecidas en la Hoja de Recepción Dispuestos en las observaciones de la empresa<br/><br/>
                            FIRMA CLIENTE ___________________ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POR ISCOMSERCA ____________________
                            
			</td>
                        
                </tr>
                
	</table>
EOF;

$pdf->writeHTML($bloque2, false, false, false, false, '');

// ---------------------------------------------------------

$bloque3 = <<<EOF
        <table>
		
		<tr>
			
			<td style="width:540px"><img src="images/back.jpg"></td>
		
		</tr>

	</table>
        
        <table>
		
		<tr>
			
			<td style="width:540px; height: 80px; text-align: center;"><br/><img src="../image/footer-recibo.jpg"></td>

		</tr>
        
        </table>
        
EOF;

$pdf->writeHTML($bloque3, false, false, false, false, '');

// ---------------------------------------------------------

$bloque4 = <<<EOF
       <table>
		
		<tr>
			
			<td style="width:540px"><img src="images/back.jpg"></td>
		
		</tr>

	</table>
        
        <table>
		
		<tr>
			
			<td style="width:540px; height: 80px; text-align: center;"><br/><img src="../image/condiciones-recibo.jpg"></td>

		</tr>
        
        </table> 
EOF;

$pdf->writeHTML($bloque4, false, false, false, false, '');

$pdf->Output('recibo-'.$this->codigo.'.pdf', 'I');

}
}

$recibo = new ImprimirRecibo();
$recibo->codigo = $_GET["equipo"];
$recibo->traerImpresionRecibo();

/*$idEquipo = $_GET['equipo'];

$pdf = new TCPDF('P', 'cm', 'letter', true, true);

$pdf->AddPage();
$pdf->Write(8.0, 'Esto es una prueba id de equipo: '.$idEquipo);
$pdf->Output('doc.pdf');*/