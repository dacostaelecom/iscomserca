<?php


function load($params){
$imprimirrecibo = new ImprimirRecibo();
$imprimirrecibo->traerImpresionRecibo();
}

class ImprimirRecibo {
public $codigo;
function traerImpresionRecibo(){
global $wpdb; 

$valorRecibo = date("Y").str_pad($this->codigo, 6, "0", STR_PAD_LEFT);

$cnx = Conexion::conectar();

$query = 
 "SELECT "
 . "CONCAT(n.meta_value, ' ', a.meta_value) AS nombre, "
 . "cd.meta_value AS cedula, "
 . "e.fecha_ingresado AS fecha_ingresado, "
 . "(SELECT CONCAT(tn.meta_value, ' ', ta.meta_value) "
 . "FROM wp_users cl "
 . "INNER JOIN wp_usermeta tn ON tn.user_id = cl.ID AND tn.meta_key LIKE 'first_name' "
 . "INNER JOIN wp_usermeta ta ON ta.user_id = cl.ID AND ta.meta_key LIKE 'last_name' "
 . "WHERE "
 . "cl.ID = e.tecnico_id) AS tecnico, "
 . "di.meta_value AS direccion, "
 . "ce.meta_value AS celular, "
 . "cl.user_email AS correo, "
 . "e.descripcion AS descripcion, "
 . "(SELECT m.nombre FROM wp_icsc_marca m WHERE m.id = e.marca_id) AS marca, "
 . "e.serial AS serial, "
 . "e.cargador AS cargador, "
 . "e.falla AS falla, "
 . "e.observacion AS observacion,"
 . "e.estado AS estado "
 . "FROM "
 . "wp_users cl "
 . "INNER JOIN wp_usermeta n ON n.user_id = cl.ID AND n.meta_key LIKE 'first_name' "
 . "INNER JOIN wp_usermeta a ON a.user_id = cl.ID AND a.meta_key LIKE 'last_name' "
 . "INNER JOIN wp_usermeta cd ON cd.user_id = cl.ID AND cd.meta_key LIKE 'cedula' "
 . "INNER JOIN wp_usermeta di ON di.user_id = cl.ID AND di.meta_key LIKE 'direccion' "
 . "INNER JOIN wp_usermeta ce ON ce.user_id = cl.ID AND ce.meta_key LIKE 'celular' "
 . "INNER JOIN wp_icsc_equipo e ON e.cliente_id = cl.ID "
 . "WHERE "
 . "e.id = :id";
    
}    
}
