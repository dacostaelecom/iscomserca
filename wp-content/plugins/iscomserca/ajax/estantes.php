<?php

function load($params)
{
    $estante = new Estante($params);
    return $estante->ejecutar();
}

class Estante
{
    public function __construct($params)
    {
        $this->params = $params;
    }
    public function ejecutar()
    {
        $function = $this->params['function'];
        $return = null;
        switch ($function)
        {
            case 'nuevo':
                $return = $this->add($this->params);
                break;
            case 'buscar':
                $return = $this->load($this->params);
                break;
            case 'buscar_es':
                $return = $this->buscares($this->params);
                break;
            case 'buscarRenglones':
                $return = $this->loadRenglones($this->params);
                break;
            case 'actualizar':
                $return = $this->actualizar($this->params);
                break;
            case 'cargarTodos':
                $return = $this->cargarTodos($this->params);
                break;
        }
        return $return;
    }
    private function add($params)
    {
        global $wpdb;
        $wpdb->query('START TRANSACTION');
        $feedback = '';
        $success = 1;
        $query = "SELECT COUNT(id) AS cantidad FROM ".$wpdb->prefix;
        $query .= "icsc_estante WHERE almacen_id = %d";
        $resultado = $wpdb->get_row($wpdb->prepare($query, $params['almacen']), ARRAY_A);
        $id = ($resultado['cantidad']+1);
        $values = array(
                            'nombre' => $params['nombre'],
                            'almacen_id' => $params['almacen'],
                            'id' => $id
                        );
        $types = array('%s', '%d');
        $success = $wpdb->insert($wpdb->prefix.'icsc_estante', $values, $types);

        if ($success)
        {
            for ($i = 0; $i < count($_POST['renglon']); $i++)
            {
                $values = array(
                                    'id' => ($i+1),
                                    'estante_id' => $id,
                                    'almacen_id' => $this->params['almacen'],
                            	    'nombre' => $this->params['renglon'][$i],
                                    'capacidad' => $this->params['capacidad'][$i]
                                );
                $types = array('%d', '%d', '%d', '%s', '%d');
                $success = $wpdb->insert($wpdb->prefix.'icsc_renglon', $values, $types);
                if (!$success)
                {
                    $success = false;
                    $wpdb->query('ROLLBACK');
                    $feedback = 'Error en rengl&oacute;n #'.($i+1).', se detuvo el ingreso del estante.';
                    break;
                }
            }
            if ($success)
            {
                $wpdb->query('COMMIT');
                $feedback = 'Estante "'.$params['nombre'].'" guardado con &Eacute;xito.';
            }
        }
        else
        {
            $wpdb->query('ROLLBACK');
            $feedback = 'No se pudo guardar el estante "'.$params['nombre'].'", por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
    }
    private function load($params)
    {
	global $wpdb;
	$query = "SELECT id, nombre from ".$wpdb->prefix."icsc_estante ";
	$query .= "WHERE activo = 1 AND almacen_id = %d";
	$estantes = $wpdb->get_results($wpdb->prepare($query, $params['almacen']), ARRAY_A);
        return array('estantes' => $estantes);
    }
    private function buscares($params){
        global $wpdb;
        $query = "SELECT nombre from ".$wpdb->prefix."icsc_estante ";
	$query .= "WHERE activo = 1 and id = %d";
	$estante = $wpdb->get_results($wpdb->prepare($query, $params['estante']), ARRAY_A);
        
        $query = "SELECT id, nombre, capacidad FROM ".$wpdb->prefix."icsc_renglon ";
        $query .= "WHERE estante_id = %d";
        $renglones = $wpdb->get_results($wpdb->prepare($query,$params['estante']), ARRAY_A);
        
        return array('q' => $query, 'estante' => $estante, 'renglones'=>$renglones);
    }
    private function loadRenglones($params)
    {
	    global $wpdb;
	    $query = "SELECT id, nombre, capacidad from ".$wpdb->prefix."icsc_renglon ";
	    $query .= "WHERE almacen_id = %d AND estante_id = %d";
	    $renglones = $wpdb->get_results($wpdb->prepare($query, $params['almacen'], $params['estante']));
        return array('renglones' => $renglones);
    }
    
    private function actualizar($params)
    {
        global $wpdb;
        $wpdb->query('START TRANSACTION');
        $feeback = '';
        $success = 0;
        $query = "SELECT nombre FROM ".$wpdb->prefix."icsc_estante WHERE id = %d AND almacen_id = %d";
        $estante = $wpdb->get_row($wpdb->prepare($query, array($params['estante'], $params['almacen'])), ARRAY_A);
        if ($estante['nombre'] != $params['nombre'])
        {
            $values = array(
                'nombre' => $params['nombre']
            );
            $where = array('id' => $params['estante'], 'almacen_id' => $params['almacen']);
            $types = array('%s');
            $where_types = array('%d', '%d');
            $success = $wpdb->update($wpdb->prefix.'icsc_estante',$values,$where,$types,$where_types);
        }
        else
        {
            $success =1;
        }
        if ($success != false)
        {
            for($i = 0; $i < count($_POST['renglonId']); $i++)
            {
                $values = array(
                    'nombre'    => $params['renglonNombre'][$i],
                    'capacidad' => $params['renglonCapacidad'][$i] 
                );
                $where = array(
                    'id' => $params['renglonId'][$i],
                    'estante_id' => $params['estante'],
                    'almacen_id' => $params['almacen']
                );
                $types = array('%s','%d');
                $where_types = array('%d','%d','%d'); 
                $success = $wpdb->update($wpdb->prefix.'icsc_renglon',$values,$where,$types,$where_types);
                if ($success == false)
                {
                    $wpdb->query('ROLLBACK');
                    $feedback = 'Error al actualizar el rengl&oacute;n "'.$params['renglonId'][$i].'", por favor intente de nuevo.';
                }
            }
            if ($success != false)
            {
                $wpdb->query('COMMIT');
                $feedback = 'Estante actualizado con &eacute;xito.';
            }
        }
        else
        {
            $wpdb->query('ROLLBACK');
            $feedback = 'No se pudo actualizar el estante "'.$params['nombre'].'", por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
        
    }
    
    private function cargarTodos($params){
        global $wpdb;
        $query = "SELECT id, almacen_id, nombre, activo FROM ".$wpdb->prefix."icsc_estante WHERE activo = 1";
        $estantes = $wpdb->get_results($query, ARRAY_A);
        
        $query = "SELECT id, estante_id, almacen_id, nombre, capacidad FROM ".$wpdb->prefix."icsc_renglon";
        $renglones = $wpdb->get_results($query, ARRAY_A);
        
        return array("estantes"=>$estantes, "renglones" => $renglones);
    }
}

