<?php

function load($params)
{
    $tecnico = new Tecnico($params);
    return $tecnico->ejecutar();
}

class Tecnico
{
    public function __construct($params)
    {
        $this->params = $params;
    }
    public function ejecutar()
    {
        $function = $this->params['function'];
        $return = null;
        switch ($function)
        {
            case 'llenar':
                $return = $this->llenar($this->params);
                break;
        }
        return $return;
    }
    
    private function llenar($params){
        global $wpdb;
        $query = "SELECT 
                    u.ID AS user_id, CONCAT(n.meta_value, ' ' , a.meta_value) AS user_name
                FROM
                    wp_users u 
                INNER JOIN 
                    wp_usermeta n ON n.user_id = u.ID AND n.meta_key LIKE 'first_name' 
                INNER JOIN
                    wp_usermeta a ON a.user_id = u.ID AND a.meta_key LIKE 'last_name'
                INNER JOIN 
                    wp_usermeta c ON c.user_id = u.ID AND c.meta_key LIKE 'wp_capabilities'
                WHERE
                    c.meta_value LIKE 'a:1:{s:6:\"author\";b:1;}'";
        $tecnicos = $wpdb->get_results($query, ARRAY_A);
        if($tecnicos){
            return array('success' => 1, 'tecnicos' => $tecnicos);
        }
        
        return array('success' => 0);
        
    }
}
