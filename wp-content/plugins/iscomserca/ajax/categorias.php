<?php

function load($params)
{
    $categoria = new Categoria($params);
    return $categoria->ejecutar();
}

class Categoria
{
    public function __construct($params)
    {
        $this->params = $params;
    }
    public function ejecutar()
    {
        $function = $this->params['function'];
        $return = null;
        switch ($function)
        {
            case 'nueva':
                $return = $this->add($this->params);
                break;
            case 'cargar':
                $return = $this->cargar($this->params);
                break;
            case 'actualizar':
                $return = $this->actualizar($this->params);
                break;
            case 'llenar':
                $return = $this->llenar($this->params);
                break;
        }
        return $return;
    }
    public $params;
    public $success;
    public $feedback;
    private function add($params)
    {
        global $wpdb;
        $feedback = '';
        //$success = 1;
        $values = array(
                            'nombre' => $params['nombre'],
                            'activa' => 1,
                        );
        $types = array('%s', '%d');
        //$this->success = $wpdb->insert($wpdb->prefix.'icsc_categoria', $values, $types);
        /*if ($wpdb->insert($wpdb->prefix.'icsc_categoria', $values, $types) != false)
        {
        return array('success' => 1, 'feedback' => 'Categor&iacute;a guardada con &eacute;xito.');
        }
        else
        {
        return array('success' => 0, 'feedback' => 'No se pudo guardar la categor&iacute;a, por favor intente de nuevo.');

        }*/
        return array('success' => $wpdb->insert($wpdb->prefix.'icsc_categoria', $values, $types), 'feedback' => $feedback);
        //$this->success = $success;
        //$this->feedback = $feedback;
    }
    
    private function cargar($params)
    {
        global $wpdb;
        $query = "SELECT id, nombre, activa FROM ".$wpdb->prefix."icsc_categoria ";
        $query .= "WHERE id = %d";
        $categoria = $wpdb->get_results($wpdb->prepare($query,$params['categoria']), ARRAY_A);
        return array('q'=>$query,'categoria'=>$categoria);
    }
    
    private function actualizar($params)
    {
        global $wpdb;
        $feeback = '';
        $success = 0;
        $values = array(
            'nombre' => $params['nombre'],
            'activa' => $params['activo']
        );
        $where = array(
          'id' => $params['id']  
        );
        
        $types = array('%s','%d');
        $where_types = array('%d');
        $success = $wpdb->update($wpdb->prefix.'icsc_categoria',$values,$where,$types,$where_types);
        if ($success>0 || $success === 0)
        {
            $feedback = 'Categor&iacute;a actualizada con &Eacute;xito.';
        }
        else
        {
            $feedback = 'No se pudo actualizar el categor&iacute;a "'.$params['nombre'].'", por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
    }
    
    private function llenar($params){
        global $wpdb;
        $query = "SELECT id, nombre FROM ".$wpdb->prefix."icsc_categoria WHERE activa = 1";
        $categorias = $wpdb->get_results($query,ARRAY_A);
        if($categorias){
            return array('success' => 1, 'categorias' => $categorias);
        }
        else{
            return array('success' => 0);
        }
    }
}

