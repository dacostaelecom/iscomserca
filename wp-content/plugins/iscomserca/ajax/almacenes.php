<?php

function load($params)
{
    $almacen = new Almacen($params);
    return $almacen->ejecutar();
}

class Almacen
{
    public function __construct($params)
    {
        $this->params = $params;
    }
    public function ejecutar()
    {
        $function = $this->params['function'];
        $return = null;
        switch ($function)
        {
            case 'nuevo':
                $return = $this->add($this->params);
                break;
            case 'cargar':
                $return = $this->cargar($this->params);
                break;
            case 'actualizar':
                $return = $this->actualizar($this->params);
                break;
            case 'llenar':
                $return = $this->llenar($this->params);
                break;
            case 'cargarTodos':
                $return = $this->cargarTodos($this->params);
                break;
        }
        return $return;
    }
    private function add($params)
    {
        global $wpdb;
        $wpdb->query('START TRANSACTION');
        $feedback = '';
        $success = 1;
        $values = array(
                            'nombre' => $params['nombre'],
                            'fecha_creacion' => current_time('mysql'),
                        );
        $types = array('%s', '%s');
        $success = $wpdb->insert($wpdb->prefix.'icsc_almacen', $values, $types);
        if ($success)
        {            
            $almacenId = $wpdb->insert_id;
            $values = array(
                                'nombre' => $params['estante'],
                                'almacen_id' => $almacenId,
                                'id' => 1
                            );
            $types = array('%s', '%d');
            $success = $wpdb->insert($wpdb->prefix.'icsc_estante', $values, $types);

            if ($success)
            {
                for ($i = 0; $i < count($_POST['renglon']); $i++)
                {
                    $values = array(
                                        'id' => ($i+1),
                                        'estante_id' => 1,
                                        'almacen_id' => $almacenId,
                                        'nombre' => $this->params['renglon'][$i],
                                        'capacidad' => $this->params['capacidad'][$i]
                                    );
                    $types = array('%d', '%d', '%d', '%s', '%d');
                    $success = $wpdb->insert($wpdb->prefix.'icsc_renglon', $values, $types);
                    if (!$success)
                    {
                        $success = false;
                        $wpdb->query('ROLLBACK');
                        $feedback = 'Error en rengl&oacute;n #'.($i+1).', se detuvo el ingreso del almac&eacute;n.';
                        break;
                    }
                }
                if ($success)
                {
                    $wpdb->query('COMMIT');
                    $feedback = 'Almac&eacute;n "'.$params['nombre'].'" guardado con &Eacute;xito.';
                }
            }
            else
            {
                $wpdb->query('ROLLBACK');
                $feedback = 'No se pudo guardar el estante, por favor intente de nuevo.';
            }
        }
        else
        {
            $wpdb->query('ROLLBACK');
            $feedback = 'No se pudo guardar el almac&eacute;n "'.$params['nombre'].'", por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
    }
    
    private function cargar($params)
    {
        global $wpdb;
        $query = "SELECT id, nombre, activo FROM ".$wpdb->prefix."icsc_almacen ";
        $query .= "WHERE id = %d";
        $almacen = $wpdb->get_results($wpdb->prepare($query,$params['almacen']), ARRAY_A);
        return array('q'=>$query,'almacen'=>$almacen);
    }
    
    private function actualizar($params)
    {
        global $wpdb;
        $feeback = '';
        $success = 0;
        $values = array(
            'nombre' => $params['nombre'],
            'activo' => $params['activo']
        );
        $where = array(
          'id' => $params['id']  
        );
        
        $types = array('%s','%d');
        $where_types = array('%d');
        $success = $wpdb->update($wpdb->prefix.'icsc_almacen',$values,$where,$types,$where_types);
        if ($success>0)
        {
            $feedback = 'Almac&eacute;n actualizado con &Eacute;xito.';
        }
        else
        {
            $feedback = 'No se pudo actualizar el almac&eacute;n "'.$params['nombre'].'", por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
    }
    
    private function llenar($params){
        global $wpdb;
        $query = "SELECT id, nombre FROM ".$wpdb->prefix."icsc_almacen";
        $almacenes = $wpdb->get_results($query,ARRAY_A);
        if($almacenes){
            return array('success' => 1, 'almacenes' => $almacenes);
        }
        else{
            return array('success' => 0);
        }
    }
    
    private function cargarTodos($params){
        global $wpdb;
        $query = "SELECT id, nombre, activo FROM ".$wpdb->prefix."icsc_almacen WHERE activo = 1";
        $almacenes = $wpdb->get_results($query, ARRAY_A);
        return array('success' => 1, 'almacenes' => $almacenes);
    }
}

