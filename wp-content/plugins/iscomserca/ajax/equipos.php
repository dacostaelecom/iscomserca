<?php

function load($params)
{
    $equipo = new Equipo($params);
    return $equipo->ejecutar();
}

class Equipo
{
    public function __construct($params)
    {
        $this->params = $params;
    }
    public function ejecutar()
    {
        $function = $this->params['function'];
        $return = null;
        switch ($function)
        {
            case 'asignar':
                $return = $this->asignar($this->params);
                break;
            case 'nuevo':
                $return = $this->add($this->params);
                break;
            case 'autocompleteClientes':
                $return = $this->autocompleteClientes($this->params);
                break;
            case 'agregarNota':
                $return = $this->agregarNota($this->params);
                break;
            case 'mover':
                $return = $this->mover($this->params);
                break;
            case 'actualizarEstado':
                $return = $this->actualizarEstado($this->params);
                break;
            case 'editar':
                $return = $this->editar($this->params);
                break;
            case 'paginacion':
                $return = $this->paginacion($this->params);
                break;
            case 'cargarTodos':
                $return = $this->cargarTodos($this->params);
                break;
            case 'cargarXTecnico':
                $return = $this->cargarXTecnico($this->params);
                break;
        }
        return $return;
    }
    
    private function asignar($params){
        global $wpdb;
        $feedback = '';
        $values = array('tecnico_id' => $params['tecnico']);
        $types = array('%d');
        $where = array('id' => $params['equipo']);
        $whereTypes = array('%d');
        $success = $wpdb->update($wpdb->prefix.'icsc_equipo', $values, $where, $types, $whereTypes);

        if (false != $success)
        {
            $feedback .= 'Asignado Técnico a equipo exitosamente.';
        }
        else
        {
            $feedback .= 'No se pudo asignar el técnico al equipo, por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
        
    }


    /**
     * Función para agregar un nuevo equipo 
     * @global type $wpdb
     * @param type $params
     * @return type
     */
    private function add($params)
    {
        global $wpdb;
        $feedback = '';
        $success = 1;
        $wpdb->query('START TRANSACTION');
        $clienteId;
        if (isset($params['clienteId']))
        {
            $clienteId = $params['clienteId'];
            $login = $params['login'];
        }
        else
        {
            $login = $this->makeLogin($params['nombre'], $params['apellido']);
            $pass = $this->generatePassword();
            //return array('success' => 0, 'feedback' => $login.' '.$pass);
            $userData = array(
                'user_login'  =>  $login,
                'user_nicename'  =>  $login,
                'user_email'    =>  $params['correo'],
                'user_pass'   =>  wp_hash_password($pass),
                'user_registered' => current_time('mysql')
            );
            $userTypes = array('%s', '%s', '%s', '%s', '%s');
            $success = $wpdb->insert($wpdb->users, $userData, $userTypes);
            $clienteId = $wpdb->insert_id;            
            if ($success == false)
            {
                $wpdb->query('ROLLBACK');
                return array('success' => 0, 'feedback' => 'Error al ingresar el cliente.');
            }
            
            $success = $wpdb->insert($wpdb->usermeta, 
                                    array('user_id' => $clienteId, 'meta_key' => 'first_name', 'meta_value' => $params['nombre']), 
                                    array('%d', '%s', '%s'));
            if ($success == false)
            {
                $wpdb->query('ROLLBACK');
                return array('success' => 0, 'feedback' => 'Error al ingresar el nombre del cliente.');
            }
            $success = $wpdb->insert($wpdb->usermeta, 
                                    array('user_id' => $clienteId, 'meta_key' => 'last_name', 'meta_value' => $params['apellido']), 
                                    array('%d', '%s', '%s'));
            if ($success == false)
            {
                $wpdb->query('ROLLBACK');
                return array('success' => 0, 'feedback' => 'Error al ingresar el apellido del cliente.');
            }
            $success = $wpdb->insert($wpdb->usermeta, 
                                    array('user_id' => $clienteId, 'meta_key' => 'wp_capabilities', 'meta_value' => 'a:1:{s:10:"subscriber";b:1;}'), 
                                    array('%d', '%s', '%s'));
            if ($success == false)
            {
                $wpdb->query('ROLLBACK');
                return array('success' => 0, 'feedback' => 'Error al ingresar privilegios del cliente.');
            }
            $success = $wpdb->insert($wpdb->usermeta, 
                                    array('user_id' => $clienteId, 'meta_key' => 'wp_user_level', 'meta_value' => '0'), 
                                    array('%d', '%s', '%s'));
            if ($success == false)
            {
                $wpdb->query('ROLLBACK');
                return array('success' => 0, 'feedback' => 'Error al ingresar el nivel del cliente.');
            }
            $success = $wpdb->insert($wpdb->usermeta, 
                                    array('user_id' => $clienteId, 'meta_key' => 'prefijo_cedula', 'meta_value' => $params['prefijo_cedula']), 
                                    array('%d', '%s', '%s'));
            if ($success == false)
            {
                $wpdb->query('ROLLBACK');
                return array('success' => 0, 'feedback' => 'Error al ingresar el prefijo de c&eacute;dula del cliente.');
            }
            $success = $wpdb->insert($wpdb->usermeta, 
                                    array('user_id' => $clienteId, 'meta_key' => 'cedula', 'meta_value' => $params['cedula']), 
                                    array('%d', '%s', '%s'));
            if ($success == false)
            {
                $wpdb->query('ROLLBACK');
                return array('success' => 0, 'feedback' => 'Error al ingresar la c&eacute;dula del cliente.');
            }
            $success = $wpdb->insert($wpdb->usermeta, 
                                    array('user_id' => $clienteId, 'meta_key' => 'celular', 'meta_value' => $params['celular']), 
                                    array('%d', '%s', '%s'));
            if ($success == false)
            {
                $wpdb->query('ROLLBACK');
                return array('success' => 0, 'feedback' => 'Error al ingresar el celular del cliente.');
            }
            $success = $wpdb->insert($wpdb->usermeta, 
                                    array('user_id' => $clienteId, 'meta_key' => 'direccion', 'meta_value' => $params['direccion']), 
                                    array('%d', '%s', '%s'));
            if ($success == false)
            {
                $wpdb->query('ROLLBACK');
                return array('success' => 0, 'feedback' => 'Error al ingresar la direcci&oacute;n del cliente.');
            }
        }
        // Datos del Equipo
        $values = array(
                            'descripcion' => $params['equipo'],
                            'serial' => $params['serial'],
                            'cliente_id' => $clienteId,
                            'marca_id' => $params['marca'],
                            'categoria_id' => $params['categoria'],
                            'cargador'    => $params['cargador'],
                            'estado' => 1,
                            'fecha_ingresado' => current_time('mysql'),
                            'tecnico_id' => $params['tecnico'],
                            'falla' => $params['falla'],
                            'observacion' => $params['observacion']
                        );
        $types = array('%s', '%s', '%d', '%d', '%d', '%d', '%d', '%s', '%d', '%s','%s');
        $success = $wpdb->insert($wpdb->prefix.'icsc_equipo', $values, $types);
        /* Estas van para agregar el movimiento
         *                    'almacen_id' => $params['almacen'],
         *                    'estante_id' => $params['estante'],
         *                    'renglon_id' => $params['renglon'],
         */
        if ($success)
        {
            $id = $wpdb->insert_id;
            $values = array(
                                'id' => 1,
                                'equipo_id' => $id,
                                'almacen_id' => $params['almacen'],
                                'estante_id' => $params['estante'],
                                'renglon_id' => $params['renglon'],
                                'fecha_movimiento' => current_time('mysql')
                            );
            $types = array('%d', '%d', '%d', '%d', '%d', '%s');
            $success = $wpdb->insert($wpdb->prefix.'icsc_equipo_movimientos', $values, $types);
            if ($success)
            {
                $wpdb->query('COMMIT');
                $feedback = 'Equipo "'.$params['equipo'].'" registrado con éxito.';
                $idE = $id;
                $mensaje = "Para hacer seguimiento al status de su equipo, ingrese a nuestro sitio con estas credenciales.\n";
                $mensaje .= "Login: $login\n";
                //$mensaje .= "Clave: $pass\n";
                if (!wp_mail($params['correo'], 'Su Cuenta en Iscomserca', $mensaje))
                {
                    $feedback .= ' Pero no se pudo enviar el correo al cliente, por favor envíelo manualmente ';
                    $feedback .= 'desde el panel de usuarios a la cuenta \''.$login.'\'.';
                }
            }
            else
            {
                $wpdb->query('ROLLBACK');
                $feedback = 'Hubo un error al agregar el equipo "'.$params['equipo'];
                $feedback .= '" en el almac&eacute;n, por favor intente de nuevo.';
                $idE = null;
            }
        }
        else
        {
            $wpdb->query('ROLLBACK');
            $feedback = 'No se pudo registrar el equipo "'.$params['equipo'].'", por favor intente de nuevo.';
            $idE = null;
        }
        return array('success' => $success, 'feedback' => $feedback, 'id' => $idE);
    }
    private function autocompleteClientes($params)
    {
        global $wpdb;
        $query = "SELECT CONCAT(p.meta_value, '-',cd.meta_value, ' - ', n.meta_value, ' ', a.meta_value) AS label, ";
        $query .= "n.meta_value AS nombre, a.meta_value AS apellido, cl.user_email AS email, ";
        $query .= "cd.meta_value AS value, cl.ID AS id, cl.user_login AS alias, ce.meta_value AS celular, di.meta_value AS direccion FROM ";
        $query .= "$wpdb->users cl INNER JOIN $wpdb->usermeta cd ON cd.user_id = cl.ID AND cd.meta_key LIKE 'cedula' ";
        $query .= "INNER JOIN $wpdb->usermeta p ON p.user_id = cl.ID AND p.meta_key LIKE 'prefijo_cedula' ";
        $query .= "INNER JOIN $wpdb->usermeta ce ON ce.user_id = cl.ID AND ce.meta_key LIKE 'celular' ";
        $query .= "INNER JOIN $wpdb->usermeta di ON di.user_id = cl.ID AND di.meta_key LIKE 'direccion' ";
        $query .= "INNER JOIN $wpdb->usermeta n ON n.user_id = cl.ID AND n.meta_key LIKE 'first_name' ";
        $query .= "INNER JOIN $wpdb->usermeta a ON a.user_id = cl.ID AND a.meta_key LIKE 'last_name' ";
        $query .= "WHERE cd.meta_value LIKE %s AND p.meta_value LIKE %s";
        $clientes = $wpdb->get_results($wpdb->prepare($query, array($params['term'].'%', $params['prefijo'])), ARRAY_A);
        
        //var_dump($query);
        //exit(0);
        return $clientes;
    }
    private function actualizarEstado($params)
    {
        global $wpdb;
        $feedback = '';
        $values = array('estado' => $params['estado']);
        $types = array('%d');
        $where = array('id' => $params['equipo']);
        $whereTypes = array('%d');
        $success = $wpdb->update($wpdb->prefix.'icsc_equipo', $values, $where, $types, $whereTypes);

        if ($success != false)
        {
            $feedback .= 'Estado de equipo actualizado exitosamente.';
        }
        else
        {
            $feedback .= 'No se pudo actualizar el estado de equipo, por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
    }
    private function agregarNota($params)
    {
        global $wpdb;
        $feedback = '';
        $query = "SELECT COUNT(id) AS cantidad FROM ".$wpdb->prefix;
        $query .= "icsc_equipo_notas WHERE equipo_id = %d";
        $resultado = $wpdb->get_row($wpdb->prepare($query, $params['equipo']), ARRAY_A);
        $id = ($resultado['cantidad']+1);
        $values = array(
                            'nota' => stripslashes($params['nota']),
                            'equipo_id' => $params['equipo'],
                            'id' => $id,
                            'privada' =>  $params['privada'],
                            'fecing'  =>  date('Y-m-d', strtotime("now"))
                        );
        $types = array('%s', '%d', '%d', '%s');
        $success = $wpdb->insert($wpdb->prefix.'icsc_equipo_notas', $values, $types);
        if ($success != false)
        {
            $feedback .= 'Nota ingresada exitosamente.';
            if ($params['privada'] == 0)
            {
                $feedback .= $this->enviarNota($params['equipo'], stripslashes($params['nota']));
                //$feedback .= $this->enviarNota($params['equipo'], strip_tags($params['nota']));
            }
        }
        else
        {
            $feedback .= 'Error al ingresar la nota, por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
    }
    private function enviarNota($equipo, $nota)
    {
        global $wpdb;
        
        $query = "SELECT u.ID, u.user_email AS correo FROM $wpdb->users u INNER JOIN ";
        $query .= $wpdb->prefix."icsc_equipo e ON e.cliente_id = u.ID WHERE e.id = %d";
        $resultado = $wpdb->get_row($wpdb->prepare($query, $equipo), ARRAY_A);
        $enviado = wp_mail($resultado['correo'], 'Estatus de Equipo', $nota);
        if (!$enviado)
        {
            return ' Pero no se pudo enviar el correo al cliente.';
        }
        else
        {
            return 'Se ha enviado un correo al cliente.';
        }
    }
    private function mover($params)
    {
        global $wpdb;
        $feedback = '';
        $success = 1;
        $query = "SELECT COUNT(id) AS cantidad FROM ".$wpdb->prefix;
        $query .= "icsc_equipo_movimientos WHERE equipo_id = %d";
        $resultado = $wpdb->get_row($wpdb->prepare($query, $params['equipo']), ARRAY_A);
        $id = ($resultado['cantidad']+1);
        $values = array(
                            'equipo_id' => $params['equipo'],
                            'almacen_id' => $params['almacen'],
                            'estante_id' => $params['estante'],
                            'renglon_id' => $params['renglon'],
                            'id' => $id,
                            'fecha_movimiento' => current_time('mysql')
                        );
        $types = array('%d', '%d', '%d', '%d', '%d', '%s');
        $success = $wpdb->insert($wpdb->prefix.'icsc_equipo_movimientos', $values, $types);

        if ($success)
        {
            $feedback .= 'Equipo movido exitosamente.';
        }
        else
        {
            $feedback .= 'No se pudo mover el equipo, por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
    }
    
    private function editar($params)
    {//agregar resto de campos
        global $wpdb;
        $feedback = '';
        $success = 1;
        $query = "SELECT descripcion, serial, categoria_id, marca_id FROM ".$wpdb->prefix;
        $query .= "icsc_equipo WHERE id = %d";
        $equipo = $wpdb->get_row($wpdb->prepare($query, $params['equipo']), ARRAY_A);
        $values = array();
        $types = array();
        if ($equipo['descripcion'] != $params['descripcion'])
        {
            $values['descripcion'] = $params['descripcion'];
            $types[] = '%s';
        }
        if ($equipo['serial'] != $params['serial'])
        {
            $values['serial'] = $params['serial'];
            $types[] = '%s';
        }
        if ($equipo['categoria_id'] != $params['categoria'])
        {
            $values['categoria_id'] = $params['categoria'];
            $types[] = '%d';
        }
        if ($equipo['marca_id'] != $params['marca'])
        {
            $values['marca_id'] = $params['marca'];
            $types[] = '%d';
        }
        $where = array('id' => $params['equipo']);
        $whereTypes = array('%d');
        if (count($values) > 0)
        {
            $success = $wpdb->update($wpdb->prefix.'icsc_equipo', $values, $where, $types, $whereTypes);
        }
        else
        {
            $success = false;
        }
        if ($success)
        {
            $feedback .= 'Equipo actualizado exitosamente.';
        }
        elseif (count($values) > 0)
        {
            $feedback .= 'No se pudo actualizar el estado de equipo, por favor intente de nuevo.';
        }
        else
        {
            $feedback .= 'No hubo datos para modificar.';
        }
        return array('success' => $success, 'feedback' => $feedback, 'query' => $query);
    }
    
    /**
     * Función para crear el usernam
     * @param type $nombre
     * @param type $apellido
     * @return string
     */
    private function makeLogin($nombre, $apellido)
    {
        setlocale(LC_ALL, "en_US.utf8");
        $nombre = strtolower(iconv('utf-8', 'ascii//TRANSLIT//IGNORE', $nombre));
        $nombre = str_replace("'", "", $nombre);
        $apellido = strtolower(iconv('utf-8', 'ascii//TRANSLIT//IGNORE', $apellido));
        $apellido = str_replace("'", "", $apellido);
        $inicial = substr($nombre, 0, 1);
        $i = 0;
        $repetido = false;
        do
        {
            $repetido = get_user_by('login', $inicial.$apellido);
            if ($repetido != false)
            {
                $i++;
                $inicial .= substr($nombre, $i, 1);
            }
        }while($repetido && $i < strlen($nombre));
        $login = $inicial.$apellido;
        if ($repetido)
        {
            $i = 1;
            do
            {
                $repetido = get_user_by('login', $login.$i);
                if ($repetido != false)
                {
                    $i++;
                }
            }while($repetido);
        }
        return $login;
    }
    private function generatePassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        for ($i = 0; $i < 8; $i++)
        {
            $n = rand(0, (strlen($alphabet) - 1));//replace for random_int in PHP7
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    private function paginacion()
    {
        global $wpdb;
        $contador = "SELECT COUNT(id) AS renglones FROM ".$wpdb->prefix."icsc_equipo";
        /*$contador = $wpdb->get_row($query, ARRAY_A);
        $paginas = floor($contador['paginas']/10);
        if ($contador['paginas']%10 > 0)
        {
            $paginas++;
        }*/
        $query = "SELECT e.id, e.descripcion, e.serial, e.categoria_id, c.nombre AS categoria, e.marca_id, ";
        $query .= "m.nombre AS marca, e.estado AS estado, ";
        $query .= " DATE_FORMAT(e.fecha_ingresado, '%d/%m/%Y') AS fecha_ingresado FROM ";
        $query .= $wpdb->prefix."icsc_equipo e INNER JOIN ".$wpdb->prefix."icsc_categoria c ON ";
        $query .= "e.categoria_id = c.id INNER JOIN ".$wpdb->prefix."icsc_marca m ON ";
        $query .= "e.marca_id = m.id";// LIMIT 0, 10"; $_POST['filtro'][...]
        if ($_POST['filtro'] != 0 && $_POST['inicio'] == '' && $_POST['final'] == '')
        {
            $where .= " WHERE estado = '".$_POST['filtro']."' ";
        }
        elseif ($_POST['filtro'] == 0 && ($_POST['inicio'] != '' || $_POST['final'] != ''))
        {
            $where .= " WHERE ";
            if ($_POST['inicio'] != '')
            {
                $start = explode('/', $_POST['inicio']);
                $where .= "fecha_ingresado >= '".$start[2]."-".$start[1]."-".$start[0]." 00:00:00' ";
            }
            if ($_POST['inicio'] != '' && $_POST['final'] != '')
            {
                $where .= "AND ";
            }
            if ($_POST['final'] != '')
            {
                $end = explode('/', $_POST['final']);
                $where .= "fecha_ingresado <= '".$end[2]."-".$end[1]."-".$end[0]." 23:59:59' ";
            }
        }
        elseif ($_POST['filtro'] != 0 && ($_POST['inicio'] != '' || $_POST['final'] != ''))
        {
            $where .= " WHERE estado = '".$_POST['filtro']."' AND ";
            if ($_POST['inicio'] != '')
            {
                $start = explode('/', $_POST['inicio']);
                $where .= "fecha_ingresado >= '".$start[2]."-".$start[1]."-".$start[0]." 00:00:00' ";
            }
            if ($_POST['inicio'] != '' && $_POST['final'] != '')
            {
                $where .= "AND ";
            }
            if ($_POST['final'] != '')
            {
                $end = explode('/', $_POST['final']);
                $where .= "fecha_ingresado <= '".$end[2]."-".$end[1]."-".$end[0]." 23:59:59' ";
            }
        }
        $rows = $wpdb->get_row($contador.$where, ARRAY_A);
        $pages = (int)($rows['rows']/10);
        if ($rows['renglones']%10 > 0)
        {
            $pages++;
        }
        $query .= $where."LIMIT ".(($_POST['pagina']-1)*10).", 10";
        return array('paginas' => $pages, 'results' => $wpdb->get_results($query));    
    }
    
    private function cargarTodos($params){
        global $wpdb;
        
        $query = "SELECT e.id, e.descripcion, e.serial, e.categoria_id, c.nombre AS categoria, e.marca_id, ";
        $query .= "m.nombre AS marca, e.estado AS estado, ";
        $query .= "DATE_FORMAT(e.fecha_ingresado, '%d/%m/%Y') AS fecha_ingresado, ";
        $query .= " e.tecnico_id AS tecnico, cl.meta_value AS cliente FROM ";
        $query .= $wpdb->prefix . "icsc_equipo e INNER JOIN " . $wpdb->prefix . "icsc_categoria c ON ";
        $query .= "e.categoria_id = c.id INNER JOIN " . $wpdb->prefix . "icsc_marca m ON ";
        $query .= "e.marca_id = m.id INNER JOIN wp_usermeta cl ON e.cliente_id = cl.user_id AND cl.meta_key LIKE 'cedula' ";
        $query .= "ORDER BY e.fecha_ingresado DESC";
        $resultados1 = $wpdb->get_results($query, ARRAY_A);
        $almacen = array();
        $equipos = array();
        if($resultados1){
            foreach ($resultados1 as $equipo){
                $query = "SELECT "
                        . " mo.almacen_id AS almacen, mo.estante_id AS estante, mo.renglon_id AS renglon "
                        . "FROM "
                        . $wpdb->prefix."icsc_equipo_movimientos mo "
                        . "WHERE "
                        . " mo.equipo_id = " . $equipo['id'] . " "
                        . " ORDER BY fecha_movimiento DESC "
                        . " LIMIT 0, 1";
                $resultados2 = $wpdb->get_results($query, ARRAY_A);
                $almacen[] = array('almacen' => $resultados2[0]['almacen'], 'estante' => $resultados2[0]['estante'], 'renglon' => $resultados2[0]['renglon']);
            }
            $cont = 0;
            foreach ($resultados1 as $equipo){
                if($equipo['serial'] === ''){ $descripcion = $equipo['descripcion']; }else{ $descripcion = $equipo['descripcion'] . ' - ' . $equipo['serial']; }
                $equipos[] = array(
                    'id' => $equipo['id'], 
                    'equipo' => $descripcion, //$equipo['equipo'],
                    'descripcion' => $equipo['descripcion'],
                    'serial' => $equipo['serial'],
                    'categoria_id' => $equipo['categoria_id'], 
                    'categoria' => $equipo['categoria'],
                    'marca_id' => $equipo['marca_id'],
                    'marca' => $equipo['marca'],
                    'estado' => $equipo['estado'],
                    'cliente' => $equipo['cliente'],
                    'fecha_ingresado' => $equipo['fecha_ingresado'],
                    'almacen' => $almacen[$cont]['almacen'],
                    'estante' => $almacen[$cont]['estante'],
                    'renglon' => $almacen[$cont]['renglon'],
                    'tecnico' => $equipo['tecnico_id']
                );
                $cont++;
            }
            
            return array('equipos' => $equipos);
            
        }
        else{
            return array('equipos' => null);
        }
        
        //if($equipos){
        //return array('equipos' => $equipos, 'query' => $query);
                
        //return array('success' => false);
    }
    
    private function cargarXTecnico($params){
        global $wpdb;
        
        $query = "SELECT e.id, e.descripcion, e.serial, e.categoria_id, c.nombre AS categoria, e.marca_id, ";
        $query .= "m.nombre AS marca, e.estado AS estado, ";
        $query .= "DATE_FORMAT(e.fecha_ingresado, '%d/%m/%Y') AS fecha_ingresado, ";
        $query .= " e.tecnico_id AS tecnico, cl.meta_value AS cliente FROM ";
        $query .= $wpdb->prefix . "icsc_equipo e INNER JOIN " . $wpdb->prefix . "icsc_categoria c ON ";
        $query .= "e.categoria_id = c.id INNER JOIN " . $wpdb->prefix . "icsc_marca m ON ";
        $query .= "e.marca_id = m.id INNER JOIN wp_usermeta cl ON e.cliente_id = cl.user_id AND cl.meta_key LIKE 'cedula'";
        $query .= "WHERE e.tecnico_id = ".$params['usuario']." ";
        $query .= " ORDER BY e.fecha_ingresado DESC";
        $resultados1 = $wpdb->get_results($query, ARRAY_A);
        $almacen = array();
        $equipos = array();
        if($resultados1){
            foreach ($resultados1 as $equipo){
                $query = "SELECT "
                        . " mo.almacen_id AS almacen, mo.estante_id AS estante, mo.renglon_id AS renglon "
                        . "FROM "
                        . $wpdb->prefix."icsc_equipo_movimientos mo "
                        . "WHERE "
                        . " mo.equipo_id = " . $equipo['id'] . " "
                        . " ORDER BY fecha_movimiento DESC "
                        . " LIMIT 0, 1";
                $resultados2 = $wpdb->get_results($query, ARRAY_A);
                $almacen[] = array('almacen' => $resultados2[0]['almacen'], 'estante' => $resultados2[0]['estante'], 'renglon' => $resultados2[0]['renglon']);
            }
            $cont = 0;
            foreach ($resultados1 as $equipo){
                if($equipo['serial'] === ''){ $descripcion = $equipo['descripcion']; }else{ $descripcion = $equipo['descripcion'] . ' - ' . $equipo['serial']; }
                $equipos[] = array(
                    'id' => $equipo['id'], 
                    'equipo' => $descripcion, //$equipo['equipo'],
                    'descripcion' => $equipo['descripcion'],
                    'serial' => $equipo['serial'],
                    'categoria_id' => $equipo['categoria_id'], 
                    'categoria' => $equipo['categoria'],
                    'marca_id' => $equipo['marca_id'],
                    'marca' => $equipo['marca'],
                    'estado' => $equipo['estado'],
                    'fecha_ingresado' => $equipo['fecha_ingresado'],
                    'cliente' => $equipo['cliente'],
                    'almacen' => $almacen[$cont]['almacen'],
                    'estante' => $almacen[$cont]['estante'],
                    'renglon' => $almacen[$cont]['renglon'],
                    'tecnico' => $almacen[$cont]['tecnico']
                );
                $cont++;
            }
            
            return array('equipos' => $equipos);
            
        }
        else{
            return array('equipos' => null);
        }
        
    }
    
}

