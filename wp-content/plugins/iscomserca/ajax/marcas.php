<?php

function load($params)
{
    $marca = new Marca($params);
    return $marca->ejecutar();
}

class Marca
{
    public function __construct($params)
    {
        $this->params = $params;
    }
    public function ejecutar()
    {
        $function = $this->params['function'];
        $return = null;
        switch ($function)
        {
            case 'nueva':
                $return = $this->add($this->params);
                break;
            case 'cargar':
                $return = $this->cargar($this->params);
                break;
            case 'actualizar':
                $return = $this->actualizar($this->params);
                break;
            case 'llenar':
                $return = $this->llenar($this->params);
                break;
        }
        return $return;
    }
    private function add($params)
    {
        global $wpdb;
        $feedback = '';
        $success = 1;
        $values = array(
                            'nombre' => $params['nombre'],
                        );
        $types = array('%s');
        //$success = $wpdb->insert($wpdb->prefix.'icsc_marca', $values, $types);
        /*if ($success)
        {
            $feedback = 'Marca "'.$params['nombre'].'" guardada con &eacute;xito.';
        }
        else
        {
            $feedback = 'No se pudo guardar la marca "'.$params['nombre'].'", por favor intente de nuevo.';
        }*/
        return array('success' => $wpdb->insert($wpdb->prefix.'icsc_marca', $values, $types), 'feedback' => '');
    }
    
    private function cargar($params)
    {
        global $wpdb;
        $query = "SELECT id, nombre FROM ".$wpdb->prefix."icsc_marca ";
        $query .= "WHERE id = %d";
        $marcas = $wpdb->get_results($wpdb->prepare($query,$params['marca']), ARRAY_A);
        return array('q'=>$query,'marca'=>$marcas);
    }
    
    private function actualizar($params)
    {
        global $wpdb;
        $feeback = '';
        $success = 0;
        $values = array(
            'nombre' => $params['nombre']
        );
        $where = array(
          'id' => $params['id']  
        );
        
        $types = array('%s');
        $where_types = array('%d');
        $success = $wpdb->update($wpdb->prefix.'icsc_marca',$values,$where,$types,$where_types);
        if ($success>0||$success===0)
        {
            $feedback = 'Marca actualizada con &Eacute;xito.';
        }
        else
        {
            $feedback = 'No se pudo actualizar la marca "'.$params['nombre'].'", por favor intente de nuevo.';
        }
        return array('success' => $success, 'feedback' => $feedback);
    }
    
    private function llenar($params){
        global $wpdb;
        $query = "SELECT id, nombre FROM ".$wpdb->prefix."icsc_marca";
        $marcas = $wpdb->get_results($query,ARRAY_A);
        if($marcas){
            return array('success' => 1, 'marcas' => $marcas);
        }
        else{
            return array('success' => 0);
        }
    }
}

