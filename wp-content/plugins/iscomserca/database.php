<?php

//creates the tables in case they dont exist
function iscomsercaCreate()
{
    //
    global $wpdb;
    $created = true;
    $creation = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."icsc_almacen` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(60) NOT NULL,
  `fecha_creacion` DATETIME NOT NULL,
  `activo` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`));";



    $created &= $wpdb->query($creation);
    $creation = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."icsc_estante` (
    `id` INT UNSIGNED NOT NULL,
    `almacen_id` INT UNSIGNED NOT NULL,
    `nombre` VARCHAR(60) NOT NULL,
    `activo` TINYINT(1) NOT NULL DEFAULT 1,
    PRIMARY KEY (`almacen_id`, `id`),
    CONSTRAINT `fk_estante_almacen`
      FOREIGN KEY (`almacen_id`)
      REFERENCES `".$wpdb->prefix."icsc_almacen` (`id`)
      ON DELETE NO ACTION
    ON UPDATE NO ACTION);";

    $created &= $wpdb->query($creation);
    $creation = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."icsc_renglon` (
  `id` INT UNSIGNED NOT NULL,
  `estante_id` INT UNSIGNED NOT NULL,
  `almacen_id` INT UNSIGNED NOT NULL,
  `nombre` VARCHAR(60) NOT NULL,
  `capacidad` INT NULL,
  PRIMARY KEY (`almacen_id`, `estante_id`, `id`),
  CONSTRAINT `fk_renglon_estante1`
    FOREIGN KEY (`almacen_id` , `estante_id`)
    REFERENCES `".$wpdb->prefix."icsc_estante` (`almacen_id` , `id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);";

    $created &= $wpdb->query($creation);
    $creation = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."icsc_categoria` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `activa` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`));";

    $created &= $wpdb->query($creation);
    $creation = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."icsc_marca` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`id`));";

    $created &= $wpdb->query($creation);
    $creation = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."icsc_equipo` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador único del registro',
  `descripcion` VARCHAR(80) NOT NULL COMMENT 'Descripción del equipo',
  `serial` VARCHAR(16) NOT NULL COMMENT 'Serial del equipo',
  `cliente_id` BIGINT NOT NULL COMMENT 'Dueño del equipo',
  `fecha_ingresado` DATETIME NOT NULL COMMENT 'Fecha que ingreso el equipo',
  `categoria_id` INT UNSIGNED NOT NULL COMMENT 'Categoría del equipo',
  `marca_id` INT UNSIGNED NOT NULL COMMENT 'Marca del equipo',
  `cargador` TINYINT(1) DEFAULT FALSE COMMENT 'Posee Cargador', 
  `estado` TINYINT NOT NULL COMMENT '1- Recibido 2- Esperando repuestos 3- En reparación 4-Reparado 5- Reparado y entregado 6- Entregado pero no reparado 7- Perdido',
  `fecha_entregado` DATETIME NULL COMMENT 'Fecha de entrega del equipo',
  `tecnico_id` BIGINT(20) NOT NULL COMMENT 'Técnico asignado para reparar equipo',
  `falla` TEXT NULL COMMENT 'Falla que presenta el equipo',
  `observacion` TEXT NULL COMMENT 'Observación',
  `anulado` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Anulado',
  PRIMARY KEY (`id`),
  INDEX `fk_equipo_categoria1_idx` (`categoria_id` ASC),
  INDEX `fk_equipo_marca_id1_idx` (`marca_id` ASC),
  CONSTRAINT `fk_equipo_categoria1`
    FOREIGN KEY (`categoria_id`)
    REFERENCES `".$wpdb->prefix."icsc_categoria` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipo_marca_id1`
    FOREIGN KEY (`marca_id`)
    REFERENCES `".$wpdb->prefix."icsc_marca` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);";

    $created &= $wpdb->query($creation);
    $creation = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."icsc_equipo_movimientos` (
  `id` INT UNSIGNED NOT NULL,
  `equipo_id` BIGINT UNSIGNED NOT NULL,
  `almacen_id` INT UNSIGNED NOT NULL,
  `estante_id` INT UNSIGNED NOT NULL,
  `renglon_id` INT UNSIGNED NOT NULL,
  `fecha_movimiento` DATETIME NOT NULL,
  PRIMARY KEY (`id`, `equipo_id`),
  INDEX `fk_equipo_movimientos_renglon1_idx` (`almacen_id` ASC, `estante_id` ASC, `renglon_id` ASC),
  INDEX `fk_equipo_movimientos_equipo1_idx` (`equipo_id` ASC),
  CONSTRAINT `fk_equipo_movimientos_renglon1`
    FOREIGN KEY (`almacen_id` , `estante_id` , `renglon_id`)
    REFERENCES `".$wpdb->prefix."icsc_renglon` (`almacen_id` , `estante_id` , `id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_equipo_movimientos_equipo1`
    FOREIGN KEY (`equipo_id`)
    REFERENCES `".$wpdb->prefix."icsc_equipo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    ";
    $created &= $wpdb->query($creation);
    $creation = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."icsc_equipo_notas` (
  `id` INT UNSIGNED NOT NULL,
  `equipo_id` BIGINT UNSIGNED NOT NULL,
  `nota` TEXT NOT NULL,
  PRIMARY KEY (`equipo_id`, `id`),
  CONSTRAINT `fk_equipo_notas_equipo1`
    FOREIGN KEY (`equipo_id`)
    REFERENCES `".$wpdb->prefix."icsc_equipo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);";

    $created &= $wpdb->query($creation);

    return $created;
}

//updates tables in case the structure doesn't match what it should be
function iscomsercaUpdate($version)
{
    global $wpdb;
    $updated = true;
    switch($version)
    {
        case 1:
            $update = "ALTER TABLE `".$wpdb->prefix."icsc_equipo` 
        ADD COLUMN `serial` VARCHAR(16) NOT NULL AFTER `descripcion`;";
            $updated &= $wpdb->query($update);
        case 2:
            $update = "ALTER TABLE `".$wpdb->prefix."icsc_equipo_notas` 
        ADD COLUMN `privada` TINYINT NULL DEFAULT 0 AFTER `nota`;";
            $updated &= $wpdb->query($update);
    }
    if ($updated)
    {
        update_option('iscomserca_database', 2);
    }
}
