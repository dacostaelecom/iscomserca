<?php
/*
Plugin Name: Sistema de Servicio de Equipos
Plugin URI: http://iscomserca.com.ve/
Description: Sistema de Servicio de Equipos
Author: Christian Meier y Dixon Acosta
Version: 0.3
Author URI: http://iscomserca.com.ve/
*/

define( 'EQ_PATH', __FILE__ );


add_action( 'admin_menu', 'iscomsercaMenu' );
add_action( 'admin_menu', 'iscomsercaSubmenus');
add_action( 'admin_enqueue_scripts', 'iscomsercaWidgets');
add_action( 'wp_ajax_shoot', 'shoot_callback');
add_action( 'admin_head', 'iscomserca_admin_styles');
//add_filter('query_vars', 'iscomsercaQueryVars');
//add_filter('pre_get_document_title', 'iscomsercaPageTitle');
//add_filter( 'login_redirect', 'eq_login_redirect' );
//add_action('permalink_structure_changed', 'iscomsercaRewrite');


require 'shortcodes.php';
register_activation_hook(__FILE__, 'iscomsercaInstall');  //database and adding pages here
register_deactivation_hook(__FILE__, 'iscomsercaDeactivate');


function iscomsercaMenu()
{
    // Administrador (Analista de Sistema)
    if(current_user_can('administrator')){
        add_menu_page('Lista de Equipos',   'Sistema Iscomserca', 'manage_iscomserca', 'iscomserca/screens/main.php');
    }
    // Editor (Coordinador)
    else if(current_user_can('editor')){
        add_menu_page('Lista de Equipos',   'Sistema Iscomserca', 'edit_iscomserca', 'iscomserca/screens/coordinador.php');
    }
    // Autor (Tecnico)
    else if(current_user_can('author')){
        add_menu_page('Lista de Equipos',   'Sistema Iscomserca', 'auth_iscomserca', 'iscomserca/screens/tecnico.php');
    }
    // Colaborador (Receptor(a))
    else if(current_user_can('contributor')){
        add_menu_page('Lista de Equipos',   'Sistema Iscomserca', 'cola_iscomserca', 'iscomserca/screens/receptor.php');
    }
    //add_menu_page('Lista de Equipos',   'Sistema Iscomserca', 'edit_iscomserca', 'iscomserca/screens/main.php');
}

function iscomsercaSubmenus()
{
    // Administrador (Analista de Sistema)
    add_submenu_page('iscomserca/screens/main.php', 'Agregar Categor&iacute;as', 'Agregar Categor&iacute;as', 'manage_iscomserca', 'iscomserca/submenus/agregar_categorias.php');
    add_submenu_page('iscomserca/screens/main.php', 'Editar Categor&iacute;as', 'Editar Categor&iacute;as', 'manage_iscomserca', 'iscomserca/submenus/editar_categoria.php');
    add_submenu_page('iscomserca/screens/main.php', 'Agregar Marcas', 'Agregar Marcas', 'manage_iscomserca', 'iscomserca/submenus/agregar_marcas.php');
    add_submenu_page('iscomserca/screens/main.php', 'Editar Marcas', 'Editar Marcas', 'manage_iscomserca', 'iscomserca/submenus/editar_marca.php');
    add_submenu_page('iscomserca/screens/main.php', 'Agregar Almacenes', 'Agregar Almacenes', 'manage_iscomserca', 'iscomserca/submenus/agregar_almacenes.php');
    add_submenu_page('iscomserca/screens/main.php', 'Editar Almacenes', 'Editar Almacenes', 'manage_iscomserca', 'iscomserca/submenus/editar_almacen.php');
    add_submenu_page('iscomserca/screens/main.php', 'Agregar Almacenes', 'Agregar Estantes', 'manage_iscomserca', 'iscomserca/submenus/agregar_estantes.php');
    add_submenu_page('iscomserca/screens/main.php', 'Editar Estantes', 'Editar Estantes', 'manage_iscomserca', 'iscomserca/submenus/editar_estante.php');
    add_submenu_page('iscomserca/screens/main.php', 'Agregar Equipos', 'Agregar Equipos', 'manage_iscomserca', 'iscomserca/submenus/agregar_equipos.php');
    
    // Editor (Coordinador)
    add_submenu_page('iscomserca/screens/coordinador.php', 'Agregar Categor&iacute;as', 'Agregar Categor&iacute;as', 'edit_iscomserca', 'iscomserca/submenus/agregar_categorias.php');
    add_submenu_page('iscomserca/screens/coordinador.php', 'Editar Categor&iacute;as', 'Editar Categor&iacute;as', 'edit_iscomserca', 'iscomserca/submenus/editar_categoria.php');
    add_submenu_page('iscomserca/screens/coordinador.php', 'Agregar Marcas', 'Agregar Marcas', 'edit_iscomserca', 'iscomserca/submenus/agregar_marcas.php');
    add_submenu_page('iscomserca/screens/coordinador.php', 'Editar Marcas', 'Editar Marcas', 'edit_iscomserca', 'iscomserca/submenus/editar_marca.php');
    add_submenu_page('iscomserca/screens/coordinador.php', 'Agregar Almacenes', 'Agregar Almacenes', 'edit_iscomserca', 'iscomserca/submenus/agregar_almacenes.php');
    add_submenu_page('iscomserca/screens/coordinador.php', 'Editar Almacenes', 'Editar Almacenes', 'edit_iscomserca', 'iscomserca/submenus/editar_almacen.php');
    add_submenu_page('iscomserca/screens/coordinador.php', 'Agregar Almacenes', 'Agregar Estantes', 'edit_iscomserca', 'iscomserca/submenus/agregar_estantes.php');
    add_submenu_page('iscomserca/screens/coordinador.php', 'Editar Estantes', 'Editar Estantes', 'edit_iscomserca', 'iscomserca/submenus/editar_estante.php');
    add_submenu_page('iscomserca/screens/coordinador.php', 'Agregar Equipos', 'Agregar Equipos', 'edit_iscomserca', 'iscomserca/submenus/agregar_equipos.php');
    
    // Autor (Tecnico)
    add_submenu_page('iscomserca/screens/tecnico.php', 'Agregar Equipos', 'Agregar Equipos', 'auth_iscomserca', 'iscomserca/submenus/agregar_equipos.php');
    
    // Contributor
    add_submenu_page('iscomserca/screens/receptor.php', 'Agregar Equipos', 'Agregar Equipos', 'cola_iscomserca', 'iscomserca/submenus/agregar_equipos.php');
    
}

function iscomsercaWidgets()
{
    // Esto sirve para incluir una version de jQuery distinto del que posee Wordpress (Es un ejemplo)
    /*wp_register_script('jquery3.4.1', plugins_url('iscomserca/js/jquery-3.4.1.min.js'));
    wp_add_inline_script( 'jquery3.4.1', 'var jQuery3_4_1 = $.noConflict(true);' );
    wp_enqueue_script( 'plugin-javascript', plugins_url( 'iscomserca/js/js.js', __FILE__ ), array( 'jquery3.4.1' ) );*/
    
    if (!wp_style_is('jquery-style'))
    {
        wp_enqueue_style('jquery-style', '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.css');
    }
    wp_enqueue_script('jquery-ui-core', array('jquery'));
    wp_enqueue_script('jquery-ui-widget');
    wp_enqueue_script('jquery-ui-dialog');
    wp_enqueue_script('jquery-ui-button');
    wp_enqueue_script('jquery-ui-autocomplete');
    wp_enqueue_script('jquery-ui-spinner');
    wp_enqueue_script('jquery-ui-progressbar');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('blockUI', plugins_url('iscomserca/js/jquery.blockUI.js'));
    wp_enqueue_script('globalize', plugins_url('iscomserca/js/globalize.js'));
    
    // Boostrap 4.3.1
    wp_enqueue_style('bootstrap_css', plugins_url('iscomserca/css/bootstrap.min.css'), array(), '4.3.1');
    wp_enqueue_script('bootstrap_js', plugins_url('iscomserca/js/bootstrap.min.js'), array('jquery'), '4.3.1');
    
    // DataTables 
    wp_enqueue_style('datatables_css', plugins_url('iscomserca/css/jquery.dataTables.min.css'), array(), '1.10.19');
    wp_enqueue_script('datatables_js', plugins_url('iscomserca/js/jquery.dataTables.min.js'), array('jquery'), '1.10.19');
    
    // DataTables Responsive
    wp_enqueue_style('dt_reponsive_css', plugins_url('iscomserca/css/responsive.dataTables.min.css'), array(), '2.2.3');
    wp_enqueue_script('dt_responsive_js', plugins_url('iscomserca/js/dataTables.responsive.min.js'), array(), '2.2.3');
    
    
    // TyniMCE 5.0
    //wp_enqueue_script('tynimce_js', plugins_url('iscomserca/js/tinymce.min.js'), array(), '5.0.9');
    
    // CKEditor
    wp_enqueue_script('ckeditor_js', plugins_url('iscomserca/js/ckeditor/ckeditor.js'), array(), '1.0.0');
    
    // Sweealert2
    wp_enqueue_script('sweealert2_js', plugins_url('iscomserca/js/sweetalert2.all.js'), array(), '7.1.2');
    
    wp_enqueue_style('font-awesome_css', plugins_url('iscomserca/css/font-awesome.min.css'), array("bootstrap_css"), '4.7.0');
}

function iscomserca_admin_styles() {
	?>
	<style>
	/* BlockUI Styles */
	.blockUI.blockPage {
	    z-index: 1011;
		position: fixed;
		padding: 10px 20px;
		margin: 0px;
		width: 30%;
		top: 40%;
		left: 35%;
		text-align: center;
		color: rgb(0, 0, 0);
		border: 1px solid rgb(170, 170, 170);
		border-radius: 5px;
		cursor: wait;
		background-color: rgb(255, 255, 255);
	}
	</style>
	<?php
}

function shoot_callback()
{
    require 'ajax/'.$_POST['screen'].'.php';
    $return = load($_POST);
    echo json_encode($return);

    die(); // this is required to return a proper result
}

function iscomsercaInstall()
{
    //create a folder where all the estate images will be uploaded to
    /*$folder = get_home_path().'wp-content'.DIRECTORY_SEPARATOR;
    $folder .= 'uploads';
    if (!file_exists($folder))
    {
        mkdir($folder, 0755);
    }
    $folder .= DIRECTORY_SEPARATOR.'vd-estate';
    if (!file_exists($folder))
    {
        mkdir($folder, 0755);
    }
    if (!file_exists($folder.DIRECTORY_SEPARATOR.'listings') && !is_multisite())
    {
        mkdir($folder.DIRECTORY_SEPARATOR.'listings', 0755);
    }
    if (!file_exists($folder.DIRECTORY_SEPARATOR.'agents') && !is_multisite())
    {
        mkdir($folder.DIRECTORY_SEPARATOR.'agents', 0755);
    }
	 if (!file_exists($folder.DIRECTORY_SEPARATOR.'agency`') && !is_multisite())
    {
        mkdir($folder.DIRECTORY_SEPARATOR.'agency', 0755);
    }*/

    //update existing roles and create a new role
    // Roles
    $roleAdministrator = get_role('administrator'); // Administrador (Analista del Sistema) 
    $roleEditor = get_role('editor'); // Editor (Coordinador)
    $roleAuthor = get_role('author'); // Author (Tecnico)
    $roleContributor = get_role('contributor'); // Contributor (Receptor)
    
    // Capacidades
    $roleAdministrator->add_cap( 'manage_iscomserca' );
    $roleEditor->add_cap( 'edit_iscomserca' );
    $roleAuthor->add_cap( 'auth_iscomserca' );
    $roleContributor->add_cap( 'cola_iscomserca' );
    /*add_role(
                'estate',
                'Estate Staff',
                array
                (
                    'read'         => true,
                    'edit_posts'   => false,
                    'delete_posts' => false,
                    'manage_estate'=> true
                )
            );*/

    //handle the database tables
    global $wpdb;
    $version = get_option('iscomserca_database');
    if ($version == false)
    {
        add_option('iscomserca_database', 3);
        require_once 'database.php';
        iscomsercaCreate();
    }
    elseif ($version < 3)
    {
        require_once 'database.php';
        iscomsercaUpdate($version);
    }
    iscomsercaPages();
}
function iscomsercaDeactivate()
{

    //set user roles back to normal
    global $wp_roles;
    $wp_roles->remove_cap('administrator', 'manage_iscomserca');
    $wp_roles->remove_cap('editor', 'edit_iscomserca' );
    $wp_roles->remove_cap('author', 'auth_iscomserca' );
    $wp_roles->remove_cap('contributor', 'cola_iscomserca' );
    //remove_role('estate');
    //flush_rewrite_rules();
}
function iscomsercaPages()
{
    global $wpdb;
    $query = "SELECT ID, post_status FROM $wpdb->posts WHERE post_content LIKE ";
    $query .= "'[iscomserca-estado]' AND post_type = 'page'";
    $page = $wpdb->get_row($query, ARRAY_A);
    $postId = 0;
    if ($page == NULL)
    {
        $postId = wp_insert_post(
	      array(
	        'comment_status'  => 'closed',
	        'ping_status'   => 'closed',
	        'post_author'   => get_current_user_id(),
	        'post_name'   => 'iscomserca-estado',
	        'post_title'    => 'Status de Equipo',
	        'post_status'   => 'publish',
	        'post_type'   => 'page',
                'post_content' => '[iscomserca-estado]'
	      )
	    );
    }
    elseif ($page['post_status'] != 'publish')
    {
        wp_update_post(array('ID' => $page['ID'], 'post_status'  => 'publish'));
    }
}
function iscomsercaUserContact($methods, $user) 
{
    //$methods['cedula'] = 'C&eacute;dula/RIF';
    $methods['celular'] = 'Celular';

    return $methods;
}

add_filter( 'user_contactmethods', 'iscomsercaUserContact', 10, 2 );

add_action( 'show_user_profile', 'iscomsercaCedula' );
add_action( 'edit_user_profile', 'iscomsercaCedula' );

function iscomsercaCedula($user)
{
    $prefijos = array('E', 'G', 'J','V');
?>
                <table class="form-table">

            <tr>
                <th><label for="cedula">C&eacute;dula/RIF</label></th>

                <td>
                    <select name="cedula_prefijo" id="cedula_prefijo">
                        <option value=""></option>
<?php
    for($i=0; $i < count($prefijos); $i++)
    {
?>
                        <option value="<?php echo $prefijos[$i];?>"<?php if(get_the_author_meta( 'cedula_prefijo', $user->ID ) == $prefijos[$i]) echo ' selected="selected"';?>><?php echo $prefijos[$i];?></option>
<?php
    }
?>
                    </select>
                    <input type="text" name="cedula" id="cedula" value="<?php echo esc_attr( get_the_author_meta( 'cedula', $user->ID ) ); ?>" class="regular-text" />
                </td>
            </tr>

        </table>
<?php
}



add_action( 'personal_options_update', 'iscomsercaGuardarCedula' );
add_action( 'edit_user_profile_update', 'iscomsercaGuardarCedula' );

function iscomsercaGuardarCedula( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;

    /* Copy and paste this line for additional fields. Make sure to change 'paypal_account' to the field ID. */
    update_usermeta( $user_id, 'cedula_prefijo', $_POST['cedula_prefijo'] );
    update_usermeta( $user_id, 'cedula', $_POST['cedula'] );
}
