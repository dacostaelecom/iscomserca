<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" style="float: right; margin-right: 20px;">
  <div class="toast-header">
    <strong class="mr-auto">Mensaje</strong>
    <!--<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>-->
  </div>
  <div class="toast-body">
      <p id="feedback"></p>
  </div>
</div>
<div class="container">
    <h1 class="text-center text-info">Agregar Estantes</h1>
    <hr/>
    <div class="row form-group">
        <div class="col-md-12">
            <label for="iscomsercaAlmacen">Almac&eacute;n del Estante</label>
            <select id="iscomsercaAlmacen" class="form-control"></select>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <label for="iscomsercaEstante">Nuevo Estante: </label>
            <input type="text" class="form-control" id="iscomsercaEstante" placeholder="Escribe el nombre del estante" max="60" />
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <table id="tablaRenglones" class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Nombre del Rengl&oacute;n</th>
                        <th>Capacidad del Rengl&oacute;n</th>
                        <th style="text-align: center;"><input type="button" class="iscomsercaAgregar btn btn-info btn-sm" value="Agregar" /></th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                    <tr>
                        <th>Nombre del Rengl&oacute;n</th>
                        <th>Capacidad del Rengl&oacute;n</th>
                        <th style="text-align: center;"><input type="button" class="iscomsercaAgregar btn btn-info btn-sm" value="Agregar" /></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <br />
    <div class="row form-group">
        <div class="col-md-12">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaEnviar" value="Guardar" />
        </div>
    </div>
</div>
<script type="text/javascript">
var $ = jQuery.noConflict();

llenarAlmacen = function(){
  $.ajax({
      url: ajaxurl,
      type: 'POST',
      dataType: 'json',
      data:{
          action: 'shoot',
          screen: 'almacenes',
          function: 'cargarTodos'
      }
  }).done(function(data){
     if(parseInt(data.success) === 1){
         $('#iscomsercaAlmacen').empty();
         $('#iscomsercaAlmacen').append($('<option>',{value:'0'}).text('Escoja un almacen...'));
         $.map(data.almacenes, function(item){
             $('#iscomsercaAlmacen').append($('<option>',{value:item.id}).text(item.nombre));
         });
     } 
  });  
};

$(function(){
    $('.toast').toast({ delay: 8000, animation: true });
    llenarAlmacen();
    $('#iscomsercaEnviar').on('click', function(){
        if ($.trim($('#iscomsercaEstante').val()) !== ''){
            var renglones = [];
            var capacidades = [];
            for (var i = 0; i < $("input[name='renglon']").length; i++){
                renglones[i] = $($("input[name='renglon']")[i]).val();
                capacidades[i] = $($("input[name='capacidad']")[i]).val();
            }
            $.blockUI({ message: '<h1>Guardando nuevo estante...</h1>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'estantes',
                    function: 'nuevo',
                    nombre: $('#iscomsercaEstante').val(),
                    almacen: $('#iscomsercaAlmacen').val(),
                    renglon: renglones,
                    capacidad: capacidades
                },
            }).done(function(data){
                $('#iscomsercaAlmacen').val('0');
                $('#iscomsercaEstante').val('');
                $('#tablaRenglones').find('tbody').empty();
                $('#feedback').html(data.feedback);
                $('.toast').toast('show');
                $.unblockUI();
            });
        }else{
            $('#feedback').html('Por favor ingrese un estante');
        }
    });

    $('.iscomsercaAgregar').on('click', function(){
	$('#tablaRenglones').find('tbody').append($('<tr>')
	                        .append($('<td>').css('width', '200px')
	                            .append($('<input>', {'type': 'text', 'name': 'renglon', 'value': $('#country').val(), 'class': 'form-control'})))
	                        .append($('<td>', {'text': $('#state option:selected').text()}).css('width', '200px')
	                            .append($('<input>', {'type': 'number', 'name': 'capacidad', 'min': '0', 'class': 'form-control'})))
	                        .append($('<td>', { 'style': 'text-align: center'}).css('width', '50px')
	                            .append($('<input>', {'type': 'button', 'value': 'X'}).addClass('button').addClass('button-small')
                                .click(function(){remove($(this));}))));
    });
});

function remove(button){
    button.parent().parent().remove();
}
</script>