<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" style="float: right; margin-right: 20px;">
  <div class="toast-header">
    <strong class="mr-auto">Mensaje</strong>
    <!--<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>-->
  </div>
  <div class="toast-body">
      <p id="feedback"></p>
  </div>
</div>
<div class="container" id="iscomsercaPrincipal">
    <div class="row">
        <div class="col-sm">
            <h1 class="text-center text-info">Agregar Equipo</h1>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-sm">
            <h5>Cliente</h5>
        </div>
    </div>
    <hr/>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaPrefijo">C&eacute;dula/RIF</label>
        </div>
        <div class="col-sm col-sm-1">
            <select id="iscomsercaPrefijoCedula" class="form-control"></select>
        </div>
        <div class="col-sm col-sm-10">
            <input type="text" id="iscomsercaCedula" class="iscomsercaCliente form-control">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaLogin">Login</label>
        </div>
        <div class="col-sm col-sm-11">
            <input type="text" id="iscomsercaLogin" class="iscomsercaCliente form-control" readonly="readonly"/>
            <input type="hidden" id="iscomsercaClienteId" value="0" />
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaNombre" class="text-right">Nombre</label>
        </div>
        <div class="col-sm col-sm-5">
            <input type="text" id="iscomsercaNombre" class="iscomsercaCliente form-control">
        </div>
        <div class="col-sm col-sm-1">
            <label for="iscomsercaApellido">Apellido</label>
        </div>
        <div class="col-sm col-sm-5">
            <input type="text" id="iscomsercaApellido" class="iscomsercaCliente form-control">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaCelular" class="text-right">Celular</label>
        </div>
        <div class="col-sm col-sm-5">
            <input type="text" id="iscomsercaCelular" class="iscomsercaCliente form-control">
        </div>
        <div class="col-sm col-sm-1">
            <label for="iscomsercaCorreo">Correo</label>
        </div>
        <div class="col-sm col-sm-5">
            <input type="text" id="iscomsercaCorreo" class="iscomsercaCliente form-control">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaDireccion" class="text-right">Direcci&oacute;n</label>
        </div>
        <div class="col-sm col-sm-11">
            <input type="text" id="iscomsercaDireccion" class="iscomsercaCliente form-control">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-3">
            <input type="button" id="iscomsercaClienteLimpiar" class="btn btn-info btn-sm form-control" value="Limpiar Campos de Cliente">
        </div>
    </div>
    <br/><br/>
    <div class="row">
        <div class="col-sm">
            <h5>Equipo</h5>
        </div>
    </div>
    <hr/>
    
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaEquipo" class="text-right">Equipo</label>
        </div>
        <div class="col-sm col-sm-11">
            <input type="text" id="iscomsercaEquipo" class="form-control" placeholder="Escribe el nombre del equipo" max="80">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaSerial" class="text-right">Serial</label>
        </div>
        <div class="col-sm col-sm-11">
            <input type="text" id="iscomsercaSerial" class="form-control" placeholder="Escribe el serial del equipo" max="80">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaFalla" class="text-right">Falla</label>
        </div>
        <div class="col-sm col-sm-11">
            <input type="text" id="iscomsercaFalla" class="form-control" placeholder="Escribe la falla del equipo" max="80">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaCategoria" class="text-right">Categor&iacute;a</label>
        </div>
        <div class="col-sm col-sm-11">
            <select id="iscomsercaCategoria" class="form-control"></select>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaMarca" class="text-right">Marca</label>
        </div>
        <div class="col-sm col-sm-11">
            <select id="iscomsercaMarca" class="form-control"></select>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaCargador" class="text-right">Cargador</label>
        </div>
        <div class="col-sm col-sm-11">
            <input type="checkbox" id="iscomsercaCargador" class="form-control" value="0"/>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaAlmacen" class="text-right">Almac&eacute;n</label>
        </div>
        <div class="col-sm col-sm-11">
            <select id="iscomsercaAlmacen" class="form-control"></select>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaEstante" class="text-right">Estante</label>
        </div>
        <div class="col-sm col-sm-11">
            <select id="iscomsercaEstante" class="form-control"></select>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaRenglon" class="text-right">Rengl&oacute;n</label>
        </div>
        <div class="col-sm col-sm-11">
            <select id="iscomsercaRenglon" class="form-control"></select>
        </div>
    </div>
    
    <?php if(current_user_can('author')){ ?>
    <input type="hidden" value="<?php global $current_user; echo $current_user->ID; ?>" id="iscomsercaTecnicoSelect"/>
    <?php }else{ ?>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaTecnicoSelect" class="text-right">T&eacute;cnico</label>
        </div>
        <div class="col-sm col-sm-11">
            <select id="iscomsercaTecnicoSelect" class="form-control"></select>
        </div>
    </div>
    <?php } ?>
    <div class="row form-group">
        <div class="col-sm col-sm-1">
            <label for="iscomsercaObservacion" class="text-right">Observaci&oacute;n</label>
        </div>
        <div class="col-sm col-sm-11">
            <textarea id="iscomsercaObservacion" class="form-control"></textarea>
        </div>
    </div>
    <br/>
    
    <div class="row form-group">
        <div class="col-sm col-sm-2">
            <input type="button" id="iscomsercaEnviar" class="btn btn-primary btn-sm form-control" value="Guardar">
        </div>
    </div>
</div>
<script type="text/javascript">
var $ = jQuery.noConflict();

var cargarCombos = function(){
    //alert('entre');
    // Prefijo de Cédula/Razón Social
    $('#iscomsercaPrefijoCedula').append($('<option>').text(''));
    $('#iscomsercaPrefijoCedula').append($('<option>', {value: 'E'}).text('E'));
    $('#iscomsercaPrefijoCedula').append($('<option>', {value: 'G'}).text('G'));
    $('#iscomsercaPrefijoCedula').append($('<option>', {value: 'J'}).text('J'));
    $('#iscomsercaPrefijoCedula').append($('<option>', {value: 'V'}).text('V'));
    
    // Categorías
    $.ajax({
        url: ajaxurl,
          type: 'POST',
          dataType: 'json',
          data:{
              action: 'shoot',
              screen: 'categorias',
              function: 'llenar'
          }
    }).done(function(data){
       if(data){
           if(parseInt(data.success) === 1){
                $('#iscomsercaCategoria').empty();
                $('#iscomsercaCategoria').append($('<option>',{ value: '0'}).text('Escoja una categoría...'));
                $.map(data.categorias, function(item){
                   $('#iscomsercaCategoria').append($('<option>',{ value: item.id }).text(item.nombre));
                });
           }
           
       } 
    });
    
    // Marca
    $.ajax({
        url: ajaxurl,
          type: 'POST',
          dataType: 'json',
          data:{
              action: 'shoot',
              screen: 'marcas',
              function: 'llenar'
          }
    }).done(function(data){
       if(data){
           if(parseInt(data.success) === 1){
                $('#iscomsercaMarca').empty();
                $('#iscomsercaMarca').append($('<option>',{ value: '0'}).text('Escoja una marca...'));
                $.map(data.marcas, function(item){
                   $('#iscomsercaMarca').append($('<option>',{ value: item.id }).text(item.nombre));
                });
           }
           
       } 
    });
    
    // Almacen
    $.ajax({
        url: ajaxurl,
          type: 'POST',
          dataType: 'json',
          data:{
              action: 'shoot',
              screen: 'almacenes',
              function: 'cargarTodos'
          }
    }).done(function(data){
       if(data){
           //console.log(data.success);
           if(parseInt(data.success) === 1){
                $('#iscomsercaAlmacen').empty();
                $('#iscomsercaAlmacen').append($('<option>',{ value: '0'}).text('Escoja un almacen...'));
                $.map(data.almacenes, function(item){
                   $('#iscomsercaAlmacen').append($('<option>',{ value: item.id }).text(item.nombre));
                });
           }
           
       } 
    });
    
    // Tecnicos
    $.ajax({
        url: ajaxurl,
        type: 'POST',
        dataType: 'json',
        data:{
            action: 'shoot',
             screen: 'tecnicos',
             function: 'llenar'
        }
    }).done(function(data){
       if(data){
           if(parseInt(data.success) === 1){
               $('#iscomsercaTecnicoSelect').empty();
               $('#iscomsercaTecnicoSelect').append($('<option>',{value:0}).text('Escoja un técnico...'));
               $.map(data.tecnicos, function(item){
                    $('#iscomsercaTecnicoSelect').append($('<option>',{ value: item.user_id}).text(item.user_name));
               });
           }
           else if(parseInt(data.success) === 0){
               return false;
           }
       } 
    });
};

$(function(){
    cargarCombos();
    $('#iscomsercaPrefijoCedula').focus();
    $('.toast').toast({ delay: 8000, animation: true });
    //alert('<php echo plugins_url(); ?>');
    // Guardar datos del equipo
    $('#iscomsercaEnviar').click(function(){
        if ($.trim($('#iscomsercaEquipo').val()) !== ''){
            var renglones = [];
            var capacidades = [];
            for (var i = 0; i < $("input[name='renglon']").length; i++){
                renglones[i] = $($("input[name='renglon']")[i]).val();
                capacidades[i] = $($("input[name='capacidad']")[i]).val();
            }
            //$.blockUI({ message: '<h1>Guardando nuevo equipo...</h1>'});
            var ajaxJson =  {
                            action: 'shoot',
                            screen: 'equipos',
                            function: 'nuevo',
                            equipo: $('#iscomsercaEquipo').val(),
                            serial: $('#iscomsercaSerial').val(),
                            marca: $('#iscomsercaMarca').val(),
                            categoria: $('#iscomsercaCategoria').val(),
                            cargador: $("#iscomsercaCargador").val(),
                            almacen: $('#iscomsercaAlmacen').val(),
                            estante: $('#iscomsercaEstante').val(),
                            renglon: $('#iscomsercaRenglon').val(),
                            tecnico: $('#iscomsercaTecnicoSelect').val(),
                            falla: $('#iscomsercaFalla').val(),
                            observacion: $('#iscomsercaObservacion').val()
                        };
            // Cliente Nuevo
            if (parseInt($('#iscomsercaClienteId').val()) === 0){
                ajaxJson['prefijo_cedula'] = $('#iscomsercaPrefijoCedula').val();
                ajaxJson['cedula'] = $('#iscomsercaCedula').val();
                ajaxJson['nombre'] = $('#iscomsercaNombre').val();
                ajaxJson['apellido'] = $('#iscomsercaApellido').val();
                ajaxJson['celular'] = $('#iscomsercaCelular').val();
                ajaxJson['direccion'] = $('#iscomsercaDireccion').val();
                ajaxJson['correo'] = $('#iscomsercaCorreo').val();
            }// Cliente ya existe
            else{
                ajaxJson['clienteId'] = $('#iscomsercaClienteId').val();
                ajaxJson['login'] = $('#iscomsercaLogin').val();
                ajaxJson['correo'] = $("#iscomsercaLoginCorreo").val();
            }
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data: ajaxJson,
            }).done(function(data){
                console.log(data.id);
                var POP;
                $('#iscomsercaAlmacen, #iscomsercaMarca, #iscomsercaCategoria, #iscomsercaClienteId,#iscomsercaTecnicoSelect').val('0');
                $('#iscomsercaEquipo,#iscomsercaSerial,#iscomsercaFalla,#iscomsercaObservacion').val('');
                limpiarCliente();
                $('#iscomsercaEstante').empty().append($('<option>', {'value': '0'}).html('Escoja un estante...'));
                $('#iscomsercaRenglon').empty().append($('<option>', {'value': '0'}).html('Escoja un rengl&oacute;n...'));
                $('#feedback').html(data.feedback);
                //$.unblockUI();
                //window.open('http://<?php echo $_SERVER["SERVER_NAME"] . '/iscomserca/wp-content/plugins/iscomserca/reportes/rep_icsc_recibo.php'; ?>?equipo='+data.id, 'Prueba', 'width=800,height=400',"_blank");
                //window.location = "";
                //$('.toast').toast('show');
                swal({
                        type: "success",
                        title: "Guardado",
                        text: "¡"+data.feedback+"!",
                        showConfirmButton: true,
                        confirmButtonText: "Ok"
                }).then(function(result){
                        if(result.value){
                           // USAR EN HOSTING
                           //window.open('http://<?php echo $_SERVER["SERVER_NAME"] . '/wp-content/plugins/iscomserca/reportes/rep_icsc_recibo.php'; ?>?equipo='+data.id, 'Prueba', 'width=800,height=400',"_blank"); 
                           
                           // USAR EN LOCALHOST
                           window.open('http://<?php echo $_SERVER["SERVER_NAME"] . '/iscomserca/wp-content/plugins/iscomserca/reportes/rep_icsc_recibo.php'; ?>?equipo='+data.id, 'Prueba', 'width=800,height=400',"_blank"); 
                           $('#iscomsercaPrefijoCedula').focus();
                        }
                });
            });
        }else{
            swal({
                type: "warning",
                title: "Ingrese Equipo",
                text: "Por favor ingrese los datos para registrar un equipo",
                showConfirmButton: true,
                confirmButtonText: "Ok"
            }).then(function(result){
                if(result.value){
                    $('#iscomsercaPrefijoCedula').focus();
                }
            });
            //$('#feedback').html('Por favor ingrese un equipo');
        }
    });
    
    $("#iscomsercaCargador").on("click", function(){
        //e.preventDefault();
        if($(this).is(":checked")){
            $(this).val(1);
            //alert($(this).val());
        }
        else{
            $(this).val(0);
            //alert($(this).val());
        }
    });
    
    $('#iscomsercaAlmacen').change(function(){
        $('#iscomsercaEstante').empty().append($('<option>', {'value': '0'}).html('Escoja un estante...'));
        $('#iscomsercaRenglon').empty().append($('<option>', {'value': '0'}).html('Escoja un rengl&oacute;n...'));
        if (parseInt($(this).val()) !== 0){
            $.blockUI({ message: '<h4>Buscando estantes...</h4>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'estantes',
                    function: 'buscar',
                    almacen: $('#iscomsercaAlmacen').val()
                },
            }).done(function(data){
                for (var i = 0; i < data.estantes.length; i++){
                    $('#iscomsercaEstante').append($('<option>', {'value': data.estantes[i].id, 'text': data.estantes[i].nombre}));
                }
                $.unblockUI();
            });
        }
    });
    
    $('#iscomsercaEstante').change(function(){
        $('#iscomsercaRenglon').empty();
        $('#iscomsercaRenglon').append($('<option>', {'value': '0'}).html('Escoja un rengl&oacute;n...'));
        if (parseInt($(this).val()) !== 0){
            $.blockUI({ message: '<h4>Buscando renglones...</h4>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'estantes',
                    function: 'buscarRenglones',
                    almacen: $('#iscomsercaAlmacen').val(),
                    estante: $('#iscomsercaEstante').val()
                },
            }).done(function(data){
                for (var i = 0; i < data.renglones.length; i++){
                    $('#iscomsercaRenglon').append($('<option>', {'value': data.renglones[i].id, 'text': data.renglones[i].nombre}));
                }
                $.unblockUI();
            });
        }
    });
    
    $("#iscomsercaCedula").autocomplete({
        minLength: 3,
        delay: 1000,
        source:function(req, res){
            //alert($('#iscomsercaCedula').val()+", "+$('#iscomsercaPrefijoCedula').val());
            $.ajax({
                url: ajaxurl,
                type: "POST",
                dataType: "json",
                data: 
                {
                    action: 'shoot', 
                    screen: 'equipos', 
                    function: 'autocompleteClientes', 
                    term: $('#iscomsercaCedula').val(),
                    prefijo: $('#iscomsercaPrefijoCedula').val()
                }
            }).done(function(data){
                if(data.length !== 0){
                    console.log(data);
                    res($.map(data, function(item){
                        return{
                            label: item.label,
                            value: item.value,
                            id: item.id,
                            alias: item.alias,
                            nombre: item.nombre,
                            apellido: item.apellido,
                            email: item.email,
                            celular: item.celular,
                            prefijo: item.prefijo,
                            cedula: item.cedula,
                            direccion: item.direccion
                        }  
                    }));
                }
                else
                {
                    $('#iscomsercaCedula').removeClass('ui-autocomplete-loading');
                }
            });
        },
        select: function(evt, ui){
            $('#iscomsercaClienteId').val(ui.item.id);
            $('#iscomsercaCedula').autocomplete("option", "disabled", true).attr('disabled', 'disabled');
            $('#iscomsercaLogin').val(ui.item.alias).attr('readonly', 'readonly');
            $('#iscomsercaNombre').val(ui.item.nombre).attr('readonly', 'readonly');
            $('#iscomsercaApellido').val(ui.item.apellido).attr('readonly', 'readonly');
            $('#iscomsercaCorreo').val(ui.item.email).attr('readonly', 'readonly');
            $('#iscomsercaCelular').val(ui.item.celular).attr('readonly', 'readonly');
            $('#iscomsercaDireccion').val(ui.item.direccion).attr('readonly', 'readonly');
            $('#iscomsercaCedula').autocomplete('disable');
            $('#iscomsercaPrefijoCedula').attr('disabled', 'disabled');
            $('#iscomsercaEquipo').focus();
        },
        close: function(){
            $('#iscomsercaCedula').removeClass('ui-autocomplete-loading');
        }
    });
    $('#iscomsercaClienteLimpiar').click(function(){limpiarCliente();});
});
function limpiarCliente(){
    $('.iscomsercaCliente').val('');
    $('#iscomsercaCedula, #iscomsercaNombre, #iscomsercaApellido, #iscomsercaCelular, #iscomsercaCorreo, #iscomsercaDireccion').removeAttr('readonly');
    $('#iscomsercaCedula').autocomplete('option', 'disabled', false).removeAttr('disabled');
    $('#iscomsercaPrefijoCedula').val('').removeAttr('disabled');
    $('#iscomsercaClienteId').val('0');
    $('#iscomsercaPrefijoCedula').focus();
}
</script>