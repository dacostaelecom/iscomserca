<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" style="float: right; margin-right: 20px;">
  <div class="toast-header">
    <strong class="mr-auto">Mensaje</strong>
    <!--<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>-->
  </div>
  <div class="toast-body">
      <p id="feedback"></p>
  </div>
</div>
<div class="container">
    <h1 class="text-center text-info">Editar Estante</h1>
    <hr/>
    <div class="row form-group">
        <div class="col-md-12">
            <label for="iscomsercaAlmacen">Almac&eacute;n</label>
            <select id="iscomsercaAlmacen" class="form-control"></select>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <label for="iscomsercaEstante">Estante</label>
            <select id="iscomsercaEstante" class="form-control"></select>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <label for="iscomsercaNombre">Nombre</label>
            <input type="text" class="form-control" id="iscomsercaNombre" max="60" />
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <table id="tablaRenglones" class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th style="display: none;">Id</th>
                        <th>Nombre del Rengl&oacute;n</th>
                        <th>Capacidad del Rengl&oacute;n</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <br/>
    <div class="row form-group">
        <div class="col-md-12">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaEnviar" value="Actualizar" />
        </div>
    </div>
</div>
<script type="text/javascript">
    var $ = jQuery.noConflict();
    llenarAlmacen = function(){
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            dataType: 'json',
            data:{
                action: 'shoot',
                screen: 'almacenes',
                function: 'cargarTodos'
            }
        }).done(function(data){
           if(parseInt(data.success) === 1){
               $('#iscomsercaAlmacen').empty();
               $('#iscomsercaAlmacen').append($('<option>',{value:'0'}).text('Escoja un almacen...'));
               $.map(data.almacenes, function(item){
                   $('#iscomsercaAlmacen').append($('<option>',{value:item.id}).text(item.nombre));
               });
           } 
        });  
    };
    
    $(function(){
        $('.toast').toast({ delay: 8000, animation: true });
        $('#iscomsercaNombre').attr('disabled', 'disabled');
        $('#iscomsercaEstante').attr('disabled', 'disabled');
        llenarAlmacen();
        
        $('#iscomsercaAlmacen').change(function(){
            if($(this).val() !== 0){
                $.blockUI({ message: '<h1>Cargando datos...</h1>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        action:'shoot',
                        screen:'estantes',
                        function:'buscar',
                        almacen:$('#iscomsercaAlmacen').val()
                    }
                }).done(function(data){
                    if(data!==null){
                        $('#tablaRenglones').find('tbody').empty();
                        $('#iscomsercaNombre').val('').removeAttr('disabled','disabled');
                        $('#iscomsercaEstante').removeAttr('disabled', 'disabled');
                        $('#iscomsercaEstante').empty();
                        $('#iscomsercaEstante').append($('<option>',{value:0}).text('Escoja un estante...'));
                        $.map(data.estantes, function(item){
                            $('#iscomsercaEstante').append($('<option>',{value:item.id}).text(item.nombre));
                        });
                    }
                    else{
                        alert('El Almacen no tiene estantes asignados');
                    }
                    
                    //$('#iscomsercaNombre').focus();
                    $.unblockUI();
                });
            }
         });
         
         $('#iscomsercaEstante').change(function(){
            if($(this).val() !== 0){
                $.blockUI({ message: '<h1>Cargando datos...</h1>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        action:'shoot',
                        screen:'estantes',
                        function:'buscarRenglones',
                        almacen:$('#iscomsercaAlmacen').val(),
                        estante:$('#iscomsercaEstante').val()
                    }
                }).done(function(data){
                    if(data!==null){
                        $('#iscomsercaNombre').val($('#iscomsercaEstante option:selected').text()).removeAttr('disabled');
                        // Trabajar aqui
                        $('#tablaRenglones').find('tbody').empty();
                        for (var i = 0; i < data.renglones.length; i++){
                            $('#tablaRenglones').find('tbody').append(
                                $('<tr>')
                                    .append($('<td>', { 'style': 'display: none'}).addClass('iscomsercaRenglonId').text(data.renglones[i].id)
                                    )
                                    .append(
                                        $('<td>').append($('<input>',{value:data.renglones[i].nombre,type:'text'}).addClass('iscomsercaRenglonNombre'))
                                    )
                                    .append(
                                        $('<td>').append($('<input>',{value:data.renglones[i].capacidad,type:'number'}).addClass('iscomsercaRenglonCapacidad'))
                                    )
                            );
                        }
                    }
                    $.unblockUI();
                });
            }
         });
         
         $('#iscomsercaEnviar').click(function(){
             if($('#iscomsercaAlmacen').val() === '0'){
                 $('#feedback').html('Debe seleccionar un almacen.').attr('style','color:red;font-size:16px;');
                 $('.toast').toast('show');
                 return false;
             }
             if($('#iscomsercaEstante').val() === '0'){
                 $('#feedback').html('Debe seleccionar un estante.').attr('style','color:red;font-size:16px;');
                 $('.toast').toast('show');
                 return false;
             }
             else if($('#iscomsercaNombre').val() === ''){
                 $('#feedback').html('Debe ingresar el nombre del estante.').attr('style','color:red;font-size:16px;');
                 $('.toast').toast('show');
                 return false;
             }
             
             var codigos = [];
             var renglones = [];
             var capacidades = [];
             
             for(var i=0; i < $(".iscomsercaRenglonId").length; i++){
                 codigos[i] = $($(".iscomsercaRenglonId")[i]).text();
                 renglones[i] = $($("input.iscomsercaRenglonNombre")[i]).val();
                 capacidades[i] = $($("input.iscomsercaRenglonCapacidad")[i]).val();
             }
             
             $.blockUI({ message: '<h1>Actualizando estante...</h1>'});
             $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'estantes',
                    function: 'actualizar',
                    estante: $('#iscomsercaEstante').val(),
                    almacen: $('#iscomsercaAlmacen').val(),
                    nombre: $.trim($('#iscomsercaNombre').val()),
                    renglonId: codigos,
                    renglonNombre: renglones,
                    renglonCapacidad: capacidades
                }
             }).done(function(data){

                 if (parseInt(data.success) === 1 || parseInt(data.success) === 0)
                 {
                     $('#iscomsercaEstante').empty();
                     $('#tablaRenglones').find('tbody').empty();
                     $('#iscomsercaNombre').val('').attr('disabled', 'disabled');
                     $('#iscomsercaEstante').attr('disabled', 'disabled');
                     $('#feedback').html(data.feedback).attr('style','color:blue;font-size:16px;');
                     $('.toast').toast('show');
                     llenarAlmacen();
                 }
                 else
                 {
                     $('#feedback').html(data.feedback).attr('style','color:red;font-size:16px;');
                     $('.toast').toast('show');
                 }
                 $.unblockUI();
             });
         });
    });
</script>