<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" style="float: right; margin-right: 20px;">
  <div class="toast-header">
    <strong class="mr-auto">Mensaje</strong>
    <!--<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>-->
  </div>
  <div class="toast-body">
      <p id="feedback"></p>
  </div>
</div>
<div class="container">
    <h1 class="text-center text-info">Editar Categor&iacute;a</h1>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <label for="iscomsercaCategoria">Seleccionar Categor&iacute;a</label>
            <select id="iscomsercaCategoria" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label for="iscomsercaNombre">Nombre</label>
            <input type="text" class="form-control" id="iscomsercaNombre" placeholder="Escribe el nombre de la categor&iacute;a" max="60" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label for="iscomsercaActivo">Activo</label>
            <select id="iscomsercaActivo" class="form-control"></select>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaEnviar" value="Actualizar" />
        </div>
    </div>
</div>
<script type="text/javascript">
    var $ = jQuery.noConflict();
    llenarCategoria = function(){
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            dataType: 'json',
            data:{
                action: 'shoot',
                screen: 'categorias',
                function: 'llenar'
            }
        }).done(function(data){
           if(parseInt(data.success) === 1){
               $('#iscomsercaCategoria').empty();
               $('#iscomsercaCategoria').append($('<option>',{value:'0'}).text('Escoja una categoría...'));
               $.map(data.categorias, function(item){
                   $('#iscomsercaCategoria').append($('<option>',{value:item.id}).text(item.nombre));
               });
           } 
        });  
    };
    
    $(function(){
        $('.toast').toast({ delay: 8000, animation: true });
        $('#iscomsercaNombre').attr('disabled', 'disabled');
        $('#iscomsercaActivo').attr('disabled', 'disabled');
        llenarCategoria();
        
        $('#iscomsercaCategoria').change(function(){
            if($(this).val() !== 0){
                $.blockUI({ message: '<h1>Cargando datos...</h1>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        action:'shoot',
                        screen:'categorias',
                        function:'cargar',
                        categoria:$('#iscomsercaCategoria').val()
                    }
                }).done(function(data){
                    $('#iscomsercaActivo').append(
                         $('<option>',{'value':'1'}).append('<label>').text('Activo'),
                         $('<option>',{'value':'0'}).append('<label>').text('Inactivo'),
                    );
                    $('#iscomsercaNombre').val(data.categoria[0].nombre).removeAttr('disabled');
                    $('#iscomsercaActivo').removeAttr('disabled');
                    $('#iscomsercaActivo').val(data.categoria[0].activa);
                    $('#iscomsercaNombre').focus();
                    $.unblockUI();
                });
            }
         });

         $('#iscomsercaEnviar').on('click', function(){
             if($('#iscomsercaCategoria').val() === '0'){
                 $('#feedback').html('Debe seleccionar un categor&iacute;a.').attr('style','color:red;font-size:16px;');
                 return false;
             }
             else if($('#iscomsercaNombre').val() === ''){
                 $('#feedback').html('Debe ingresar el nombre de la categori&iacute;a.').attr('style','color:red;font-size:16px;');
                 return false;
             }
             else if($('#icomsercaActivo').val() === ''){
                 $('#feedback').html('Debe seleccionar Activo &oacute; Inactivo.').attr('style','color:red;font-size:16px;');
                 return false;
             }
             $.blockUI({ message: '<h1>Actualizando categor&iacute;a...</h1>'});
             $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'categorias',
                    function: 'actualizar',
                    id:     $('#iscomsercaCategoria').val(),
                    nombre: $.trim($('#iscomsercaNombre').val()),
                    activo: $('#iscomsercaActivo').val()
                }
             }).done(function(data){
                 if (parseInt(data.success) === 1 || parseInt(data.success) === 0){
                     $('#iscomsercaCategoria option:eq(0)').attr('selected','selected');
                     $('#iscomsercaNombre').val('').attr('disabled', 'disabled');
                     $('#iscomsercaActivo').empty();
                     $('#feedback').html(data.feedback).attr('style','color:blue;font-size:16px;');
                     $('.toast').toast('show');
                     llenarCategoria();
                 }else{
                     $('#feedback').html(data.feedback).attr('style','color:red;font-size:16px;');
                     $('.toast').toast('show');
                 }
                 $.unblockUI();
             });
         });
    });
</script>