<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" style="float: right; margin-right: 20px;">
  <div class="toast-header">
    <strong class="mr-auto">Mensaje</strong>
    <!--<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>-->
  </div>
  <div class="toast-body">
      <p id="feedback"></p>
  </div>
</div>
<div class="container">
    <h1 class="text-center text-info">Editar Marca</h1>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <label for="iscomsercaMarca">Seleccionar Marca</label>
            <select id="iscomsercaMarca" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label for="iscomsercaNombre">Nombre</label>
            <input type="text" class="form-control" id="iscomsercaNombre" placeholder="Escribe el nombre de la categor&iacute;a" max="60" />
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaEnviar" value="Actualizar" />
        </div>
    </div>
</div>
<script type="text/javascript">
    var $ = jQuery.noConflict();
    llenarMarca = function(){
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            dataType: 'json',
            data:{
                action: 'shoot',
                screen: 'marcas',
                function: 'llenar'
            }
        }).done(function(data){
           if(parseInt(data.success) === 1){
               $('#iscomsercaMarca').empty();
               $('#iscomsercaMarca').append($('<option>',{value:'0'}).text('Escoja una marca...'));
               $.map(data.marcas, function(item){
                   $('#iscomsercaMarca').append($('<option>',{value:item.id}).text(item.nombre));
               });
           } 
        });  
    };
    $(function(){
        $('.toast').toast({ delay: 8000, animation: true });
        $('#iscomsercaNombre').attr('disabled', 'disabled');
        llenarMarca();
        
        $('#iscomsercaMarca').change(function(){
            if($(this).val() !== 0){
                $.blockUI({ message: '<h1>Cargando datos...</h1>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        action:'shoot',
                        screen:'marcas',
                        function:'cargar',
                        marca:$('#iscomsercaMarca').val()
                    }
                }).done(function(data){
                    $('#iscomsercaNombre').val(data.marca[0].nombre).removeAttr('disabled');
                    $('#iscomsercaNombre').focus();
                    $.unblockUI();
                });
            }
         });

         $('#iscomsercaEnviar').on('click', function(){
             if($('#iscomsercaMarca').val() === '0'){
                 $('#feedback').html('Debe seleccionar una marca.').attr('style','color:red;font-size:16px;');
                 $('.toast').toast('show');
                 return false;
             }
             else if($('#iscomsercaNombre').val() === ''){
                 $('#feedback').html('Debe ingresar el nombre de la marca.').attr('style','color:red;font-size:16px;');
                 $('.toast').toast('show');
                 return false;
             }
             $.blockUI({ message: '<h1>Actualizando marca...</h1>'});
             $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'marcas',
                    function: 'actualizar',
                    id:     $('#iscomsercaMarca').val(),
                    nombre: $.trim($('#iscomsercaNombre').val()),
                }
             }).done(function(data){
                 if (parseInt(data.success) === 1 || parseInt(data.success) === 0){
                     $('#iscomsercaMarca option:eq(0)').attr('selected','selected');
                     $('#iscomsercaNombre').val('').attr('disabled', 'disabled');
                     $('#feedback').html(data.feedback).attr('style','color:blue;font-size:16px;');
                     $('.toast').toast('show');
                     llenarMarca();
                 }else{
                     $('#feedback').html(data.feedback).attr('style','color:red;font-size:16px;');
                     $('.toast').toast('show');
                 }
                 $.unblockUI();
             });
         });
    });
</script>