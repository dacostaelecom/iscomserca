<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" style="float: right; margin-right: 20px;">
  <div class="toast-header">
    <strong class="mr-auto">Mensaje</strong>
    <!--<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>-->
  </div>
  <div class="toast-body">
      <p id="feedback"></p>
  </div>
</div>
<div class="container">
    <h1 class="text-center text-info">Agregar Marcas</h1>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <label for="iscomsercaMarca">Nueva Marca</label>
            <input type="text" class="form-control" id="iscomsercaMarca" placeholder="Escribe el nombre de la marca" max="40" />
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaEnviar" value="Guardar" />
        </div>
    </div>
</div>
<script type="text/javascript">
var $ = jQuery.noConflict();
$(function(){
    $('.toast').toast({ delay: 8000, animation: true });
    $('#iscomsercaEnviar').on('click', function(){
        if ($.trim($('#iscomsercaMarca').val()) !== ''){
            $.blockUI({ message: '<h1>Guardando nueva marca...</h1>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'marcas',
                    function: 'nueva',
                    nombre: $('#iscomsercaMarca').val()
                },
            }).done(function(data){
                if (parseInt(data.success) === 1){
                    $('#iscomsercaMarca').val('');
                    $('#feedback').html('Marca agregada con &eacute;xito');
                    $('.toast').toast('show');
                }
                else{
                    $('#feedback').html('No se pudo agregar la marca');
                    $('.toast').toast('show');
                }
                $.unblockUI();
            });
        }
        else{
            $('#feedback').html('Por favor escriba el nombre de la marca');
            $('.toast').toast('show');
        }
    });
});
    // pendiente de agregar nueva version de jquery
    /*(function($){
        alert($.fn.jquery);
    }(jQuery3_4_1));*/
</script>