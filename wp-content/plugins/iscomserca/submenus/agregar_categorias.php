<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" style="float: right; margin-right: 20px;">
  <div class="toast-header">
    <strong class="mr-auto">Mensaje</strong>
    <!--<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>-->
  </div>
  <div class="toast-body">
      <p id="feedback"></p>
  </div>
</div>
<div class="container">
    <h1 class="text-center text-info">Agregar Categor&iacute;as</h1>
    <hr/>
    <div class="row">
        <div class="col-md-12 inline form-group">
            <label for="iscomsercaCategoria">Nueva Categor&iacute;a</label>
            <input type="text" class="form-control" id="iscomsercaCategoria" placeholder="Escribe el nombre de la categor&iacute;a"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaEnviar" value="Guardar" />
        </div>
    </div>
</div>
<script type="text/javascript">
    var $ = jQuery.noConflict();
    $(function(){
       $('.toast').toast({ delay: 8000, animation: true });
       $('#iscomsercaEnviar').on('click', function(){
            if ($.trim($('#iscomsercaCategoria').val()) !== ''){
                $.blockUI({ message: '<h1>Guardando nueva categor&iacute;a...</h1>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        action: 'shoot',
                        screen: 'categorias',
                        function: 'nueva',
                        nombre: $('#iscomsercaCategoria').val()
                    },
                }).done(function(data){
                    if (parseInt(data.success) === 1){
                        $('#iscomsercaCategoria').val('');
                        $('#feedback').html('Categor&iacute;a agregada con &eacute;xito');
                    }
                    else{
                        $('#feedback').html('No se pudo agregar la categor&iacute;a');
                        $('.toast').toast('show');
                    }
                    $.unblockUI();
                });
            }
            else{
                $('#feedback').html('Por favor ingrese una categor&iacute;a');
                $('.toast').toast('show');
            }
        }); 
    });
</script>
    