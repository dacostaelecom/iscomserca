<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" style="float: right; margin-right: 20px;">
  <div class="toast-header">
    <strong class="mr-auto">Mensaje</strong>
    <!--<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>-->
  </div>
  <div class="toast-body">
      <p id="feedback"></p>
  </div>
</div>
<div id="iscomsercaPrincipal" class="container">
    <h1 class="wp-heading-inline text-info text-center" style="margin-top: 10px;">Listado de Equipos</h1>
    <!--<p id="feedback"></p>-->
    <table id="listaEquipos" class="table table-border table-condensed">
        <thead>
            <th>Equipo</th>
            <th>Categor&iacute;a</th>
            <th>Marca</th>
            <th>Estado</th>
            <th>Fecha de recepci&oacute;n</th>
            <th>Cliente</th>
       </thead>
       <tbody></tbody>
    </table>
</div>

<!-- Formulario para Realizar el movimiento del equipo -->
<div id="iscomsercaMovimiento" style="display: none;" class="container">
    <div class="row">
        <h1 class="text text-info">Movimiento de Equipo</h1>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaMovimientoAlmacen">Almac&eacute;n</label>
            <select id="iscomsercaMovimientoAlmacen" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaMovimientoEstante">Estante</label>
            <select id="iscomsercaMovimientoEstante" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaMovimientoRenglon">Renglón</label>
            <select id="iscomsercaMovimientoRenglon" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <input type="hidden" class="form-control" id="iscomsercaMovimientoId" />
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaMover" value="Guardar" />
        </div>
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-secondary form-control iscomsercaVolver" value="Volver" />
        </div>
    </div>
</div>

<!-- Formulario para agregar una nota al equipo -->
<div id="iscomsercaNota" style="display: none;" class="container">
    <div class="row">
        <h1 class="text text-info text-align-center col-md-12">Agregar Nota</h1>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <!--<label for="iscomserca_nota">Nueva Nota</label>-->
            <textarea id="iscomserca_nota"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group-sm">
            <input type="checkbox" id="iscomsercaPrivada"  class="form-control"/>
            <label for="iscomsercaPrivada">¿Es una nota privada?</label>
        </div>
        <!--<div class="col-md-6 form-group-sm">
            <input type="checkbox" id="iscomsercaNECliente"  class="form-control"/>
            <label for="iscomsercaNECliente">¿Notificar por correo al cliente?</label>
        </div>-->
    </div>
    <div class="row">
        <input type="hidden" id="iscomsercaNotaId" />
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaNotificar" value="Guardar" />
        </div>
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-secondary form-control iscomsercaVolver" value="Volver" />
        </div>
    </div>
</div>

<!-- Formulario para cambiar de estado -->
<div id="iscomsercaEstado" style="display: none;" class="container">
    <div class="row">
        <h1 class="text text-info text-align-center col-md-12">Actualizar Estado del Equipo</h1>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEditarCategoria">Estado</label>
            <select id="iscomsercaEstadoSelect" class="form-control form-group"></select>
        </div>
    </div>
    <div class="row">
        <input type="hidden" class="form-control" id="iscomsercaEstadoId" />
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaActualizar" value="Guardar" />
        </div>
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-secondary form-control iscomsercaVolver" value="Volver" />
        </div>
    </div>
</div>

<!-- Formulario para editar un equipo -->
<div id="iscomsercaEdicion" style="display: none;" class="container">
    <div class="row">
        <h1 class="text text-info text-align-center col-md-12">Editar Equipo</h1>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEditarDescripcion">Modelo</label>
            <input type="text" class=" form-control regular-text" id="iscomsercaEditarDescripcion" max="80" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEditarSerial">Serial</label>
            <input type="text" class="form-control regular-text" id="iscomsercaEditarSerial" max="16" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEditarCategoria">Categor&iacute;a</label>
            <select id="iscomsercaEditarCategoria" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEditarMarca">Marca</label>
            <select id="iscomsercaEditarMarca" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <input type="hidden" id="iscomsercaEditarId" />
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaEditar" value="Guardar" />
        </div>
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-secondary form-control iscomsercaVolver" value="Volver" />
        </div>
    </div>
</div>

<script type="text/javascript">
    var $ = jQuery.noConflict();
    var tabla;
    
    var cargarCombos = function(){
      // Combo Almacen
      $.ajax({
          url: ajaxurl,
          type: 'POST',
          dataType: 'json',
          data:{
              action: 'shoot',
              screen: 'almacenes',
              function: 'cargarTodos'
          }
      }).done(function(data){
          if(data){
              $('#iscomsercaMovimientoAlmacen').empty();
              $('#iscomsercaMovimientoAlmacen').append($('<option>', { value: '0' }).text('Escoja un almacén...'));
              $.map(data.almacenes, function(item){
                  $('#iscomsercaMovimientoAlmacen').append($('<option>', { value: item.id }).text(item.nombre));
              });
          }
      });
      
      // Combo Estado
      //$('#iscomsercaEstadoSelect').append($('<option>', { value: '0' }).text('Escoja el estado del equipo...'));
      $('#iscomsercaEstadoSelect').empty();
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '1' }).text('Recibido'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '2' }).text('Esperando repuestos'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '3' }).text('En reparación'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '4' }).text('Reparado'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '5' }).text('Reparado y entregado'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '6' }).text('Entregado pero no reparado'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '7' }).text('Perdido'));
      
      // Combo Categoria
      $.ajax({
         url: ajaxurl,
         type: 'POST',
         dataType: 'json',
         data:{
             action: 'shoot',
             screen: 'categorias',
             function: 'llenar'
         }
      }).done(function(data){
          if(data){
              if(parseInt(data.success) === 1){
                  $('#iscomsercaEditarCategoria').empty();
                  $.map(data.categorias, function(item){
                      $('#iscomsercaEditarCategoria').append($('<option>', { value: item.id }).text(item.nombre));
                  });
              }
          }
      });
      
      // Combo Marcas
      $.ajax({
         url: ajaxurl,
         type: 'POST',
         dataType: 'json',
         data:{
             action: 'shoot',
             screen: 'marcas',
             function: 'llenar'
         }
      }).done(function(data){
          if(data){
              if(parseInt(data.success) === 1){
                  $('#iscomsercaEditarMarca').empty();
                  $.map(data.marcas, function(item){
                      $('#iscomsercaEditarMarca').append($('<option>', { value: item.id }).text(item.nombre));
                  });
              }
          }
      });
    };
    
    var listar = function(){
        tabla =  $('#listaEquipos').DataTable({
            responsive: true,
            destroy: true,
            language:{
                //url: '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'  
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            ajax: {
                url: ajaxurl,
                method: 'POST',
                data:{
                    action: 'shoot',
                    screen: 'equipos',
                    function: 'cargarTodos'
                },
                dataSrc: 'equipos'
            },
            columnDefs:[
                { targets: [0], visible: true, searchable: true, orderable: false },
                { targets: [1], visible: true, searchable: true, orderable: false },
                { targets: [2], visible: true, searchable: true, orderable: false },
                { 
                    targets: [3], visible: true, searchable: true, orderable: false,
                    render: function(value){
                       var estado = '';
                       if(value === null) return "";
                       switch(parseInt(value)){
                        case 1: estado = 'Recibido'; break;
                        case 2: estado = 'Esperando repuestos'; break;
                        case 3: estado = 'En reparación'; break;
                        case 4: estado = 'Reparado'; break;
                        case 5: estado = 'Irreparable'; break;
                        case 6: estado = 'Para repuestos'; break;
                        case 7: estado = 'Reparado y entregado'; break;
                        case 8: estado = 'Entregado pero no reparado'; break;
                        case 9: estado = 'Perdido'; break;
                      }
                      return estado; 
                    }
                },
                { targets: [4], visible: true, searchable: true, orderable: false },
                { 
                    targets: [5], visible: true, searchable: true, orderable: false,
                    render: function(value){
                        if(value === null) return "";
                        return '<i>'+value+'</i>';
                    }
                }
                
            ],
            columns:[
                { data: 'equipo', width: '270px' },
                { data: 'categoria', width: '220px' },
                { data: 'marca', width: '220px' },
                { data: 'estado', width: '200px'},
                { data: 'fecha_ingresado', width: '200px' },
                { data: 'cliente', width: '180px' }
            ]
            
        });
    };
    
    $(function(){
        //alert($.fn.jquery);
        cargarCombos();
        listar();
        //$('.toast').toast({ delay: 10000, animation: true });
        var tabla = $("#listaEquipos").DataTable();
        
       // alert($.fn.jquery);
        
        // Abrir formulario para movimiento de equipo
        $('#listaEquipos > tbody').on('click', 'button.iscomsercaMovimiento', function(){
            $('#iscomsercaPrincipal').hide(300, function () {
                $('#iscomsercaMovimiento').show(300);
            });
            
            // De esta forma se obtienen los datos del datatable por fila
            var datos = tabla.row($(this).parents('tr')).data();
            $('#iscomsercaMovimientoId').val(datos.id);
        });
        
        // Abrir formulario para agregar una nota al equipo 
        $('#listaEquipos > tbody').on('click', 'button.iscomsercaNota', function () {
            $('#iscomsercaPrincipal').hide(300, function () {
                $('#iscomsercaNota').show(300);
            });
            // De esta forma se obtienen los datos del datatable por fila
            var datos = tabla.row($(this).parents('tr')).data();
            $('#iscomsercaNotaId').val(datos.id);
            //console.log($('#iscomsercaNotaId').val());
        });
        
        // Abrir formulario para actualizar el estado del equipo
        $('#listaEquipos > tbody').on('click', 'button.iscomsercaEstado', function(){
            $('#iscomsercaPrincipal').hide(300, function () {
                $('#iscomsercaEstado').show(300);
            });
            // De esta forma se obtienen los datos del datatable por fila
            var datos = tabla.row($(this).parents('tr')).data();
            console.log($('#iscomsercaEstadoId').val());
            $('#iscomsercaEstadoId').val(datos.id);
            $('#iscomsercaEstadoSelect').val(datos.estado);
        });
        
        // Abrir formulario para la edicion del equipo
        $('#listaEquipos > tbody').on('click', 'button.iscomsercaEdicion', function(){
            $('#iscomsercaPrincipal').hide(300, function () {
                $('#iscomsercaEdicion').show(300);
            });
            var datos = tabla.row($(this).parents('tr')).data();
            $('#iscomsercaEditarId').val(datos.id);
            $('#iscomsercaEditarDescripcion').val(datos.descripcion);
            $('#iscomsercaEditarSerial').val(datos.serial);
            $('#iscomsercaEditarCategoria').val(datos.categoria_id);
            $('#iscomsercaEditarMarca').val(datos.marca_id);
        });
        
        // Volver a la pantalla principal
        $('.iscomsercaVolver').on('click', function (){
            console.log("entroe");
            $(this).parent().parent().parent().hide(400, function () {
                $('#iscomsercaPrincipal').show('blind');
            });
        });
        
        // Comboboxes
        $('#iscomsercaMovimientoAlmacen').change(function(){
            $('#iscomsercaMovimientoEstante').empty().append($('<option>', {'value': '0'}).text('Escoja un estante...'));
            $('#iscomsercaMovimientoRenglon').empty().append($('<option>', {'value': '0'}).text('Escoja un renglón...'));
            if (parseInt($(this).val()) !== 0)
            {
                $.blockUI({message: '<h1>Buscando estantes...</h1>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        action: 'shoot',
                        screen: 'estantes',
                        function: 'buscar',
                        almacen: $('#iscomsercaMovimientoAlmacen').val()
                    }
                }).done(function (data){
                    $.map(data.estantes, function(item){
                        $('#iscomsercaMovimientoEstante').append($('<option>', {'value': item.id }).text(item.nombre));
                    });
                    $.unblockUI();
                });
            }
        });
        
        $('#iscomsercaMovimientoEstante').change(function (){
            $('#iscomsercaMovimientoRenglon').empty();
            $('#iscomsercaMovimientoRenglon').append($('<option>', {'value': '0'}).text('Escoja un renglón...'));
            if (parseInt($(this).val()) !== 0){
                $.blockUI({message: '<h1>Buscando renglones...</h1>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:
                    {
                        action: 'shoot',
                        screen: 'estantes',
                        function: 'buscarRenglones',
                        almacen: $('#iscomsercaMovimientoAlmacen').val(),
                        estante: $('#iscomsercaMovimientoEstante').val()
                    },
                }).done(function(data){
                    $.map(data.renglones, function(item){
                        $('#iscomsercaMovimientoRenglon').append($('<option>', {'value': item.id }).text(item.nombre));
                    });
                    $.unblockUI();
                });
            }
        });
        
        // Boton Guardar Movimiento
        $('#iscomsercaMover').on('click', function (){
            $.blockUI({message: '<h1>Moviendo Equipo...</h1>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'equipos',
                    function: 'mover',
                    almacen: $('#iscomsercaMovimientoAlmacen').val(),
                    estante: $('#iscomsercaMovimientoEstante').val(),
                    renglon: $('#iscomsercaMovimientoRenglon').val(),
                    equipo: $('#iscomsercaMovimientoId').val()
                }
            }).done(function (data){
                
                for (var i = 0; i < $(".iscomsercaEquipoId").length; i++)
                {
                    if (parseInt($($(".iscomsercaEquipoId")[i]).val()) === parseInt($('#iscomsercaMovimientoId').val()))
                    {
                        $($(".iscomsercaAlmacenId")[i]).val($('#iscomsercaMovimientoAlmacen').val());
                        $($(".iscomsercaEstanteId")[i]).val($('#iscomsercaMovimientoEstante').val());
                        $($(".iscomsercaRenglonId")[i]).val($('#iscomsercaMovimientoRenglon').val());
                    }
                }
                //$('#iscomsercaMovimiento').dialog('close');
                $('#iscomsercaMovimiento').hide(300, function () {
                    $('#iscomsercaMovimientoAlmacen').val('0');
                    $('#iscomsercaMovimientoEstante').empty();
                    $('#iscomsercaMovimientoRenglon').empty();
                    $('#iscomsercaPrincipal').show(300);
                });
                $('#feedback').html(data.feedback);
                $('.toast').toast('show');
                listar();
                $.unblockUI();
               
            });
        });
        
        // Boton Guardar Nota del Equipo
        $('#iscomsercaNotificar').on('click', function (){
            //console.log('entre en iscomsercaNotificar '+$('#iscomsercaNotaId').val());
            $.blockUI({message: '<h1>Agregando Nota...</h1>'});
            var privada = 0, msgCliente = 0;
            if($('#iscomsercaPrivada').is(':checked') === true){ privada = 1; }
            //if($('#iscomsercaNECliente').is(':checked') === true){ msgCliente = 1; }
            
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:
                {
                    action: 'shoot',
                    screen: 'equipos',
                    function: 'agregarNota',
                    equipo: $('#iscomsercaNotaId').val(),
                    nota: tinymce.get('iscomserca_nota').getContent(),
                    privada: privada
                    //msgcliente: msgCliente
                }
            }).done(function (data){
                $('#iscomsercaNota').hide(300, function (){
                    $('#iscomsercaPrincipal').show(300);
                    $('#iscomsercaPrivada').removeAttr('checked');
                    $('#iscomsercaNECliente').removeAttr('checked');
                });
                $('#feedback').html(data.feedback);
                tinymce.get('iscomserca_nota').setContent('', {format:'raw'});
                $('.toast').toast('show');
                listar();
                $.unblockUI();
            });
        });
        
        // Boton Actualizar Estado
        $('#iscomsercaActualizar').on('click', function (){
            $.blockUI({message: '<h1>Actualizando Estado de Equipo...</h1>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'equipos',
                    function: 'actualizarEstado',
                    equipo: $('#iscomsercaEstadoId').val(),
                    estado: $('#iscomsercaEstadoSelect').val()
                }
            }).done(function (data){
                for (var i = 0; i < $(".iscomsercaEquipoId").length; i++){
                    if (parseInt($($(".iscomsercaEquipoId")[i]).val()) === parseInt($('#iscomsercaEstadoId').val())){
                        $($(".iscomsercaEstadoId")[i]).val($('#iscomsercaEstadoSelect').val());
                        $($(".iscomsercaEstadoColumna")[i]).text($('#iscomsercaEstadoSelect option:selected').text());
                    }
                }
                $('#iscomsercaEstado').hide(300, function () {
                    $('#iscomsercaPrincipal').show(300);
                });
                $('#feedback').html(data.feedback);
                $('.toast').toast('show');
                listar();
                $.unblockUI();
            });
        });
        
        // Boton para guardar actualización del equipo
        $('#iscomsercaEditar').on('click', function(){
            $.blockUI({message: '<h1>Actualizando del Equipo...</h1>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'equipos',
                    function: 'editar',
                    equipo: $('#iscomsercaEditarId').val(),
                    descripcion: $.trim($('#iscomsercaEditarDescripcion').val()),
                    serial: $.trim($('#iscomsercaEditarSerial').val()),
                    categoria: $('#iscomsercaEditarCategoria').val(),
                    marca: $('#iscomsercaEditarMarca').val()
                }
            }).done(function (data){
                /*for (var i = 0; i < $(".iscomsercaEquipoId").length; i++){
                    if (parseInt($($(".iscomsercaEquipoId")[i]).val()) === parseInt($('#iscomsercaEstadoId').val())){
                        $($(".iscomsercaEstadoId")[i]).val($('#iscomsercaEstadoSelect').val());
                        $($(".iscomsercaEstadoColumna")[i]).text($('#iscomsercaEstadoSelect option:selected').text());
                    }
                }*/
                //$('#feedback').html(data.feedback);
                //$('#iscomsercaEstado').dialog('close');
                $('#iscomsercaEdicion').hide(300, function () {
                    $('#iscomsercaPrincipal').show(300);
                });
                $('#feedback').html(data.feedback);
                $('.toast').toast('show');
                listar();
                $.unblockUI();
            });
        });
    });
    
</script>