<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="true" style="float: right; margin-right: 20px;">
  <div class="toast-header">
    <strong class="mr-auto">Mensaje</strong>
    <!--<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>-->
  </div>
  <div class="toast-body">
      <p id="feedback"></p>
  </div>
</div>
<div id="iscomsercaPrincipal" class="container">
    <h1 class="wp-heading-inline text-info text-center" style="margin-top: 10px;">Listado de Equipos</h1>
    <!--<p id="feedback"></p>-->
    <table id="listaEquipos" class="table table-border table-condensed">
        <thead>
            <th>Equipo</th>
            <th>Categor&iacute;a</th>
            <th>Marca</th>
            <th>Estado</th>
            <th>Fecha de recepci&oacute;n</th>
            <th>Cliente</th>
            <th></th> <!-- Botones -->
            <th></th> <!-- equipo_id -->
            <th></th> <!-- almacen_id -->
            <th></th> <!-- estante_id -->
            <th></th> <!-- renglon_id -->
            <th></th> <!-- categoria_id -->
            <th></th> <!-- estado_id -->
       </thead>
       <tbody></tbody>
    </table>
</div>

<!-- Formulario para Realizar el movimiento del equipo -->
<div id="iscomsercaMovimiento" style="display: none;" class="container">
    <div class="row">
        <h1 class="text text-info">Movimiento de Equipo</h1>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaMovimientoAlmacen">Almac&eacute;n</label>
            <select id="iscomsercaMovimientoAlmacen" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaMovimientoEstante">Estante</label>
            <select id="iscomsercaMovimientoEstante" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaMovimientoRenglon">Renglón</label>
            <select id="iscomsercaMovimientoRenglon" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <input type="hidden" class="form-control" id="iscomsercaMovimientoId" />
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaMover" value="Guardar" />
        </div>
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-secondary form-control iscomsercaVolver" value="Volver" />
        </div>
    </div>
</div>

<!-- Formulario para agregar una nota al equipo -->
<div id="iscomsercaNota" style="display: none;" class="container">
    <div class="row">
        <h1 class="text text-info text-align-center col-md-12">Agregar Nota</h1>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <!--<label for="iscomserca_nota">Nueva Nota</label>-->
            <textarea id="iscomserca_nota"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 form-group-sm">
            <input type="checkbox" id="iscomsercaPrivada"  class="form-control"/>
            <label for="iscomsercaPrivada">¿Es una nota privada?</label>
        </div>
        <!--<div class="col-md-6 form-group-sm">
            <input type="checkbox" id="iscomsercaNECliente"  class="form-control"/>
            <label for="iscomsercaNECliente">¿Notificar por correo al cliente?</label>
        </div>-->
    </div>
    <div class="row">
        <input type="hidden" id="iscomsercaNotaId" />
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaNotificar" value="Guardar" />
        </div>
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-secondary form-control iscomsercaVolver" value="Volver" />
        </div>
    </div>
</div>

<!-- Formulario para cambiar de estado -->
<div id="iscomsercaEstado" style="display: none;" class="container">
    <div class="row">
        <h1 class="text text-info text-align-center col-md-12">Actualizar Estado del Equipo</h1>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEstadoSelect">Estado</label>
            <select id="iscomsercaEstadoSelect" class="form-control form-group"></select>
        </div>
    </div>
    <div class="row">
        <input type="hidden" class="form-control" id="iscomsercaEstadoId" />
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaActualizar" value="Guardar" />
        </div>
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-secondary form-control iscomsercaVolver" value="Volver" />
        </div>
    </div>
</div>

<!-- Formulario para editar un equipo -->
<div id="iscomsercaEdicion" style="display: none;" class="container">
    <div class="row">
        <h1 class="text text-info text-align-center col-md-12">Editar Equipo</h1>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEditarDescripcion">Modelo</label>
            <input type="text" class=" form-control regular-text" id="iscomsercaEditarDescripcion" max="80" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEditarSerial">Serial</label>
            <input type="text" class="form-control regular-text" id="iscomsercaEditarSerial" max="16" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEditarCategoria">Categor&iacute;a</label>
            <select id="iscomsercaEditarCategoria" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaEditarMarca">Marca</label>
            <select id="iscomsercaEditarMarca" class="form-control"></select>
        </div>
    </div>
    <div class="row">
        <input type="hidden" id="iscomsercaEditarId" />
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaEditar" value="Guardar" />
        </div>
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-secondary form-control iscomsercaVolver" value="Volver" />
        </div>
    </div>
</div>

<!-- Formulario para asignar el tecnico al equipo -->
<div id="iscomsercaTecnico" style="display: none;" class="container">
    <div class="row">
        <h1 class="text text-info text-align-center col-md-12">Asignaci&oacute;n de T&eacute;cnico</h1>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <label for="iscomsercaTecnicoSelect">T&eacute;cnico</label>
            <select id="iscomsercaTecnicoSelect" class="form-control form-group"></select>
        </div>
    </div>
    <div class="row">
        <input type="hidden" class="form-control" id="iscomsercaEquipoId" />
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-primary form-control" id="iscomsercaAsignar" value="Asignar" />
        </div>
        <div class="col-md-6 form-group">
            <input type="button" class="btn btn-secondary form-control iscomsercaVolver" value="Volver" />
        </div>
    </div>
</div>

<script type="text/javascript">
    var $ = jQuery.noConflict();
    var tabla;
    
    var cargarCombos = function(){
      // Combo Almacen
      $.ajax({
          url: ajaxurl,
          type: 'POST',
          dataType: 'json',
          data:{
              action: 'shoot',
              screen: 'almacenes',
              function: 'cargarTodos'
          }
      }).done(function(data){
          if(data){
              $('#iscomsercaMovimientoAlmacen').empty();
              $('#iscomsercaMovimientoAlmacen').append($('<option>', { value: '0' }).text('Escoja un almacén...'));
              $.map(data.almacenes, function(item){
                  $('#iscomsercaMovimientoAlmacen').append($('<option>', { value: item.id }).text(item.nombre));
              });
          }
      });
      
      // Combo Estado
      //$('#iscomsercaEstadoSelect').append($('<option>', { value: '0' }).text('Escoja el estado del equipo...'));
      $('#iscomsercaEstadoSelect').empty();
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '1' }).text('Recibido'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '2' }).text('Esperando repuestos'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '3' }).text('En reparación'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '4' }).text('Reparado'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '5' }).text('Reparado y entregado'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '6' }).text('Entregado pero no reparado'));
      $('#iscomsercaEstadoSelect').append($('<option>', { value: '7' }).text('Perdido'));
      
      // Combo Categoria
      $.ajax({
         url: ajaxurl,
         type: 'POST',
         dataType: 'json',
         data:{
             action: 'shoot',
             screen: 'categorias',
             function: 'llenar'
         }
      }).done(function(data){
          if(data){
              if(parseInt(data.success) === 1){
                  $('#iscomsercaEditarCategoria').empty();
                  $.map(data.categorias, function(item){
                      $('#iscomsercaEditarCategoria').append($('<option>', { value: item.id }).text(item.nombre));
                  });
              }
          }
      });
      
      // Combo Marcas
      $.ajax({
         url: ajaxurl,
         type: 'POST',
         dataType: 'json',
         data:{
             action: 'shoot',
             screen: 'marcas',
             function: 'llenar'
         }
      }).done(function(data){
          if(data){
              if(parseInt(data.success) === 1){
                  $('#iscomsercaEditarMarca').empty();
                  $.map(data.marcas, function(item){
                      $('#iscomsercaEditarMarca').append($('<option>', { value: item.id }).text(item.nombre));
                  });
              }
          }
      });
      
      // Combo Tecnico
      $.ajax({
         url: ajaxurl,
         type: 'POST',
         dataType: 'json',
         data:{
             action: 'shoot',
             screen: 'tecnicos',
             function: 'llenar'
         }
      }).done(function(data){
         if(data){
             if(parseInt(data.success) === 1){
               $('#iscomsercaTecnicoSelect').empty();
               $('#iscomsercaTecnicoSelect').append($('<option>',{value: '0'}).text('Escoja un técnico...'));
               $.map(data.tecnicos, function(item){
                    $('#iscomsercaTecnicoSelect').append($('<option>',{ value: item.user_id}).text(item.user_name));
               });
           }
           else if(parseInt(data.success) === 0){
               return false;
           }
         } 
      });
    };
    
    var listar = function(){
        var tabla =  $('#listaEquipos').DataTable({
            responsive: true,
            destroy: true,
            language:{
                //url: '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'  
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            ajax: {
                url: ajaxurl,
                method: 'POST',
                data:{
                    action: 'shoot',
                    screen: 'equipos',
                    function: 'cargarTodos'
                },
                dataSrc: 'equipos'
            },
            columnDefs:[
                { targets: [0], visible: true, searchable: true, orderable: false },
                { targets: [1], visible: true, searchable: true, orderable: false },
                { targets: [2], visible: true, searchable: true, orderable: false },
                { 
                    targets: [3], visible: true, searchable: true, orderable: false,
                    render: function(value){
                       var estado = '';
                       if(value === null) return "";
                       switch(parseInt(value)){
                        case 1: estado = 'Recibido'; break;
                        case 2: estado = 'Esperando repuestos'; break;
                        case 3: estado = 'En reparación'; break;
                        case 4: estado = 'Reparado'; break;
                        case 5: estado = 'Irreparable'; break;
                        case 6: estado = 'Para repuestos'; break;
                        case 7: estado = 'Reparado y entregado'; break;
                        case 8: estado = 'Entregado pero no reparado'; break;
                        case 9: estado = 'Perdido'; break;
                      }
                      return estado; 
                    }
                },
                { targets: [4], visible: true, searchable: true, orderable: true },
                { 
                    targets: [5], visible: true, searchable: true, orderable: false,
                    render: function(value){
                        if(value === null) return "";
                        return '<i>'+value+'</i>';
                    }
                },
                { targets: [6], orderable: false }, // Botones
                { targets: [7], visible: false, searchable: false, orderable: false },
                { targets: [8], visible: false, searchable: false, orderable: false },
                { targets: [9], visible: false, searchable: false, orderable: false },
                { targets: [10], visible: false, searchable: false, orderable: false },
                { targets: [11], visible: false, searchable: false, orderable: false },
                { targets: [12], visible: false, searchable: false, orderable: false }
            ],
            columns:[
                { data: 'equipo', width: '250px' },
                { data: 'categoria', width: '200px' },
                { data: 'marca', width: '200px' },
                { data: 'estado', width: '180px'},
                { data: 'fecha_ingresado', width: '180px' },
                { data: 'cliente', width: '180px' },
                { 
                    data: null,
                    width: '190px',
                    defaultContent: "<button class='btn btn-primary btn-sm iscomsercaMovimiento' title='Mover Equipo'><i class='fa fa-exchange'></i></button>"
                            +"<button class='btn btn-warning btn-sm iscomsercaNota' title='Agregar Nota' style='color: white;'><i class='fa fa-comment'></i></button>"
                            +"<button class='btn btn-success btn-sm iscomsercaEstado' title='Cambiar Estado del Equipo'><i class='fa fa-clipboard'></i></button>"
                            +"<button class='btn btn-info btn-sm iscomsercaEdicion' title='Editar datos del Equipo'><i class='fa fa-pencil'></i></button>"
                            //+"<button class='btn btn-warning btn-sm iscomsercaTecnico' title='Asignar nuevo técnico a Equipo' style='color: white;'><i class='fa fa-user'></i></button>"
                },
                { data: 'categoria_id', class: 'iscomsercaCategoriaId'},
                { data: 'estado', class: 'iscomsercaEstadoId'},
                { data: 'almacen', class: 'iscomsercaAlmacenId'},
                { data: 'estante', class: 'iscomsercaEstanteId'},
                { data: 'renglon', class: 'iscomsercaRenglonId'},
                { data: 'id', class: 'iscomsercaEquipoId'}
            ]
            
        });
    };
    
    $(function(){
        //alert($.fn.jquery);
        
        cargarCombos();
        listar();
        var tabla = $("#listaEquipos").DataTable();
        
        CKEDITOR.replace("iscomserca_nota", {
            toolbar:[
                { name: "basistyles", items: ['Bold', 'Italic']},
                { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] }
            ]
        });
        
       // alert($.fn.jquery);
        
        // Abrir formulario para movimiento de equipo
        $('#listaEquipos > tbody').on('click', 'button.iscomsercaMovimiento', function(){
            $('#iscomsercaPrincipal').hide(300, function () {
                $('#iscomsercaMovimiento').show(300);
            });
            // De esta forma se obtienen los datos del datatable por fila
            var datos = tabla.row(this).data();
            $('#iscomsercaMovimientoId').val(datos.id);
        });
        
        // Abrir formulario para agregar una nota al equipo 
        $('#listaEquipos > tbody').on('click', 'button.iscomsercaNota', function () {
            $('#iscomsercaPrincipal').hide(300, function () {
                $('#iscomsercaNota').show(300);
            });
            // De esta forma se obtienen los datos del datatable por fila
            var datos = tabla.row(this).data();
            $('#iscomsercaNotaId').val(datos.id);
            //console.log($('#iscomsercaNotaId').val());
        });
        
        // Abrir formulario para actualizar el estado del equipo
        $('#listaEquipos > tbody').on('click', 'button.iscomsercaEstado', function(){
            //var tabla = $("#listaEquipos").DataTable();
            $('#iscomsercaPrincipal').hide(300, function () {
                $('#iscomsercaEstado').show(300);
            });
            // De esta forma se obtienen los datos del datatable por fila
            var datos = tabla.row(this).data();
            $('#iscomsercaEstadoId').val(datos.id);
            $('#iscomsercaEstadoSelect').val(datos.estado);
        });
        
        // Abrir formulario para la edicion del equipo
        $('#listaEquipos > tbody').on('click', 'button.iscomsercaEdicion', function(){
            $('#iscomsercaPrincipal').hide(300, function () {
                $('#iscomsercaEdicion').show(300);
            });
            var datos = tabla.row(this).data();
            $('#iscomsercaEditarId').val(datos.id);
            $('#iscomsercaEditarDescripcion').val(datos.descripcion);
            $('#iscomsercaEditarSerial').val(datos.serial);
            $('#iscomsercaEditarCategoria').val(datos.categoria_id);
            $('#iscomsercaEditarMarca').val(datos.marca_id);
        });
        
        // Abrir formulario para la asignación del equipo al tecnico
        $('#listaEquipos > tbody').on('click', 'button.iscomsercaTecnico', function(){
            $('#iscomsercaPrincipal').hide(300, function () {
                $('#iscomsercaTecnico').show(300);
            });
            var datos = tabla.row(this).data();
            $('#iscomsercaEquipoId').val(datos.id);
        });
        
        // Volver a la pantalla principal
        $('.iscomsercaVolver').on('click', function (){
            $(this).parent().parent().parent().hide(400, function () {
                $('#iscomsercaPrincipal').show('blind');
            });
        });
        
        // Comboboxes
        $('#iscomsercaMovimientoAlmacen').change(function(){
            $('#iscomsercaMovimientoEstante').empty().append($('<option>', {'value': '0'}).text('Escoja un estante...'));
            $('#iscomsercaMovimientoRenglon').empty().append($('<option>', {'value': '0'}).text('Escoja un renglón...'));
            if (parseInt($(this).val()) !== 0)
            {
                $.blockUI({message: '<h3>Buscando estantes...</h3>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        action: 'shoot',
                        screen: 'estantes',
                        function: 'buscar',
                        almacen: $('#iscomsercaMovimientoAlmacen').val()
                    }
                }).done(function (data){
                    $.map(data.estantes, function(item){
                        $('#iscomsercaMovimientoEstante').append($('<option>', {'value': item.id }).text(item.nombre));
                    });
                    $.unblockUI();
                });
            }
        });
        
        $('#iscomsercaMovimientoEstante').change(function (){
            $('#iscomsercaMovimientoRenglon').empty();
            $('#iscomsercaMovimientoRenglon').append($('<option>', {'value': '0'}).text('Escoja un renglón...'));
            if (parseInt($(this).val()) !== 0){
                $.blockUI({message: '<h3>Buscando renglones...</h3>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:
                    {
                        action: 'shoot',
                        screen: 'estantes',
                        function: 'buscarRenglones',
                        almacen: $('#iscomsercaMovimientoAlmacen').val(),
                        estante: $('#iscomsercaMovimientoEstante').val()
                    },
                }).done(function(data){
                    $.map(data.renglones, function(item){
                        $('#iscomsercaMovimientoRenglon').append($('<option>', {'value': item.id }).text(item.nombre));
                    });
                    $.unblockUI();
                });
            }
        });
        
        // Boton Guardar Movimiento
        $('#iscomsercaMover').on('click', function (){
            //$.blockUI({message: '<h1>Moviendo Equipo...</h1>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'equipos',
                    function: 'mover',
                    almacen: $('#iscomsercaMovimientoAlmacen').val(),
                    estante: $('#iscomsercaMovimientoEstante').val(),
                    renglon: $('#iscomsercaMovimientoRenglon').val(),
                    equipo: $('#iscomsercaMovimientoId').val()
                }
            }).done(function (data){
                
                for (var i = 0; i < $(".iscomsercaEquipoId").length; i++)
                {
                    if (parseInt($($(".iscomsercaEquipoId")[i]).val()) === parseInt($('#iscomsercaMovimientoId').val()))
                    {
                        $($(".iscomsercaAlmacenId")[i]).val($('#iscomsercaMovimientoAlmacen').val());
                        $($(".iscomsercaEstanteId")[i]).val($('#iscomsercaMovimientoEstante').val());
                        $($(".iscomsercaRenglonId")[i]).val($('#iscomsercaMovimientoRenglon').val());
                    }
                }
                //$('#iscomsercaMovimiento').dialog('close');
                $('#iscomsercaMovimiento').hide(300, function () {
                    
                    $('#iscomsercaMovimientoAlmacen').val('0');
                    $('#iscomsercaMovimientoEstante').empty();
                    $('#iscomsercaMovimientoRenglon').empty();
                    
                    $('#iscomsercaPrincipal').show(300, function(){
                        listar();
                        swal({
                            type: "success",
                            title: "Movimiento del Equipo",
                            text: "¡"+data.feedback+"!",
                            showConfirmButton: true,
                            confirmButtonText: "Ok"
                        });
                    });
                });
                
                
                
                //$('#feedback').html(data.feedback);
                //$('.toast').toast('show');
                
                ///$.unblockUI();
               
            });
        });
        
        // Boton Guardar Nota del Equipo
        $('#iscomsercaNotificar').on('click', function (){
            var privada = 0, msgCliente = 0;
            if($('#iscomsercaPrivada').is(':checked') === true){ privada = 1; }
            
            if(CKEDITOR.instances["iscomserca_nota"].getData() === ""){
                swal({
                    type: "warning",
                    title: "Agregar Nota",
                    text: "¡Debe ingresar la nota!",
                    showConfirmButton: true,
                    confirmButtonText: "Ok"
                }).then(function(result){
                    if(result.value){
                        CKEDITOR.instances["iscomserca_nota"].focus();                                
                    }
                });
            }
            else{
                //$.blockUI({message: '<h4>Agregando Nota...</h4>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:
                    {
                        action: 'shoot',
                        screen: 'equipos',
                        function: 'agregarNota',
                        equipo: $('#iscomsercaNotaId').val(),
                        nota: CKEDITOR.instances["iscomserca_nota"].getData(),
                        privada: privada
                        //msgcliente: msgCliente
                    }
                }).done(function (data){

                    $('#iscomsercaNota').hide(300, function (){
                        $('#iscomsercaPrivada').removeAttr('checked');
                        $('#iscomsercaNECliente').removeAttr('checked');

                        $('#iscomsercaPrincipal').show(300, function(){
                            listar();
                            swal({
                                type: "success",
                                title: "Movimiento del Equipo",
                                text: "¡"+data.feedback+"!",
                                showConfirmButton: true,
                                confirmButtonText: "Ok"
                            });
                        });
                    });

                });
                //$.unblockUI();
            }
        });
        
        // Boton Actualizar Estado
        $('#iscomsercaActualizar').on('click', function (){
            //$.blockUI({message: '<h4>Actualizando Estado de Equipo...</h4>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'equipos',
                    function: 'actualizarEstado',
                    equipo: $('#iscomsercaEstadoId').val(),
                    estado: $('#iscomsercaEstadoSelect').val()
                }
            }).done(function (data){
                for (var i = 0; i < $(".iscomsercaEquipoId").length; i++){
                    if (parseInt($($(".iscomsercaEquipoId")[i]).val()) === parseInt($('#iscomsercaEstadoId').val())){
                        $($(".iscomsercaEstadoId")[i]).val($('#iscomsercaEstadoSelect').val());
                        $($(".iscomsercaEstadoColumna")[i]).text($('#iscomsercaEstadoSelect option:selected').text());
                    }
                }
                $('#iscomsercaEstado').hide(300, function () {
                    $('#iscomsercaPrincipal').show(300, function(){
                        listar();
                        swal({
                            type: "success",
                            title: "Actualización de Estado del Equipo",
                            text: "¡"+data.feedback+"!",
                            showConfirmButton: true,
                            confirmButtonText: "Ok"
                        });
                    });
                });
                /*$('#feedback').html(data.feedback);
                $('.toast').toast('show');
                listar();*/
                //$.unblockUI();
            });
        });
        
        // Boton para guardar actualización del equipo
        $('#iscomsercaEditar').on('click', function(){
            //$.blockUI({message: '<h1>Actualizando del Equipo...</h1>'});
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data:{
                    action: 'shoot',
                    screen: 'equipos',
                    function: 'editar',
                    equipo: $('#iscomsercaEditarId').val(),
                    descripcion: $.trim($('#iscomsercaEditarDescripcion').val()),
                    serial: $.trim($('#iscomsercaEditarSerial').val()),
                    categoria: $('#iscomsercaEditarCategoria').val(),
                    marca: $('#iscomsercaEditarMarca').val()
                }
            }).done(function (data){
                /*for (var i = 0; i < $(".iscomsercaEquipoId").length; i++){
                    if (parseInt($($(".iscomsercaEquipoId")[i]).val()) === parseInt($('#iscomsercaEstadoId').val())){
                        $($(".iscomsercaEstadoId")[i]).val($('#iscomsercaEstadoSelect').val());
                        $($(".iscomsercaEstadoColumna")[i]).text($('#iscomsercaEstadoSelect option:selected').text());
                    }
                }*/
                //$('#feedback').html(data.feedback);
                //$('#iscomsercaEstado').dialog('close');
                $('#iscomsercaEdicion').hide(300, function () {
                    $('#iscomsercaPrincipal').show(300, function(){
                        listar();
                        swal({
                            type: "success",
                            title: "Actualización del Equipo",
                            text: "¡"+data.feedback+"!",
                            showConfirmButton: true,
                            confirmButtonText: "Ok"
                        });
                    });
                });
                /*$('#feedback').html(data.feedback);
                $('.toast').toast('show');
                listar();
                $.unblockUI();*/
            });
        });
        
        // Boton Asignar Tecnico
        $('#iscomsercaAsignar').on('click', function(){
            if($('#iscomsercaTecnicoSelect').val() === '0'){
                alert('Debe seleccionar un técnico');
                $('#iscomsercaTecnicoSelect').focus();
            }
            else{
                $.blockUI({message: '<h1>Asignando Técnico...</h1>'});
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    dataType: 'json',
                    data:{
                        action: 'shoot',
                        screen: 'equipos',
                        function: 'asignar',
                        equipo: $('#iscomsercaEquipoId').val(),
                        tecnico: $('#iscomsercaTecnicoSelect').val()
                    }
                }).done(function (data){
                    $('#iscomsercaTecnico').hide(300, function () {
                        $('#iscomsercaPrincipal').show(300);
                        $('#iscomsercaTecnicoSelect').val('0');
                    });
                    $('#feedback').html(data.feedback);
                    $('.toast').toast('show');
                    listar();
                    
                    $.unblockUI();
                });
            }
        });
    });
    
</script>