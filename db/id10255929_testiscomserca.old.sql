-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 03-01-2020 a las 16:39:56
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `id10255929_testiscomserca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Un comentarista de WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-08-14 04:54:29', '2017-08-14 10:54:29', 'Hola, esto es un comentario.\nPara empezar a moderar, editar y borrar comentarios, por favor, visita la pantalla de comentarios en el escritorio.\nLos avatares de las personas que hacen provienen de <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_icsc_almacen`
--

CREATE TABLE `wp_icsc_almacen` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wp_icsc_almacen`
--

INSERT INTO `wp_icsc_almacen` (`id`, `nombre`, `fecha_creacion`, `activo`) VALUES
(1, 'Almacén 1', '2017-08-14 15:58:10', 0),
(2, 'Almacén 2', '2017-08-20 07:07:07', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_icsc_categoria`
--

CREATE TABLE `wp_icsc_categoria` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `activa` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `wp_icsc_categoria`
--

INSERT INTO `wp_icsc_categoria` (`id`, `nombre`, `activa`) VALUES
(1, 'Televisores', 1),
(2, 'Laptops', 0),
(3, 'Smartphones', 0),
(4, 'Tabletas', 1),
(5, 'Consolas', 1),
(6, 'Desktops', 1),
(7, 'Scanners', 1),
(8, 'Impresoras', 1),
(9, 'Monitores', 1),
(10, 'Teclados', 1),
(11, 'Ratones', 1),
(12, 'Flash', 1),
(13, 'VHS', 1),
(14, 'Servidores', 1),
(15, 'Cámaras', 1),
(16, 'Discos Duros', 1),
(17, 'UPSs', 1),
(18, 'Fuentes de poder', 1),
(19, 'DVDs', 1),
(20, 'CD ROM', 1),
(21, 'GPS', 1),
(22, 'Fan coolers', 1),
(23, 'Tarjetas madre', 1),
(24, 'Tarjetas de video', 1),
(25, 'Router', 1),
(26, 'Smartphone', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_icsc_equipo`
--

CREATE TABLE `wp_icsc_equipo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `descripcion` varchar(80) NOT NULL,
  `serial` varchar(16) NOT NULL,
  `cliente_id` bigint(20) NOT NULL,
  `fecha_ingresado` datetime NOT NULL,
  `categoria_id` int(10) UNSIGNED NOT NULL,
  `marca_id` int(10) UNSIGNED NOT NULL,
  `estado` tinyint(4) NOT NULL COMMENT '1- Recibido 2- Esperando repuestos 3- En reparación 4-Reparado 5- Reparado y entregado 6- Entregado pero no reparado 7- Perdido',
  `fecha_entregado` datetime DEFAULT NULL,
  `tecnico_id` bigint(20) DEFAULT NULL COMMENT 'Técnico Asignado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wp_icsc_equipo`
--

INSERT INTO `wp_icsc_equipo` (`id`, `descripcion`, `serial`, `cliente_id`, `fecha_ingresado`, `categoria_id`, `marca_id`, `estado`, `fecha_entregado`, `tecnico_id`) VALUES
(1, 'Televisor de 30 pulgadas', '', 1, '2017-09-16 14:24:24', 18, 1, 4, NULL, 0),
(2, 'Tableta Android', '', 0, '2017-09-16 14:39:47', 4, 2, 1, NULL, 0),
(4, 'Nexus 4', '', 1, '2017-09-17 22:14:55', 3, 1, 2, NULL, 0),
(6, 'Galaxy 8', '', 2, '2017-09-18 01:08:19', 3, 2, 1, NULL, 2),
(7, 'Galaxy 12', '', 2, '2017-09-18 01:26:03', 4, 2, 1, NULL, 0),
(8, 'FXC-900', 'FWC19922DG', 2, '2019-07-04 03:57:45', 9, 2, 3, NULL, 2),
(9, 'DWL-G700AP', 'F32T18A0001100', 5, '2019-07-04 04:06:24', 25, 4, 1, NULL, 7),
(10, 'GJK-123456', 'FG1234589A', 6, '2019-07-04 08:24:59', 2, 1, 1, NULL, 0),
(11, 'GT72+', 'JLSJA344560112', 4, '2019-07-09 07:59:51', 26, 5, 1, NULL, 7),
(13, 'DOK-K5313', 'KBC921K11499A', 5, '2019-07-09 14:04:17', 21, 3, 3, NULL, 7),
(14, 'LPE2', '', 4, '2019-07-17 08:21:09', 16, 7, 3, NULL, 0),
(15, 'JKL-1000', 'FNB123456789', 5, '2019-07-22 12:56:19', 17, 1, 1, NULL, 2),
(16, 'M-3500', 'SM12345698745', 5, '2019-08-14 09:18:16', 17, 2, 1, NULL, 9),
(17, 'M-3698', 'SM785964561', 10, '2019-08-14 09:37:32', 14, 3, 1, NULL, 9),
(18, 'M-654', 'SM456213785285', 11, '2019-08-14 10:24:19', 6, 3, 1, NULL, 9),
(19, 'M-3654', 'SM14523687965', 12, '2019-08-14 13:34:59', 2, 2, 1, NULL, 2),
(20, 'M-3698', 'SM1234567', 5, '2019-08-21 14:00:45', 18, 2, 1, NULL, 7),
(21, 'M1254', 'SM1234567987', 5, '2019-09-06 16:49:54', 10, 2, 1, NULL, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_icsc_equipo_movimientos`
--

CREATE TABLE `wp_icsc_equipo_movimientos` (
  `id` int(10) UNSIGNED NOT NULL,
  `equipo_id` bigint(20) UNSIGNED NOT NULL,
  `almacen_id` int(10) UNSIGNED NOT NULL,
  `estante_id` int(10) UNSIGNED NOT NULL,
  `renglon_id` int(10) UNSIGNED NOT NULL,
  `fecha_movimiento` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wp_icsc_equipo_movimientos`
--

INSERT INTO `wp_icsc_equipo_movimientos` (`id`, `equipo_id`, `almacen_id`, `estante_id`, `renglon_id`, `fecha_movimiento`) VALUES
(1, 1, 1, 1, 1, '2017-09-16 14:24:24'),
(1, 2, 1, 1, 2, '2017-09-16 14:39:47'),
(1, 4, 1, 1, 1, '2017-09-17 22:14:55'),
(1, 6, 1, 1, 2, '2017-09-18 01:08:19'),
(1, 7, 1, 1, 3, '2017-09-18 01:26:03'),
(1, 8, 2, 1, 1, '2019-07-04 03:57:45'),
(1, 9, 1, 1, 2, '2019-07-04 04:06:24'),
(1, 10, 1, 1, 2, '2019-07-04 08:24:59'),
(1, 11, 1, 1, 2, '2019-07-09 07:59:51'),
(1, 13, 1, 1, 1, '2019-07-09 14:04:17'),
(1, 14, 1, 1, 2, '2019-07-17 08:21:09'),
(1, 15, 1, 1, 1, '2019-07-22 12:56:19'),
(1, 16, 1, 1, 2, '2019-08-14 09:18:16'),
(1, 17, 2, 1, 2, '2019-08-14 09:37:32'),
(1, 18, 2, 1, 1, '2019-08-14 10:24:19'),
(1, 19, 2, 1, 1, '2019-08-14 13:34:59'),
(1, 20, 2, 1, 1, '2019-08-21 14:00:45'),
(1, 21, 2, 1, 1, '2019-09-06 16:49:54'),
(2, 9, 2, 1, 1, '2019-08-26 10:04:14'),
(2, 13, 2, 1, 2, '2019-07-16 13:49:39'),
(3, 13, 1, 1, 2, '2019-07-16 13:53:26'),
(4, 13, 1, 1, 3, '2019-07-16 16:38:04'),
(5, 13, 2, 1, 1, '2019-07-16 16:40:01'),
(6, 13, 2, 1, 2, '2019-07-29 09:45:33'),
(7, 13, 2, 1, 1, '2019-07-29 09:58:29'),
(8, 13, 2, 1, 1, '2019-07-29 10:09:51'),
(9, 13, 2, 1, 1, '2019-07-29 10:10:42'),
(10, 13, 2, 1, 2, '2019-07-29 10:11:10'),
(11, 13, 2, 1, 2, '2019-08-26 10:05:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_icsc_equipo_notas`
--

CREATE TABLE `wp_icsc_equipo_notas` (
  `id` int(10) UNSIGNED NOT NULL,
  `equipo_id` bigint(20) UNSIGNED NOT NULL,
  `nota` text NOT NULL,
  `privada` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wp_icsc_equipo_notas`
--

INSERT INTO `wp_icsc_equipo_notas` (`id`, `equipo_id`, `nota`, `privada`) VALUES
(1, 1, '<p>Prob<strong>an<em>do</em>...<img class=\"alignnone size-medium wp-image-5\" src=\"http://localhost/iscomserca/wpiscomserca/wp-content/uploads/2017/10/3635627339_aea35a6edf-300x225.jpg\" alt=\"\" width=\"300\" height=\"225\" /></strong></p>', 0),
(1, 7, '<p>hola</p>', 0),
(1, 8, '<p>Esto es una prueba beta</p>', 1),
(1, 9, '<p>hola es una prueba</p>', 1),
(2, 9, '<p>dddddde</p>', 1),
(3, 9, '<p>dddeffefefefef</p>', 0),
(4, 9, '<p>juhhuhhh</p>', 1),
(5, 9, '<p>HOla es enviado</p>', 0),
(1, 10, '<p><strong>Hola&nbsp;</strong>esto es una&nbsp;<em>prueba</em></p>', 1),
(2, 10, '<p style=\"text-align: center;\"><strong>Prueba</strong></p>\n<p style=\"text-align: justify;\">Siendo esto una prueba es lo mejor.</p>', 1),
(3, 10, '<p>dddeddedededededefrgrgr</p>', 1),
(4, 10, '<p>deffefefef</p>', 0),
(1, 11, '<p>ddddedhhthth</p>', 1),
(2, 11, '<h1 style=\"text-align: center;\">Prueba de Nota</h1>\n<p style=\"text-align: justify;\">Me parece interesante esto de prueba. <a title=\"Youtube\" href=\"https://www.youtube.com/\" target=\"_blank\" rel=\"noopener\">https://www.youtube.com/</a></p>', 1),
(3, 11, '<p>dededeeffef</p>', 0),
(1, 13, 'algo', 1),
(2, 13, 'algo', 1),
(3, 13, '', 1),
(4, 13, '<p><code>&lt;html&gt;&lt;head&gt;&lt;/head&gt;&lt;body&gt;Prueba de Envio&lt;/body&gt;&lt;/html&gt;</code></p>', 1),
(5, 13, '<p>Esto es una prueba</p>', 1),
(6, 13, '<p>Esto es una prueba de envio 44</p>', 1),
(7, 13, '<p>hola una prueba</p>', 1),
(8, 13, '<p>edededehhduhuduhhihhui</p>', 0),
(9, 13, '<p>dededededlodjedjjoejodedoejeoj</p>', 1),
(10, 13, '<p>Esto es una prueba de envio</p>', 1),
(1, 14, '<p>jfhefheffghfghweffhgwefwffwwef</p>', 1),
(2, 14, '<p>Esto es una prueba de envio</p>', 1),
(3, 14, '<p>Esto es una prueba de envio.</p>', 0),
(4, 14, '<p>ddededfefef</p>', 0),
(5, 14, '<p>defefffefefef</p>', 0),
(6, 14, '<p>defefefefefefe</p>', 0),
(7, 14, '<p>dedeededeededede</p>', 0),
(8, 14, '<p>deddefefegegege</p>', 0),
(9, 14, '<p>44htrhhtjjjyj</p>', 0),
(10, 14, '<p>dehjfejhjfkhkjfhfehjfkjfejf</p>', 0),
(1, 15, '<p>Esto es una prueba</p>', 0),
(2, 15, '<p>hola una prueba de fuego</p>', 0),
(3, 15, '<p>PRUEBA DE FUEGO #2</p>', 0),
(4, 15, '<p>HOLA ESTO ES UNA PRUEBA DE FUEGO</p>', 0),
(5, 15, '<p><strong>ESTO ES UNA PRUEBA DE&nbsp;</strong>FUEGO CON&nbsp;<strong>BOLD</strong></p>', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_icsc_estante`
--

CREATE TABLE `wp_icsc_estante` (
  `id` int(10) UNSIGNED NOT NULL,
  `almacen_id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wp_icsc_estante`
--

INSERT INTO `wp_icsc_estante` (`id`, `almacen_id`, `nombre`, `activo`) VALUES
(1, 1, 'Primer estante', 1),
(2, 1, 'Norte-Este', 1),
(3, 1, 'xxxx', 1),
(1, 2, 'Primer Estante', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_icsc_marca`
--

CREATE TABLE `wp_icsc_marca` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wp_icsc_marca`
--

INSERT INTO `wp_icsc_marca` (`id`, `nombre`) VALUES
(1, 'Panasonic/Matsushita'),
(2, 'Samsung'),
(3, 'Apple'),
(4, 'D-LINK'),
(5, 'CUBOT'),
(6, 'Toshiba'),
(7, 'Maxtor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_icsc_renglon`
--

CREATE TABLE `wp_icsc_renglon` (
  `id` int(10) UNSIGNED NOT NULL,
  `estante_id` int(10) UNSIGNED NOT NULL,
  `almacen_id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `capacidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wp_icsc_renglon`
--

INSERT INTO `wp_icsc_renglon` (`id`, `estante_id`, `almacen_id`, `nombre`, `capacidad`) VALUES
(1, 1, 1, 'Alto techo', 6),
(2, 1, 1, 'Medio', 7),
(3, 1, 1, 'Bajo - piso', 10),
(1, 2, 1, 'Norte', 100),
(2, 2, 1, 'Sur', 100),
(1, 3, 1, 'xxxxx', 10),
(1, 1, 2, 'Norte', 100),
(2, 1, 2, 'Sur', 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/iscomserca', 'yes'),
(2, 'home', 'http://localhost/iscomserca', 'yes'),
(3, 'blogname', 'Iscomserca', 'yes'),
(4, 'blogdescription', 'Todo en un Solo Lugar', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'christianh_meier@hotmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'j F, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:88:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:1;s:32:\"iscomserca/IscomsercaEquipos.php\";i:2;s:17:\"so-css/so-css.php\";i:3;s:23:\"subtitles/subtitles.php\";i:4;s:33:\"wps-hide-login/wps-hide-login.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '-6', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:76:\"/storage/ssd2/572/3667572/public_html/wp-content/themes/iscomserca/style.css\";i:2;s:75:\"/storage/ssd2/572/3667572/public_html/wp-content/themes/iscomserca/page.php\";i:3;s:77:\"/storage/ssd2/572/3667572/public_html/wp-content/themes/iscomserca/footer.php\";i:4;s:80:\"/storage/ssd2/572/3667572/public_html/wp-content/themes/iscomserca/functions.php\";i:5;s:67:\"C:\\xampp\\htdocs\\wpiscomserca/wp-content/themes/iscomserca/style.css\";}', 'no'),
(40, 'template', 'edsbootstrap', 'yes'),
(41, 'stylesheet', 'iscomserca', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '13', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:63:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:17:\"manage_iscomserca\";b:1;s:15:\"edit_iscomserca\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"edit_iscomserca\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:11:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:15:\"auth_iscomserca\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:6:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:15:\"cola_iscomserca\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'es_VE', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:6:\"footer\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'cron', 'a:7:{i:1578065235;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1578087703;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1578088466;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1578092071;a:3:{s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1578135322;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1578138004;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(110, 'nonce_key', '+3BI0[dH(nrq!JqH= n~`aqx[+k0Zirkkn%$>PaB0rmw|Q H[wnMmoY$a]U8,b2D', 'no'),
(111, 'nonce_salt', 'SRM1GisC%x,;+6ReE>WO8Ne[|QRnh7G*zNLUtc5en9~`ICT<Gp y3}[`FV,Zu<-A', 'no'),
(112, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1508902277;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(126, 'auth_key', 'h:Tau6wUnMuqX[]P=2eeo=xBE:dh?kqD)n7O+ c$Kc[hv_iSV1[2,o|MSXjdRV<:', 'no'),
(127, 'auth_salt', 'Dh08yJ2GBFpr_p3&&# JUjLWqH@nN@^7I`uBAmuO8Dnaa%ub31}ob@0_<w|>XKtO', 'no'),
(128, 'logged_in_key', '2l=.fG,0~mKE,T75)&3v>M^J9fp&Pwx4~bgzP2*OPF{lsch!bAy41LbRIKV`nrf5', 'no'),
(129, 'logged_in_salt', 'k6wlk]NdAl)s_DH=,Uf64.`xa)?CX/qPi6-R9-Pkz;P9s9uQ0`kuS!QKh$`g,Sk$', 'no'),
(148, 'recently_activated', 'a:0:{}', 'yes'),
(159, 'iscomserca_database', '2', 'yes'),
(669, 'current_theme', 'Iscomserca', 'yes'),
(670, 'theme_mods_alante', 'a:3:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1508904162;s:4:\"data\";a:8:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"footer-w1\";a:0:{}s:9:\"footer-w2\";a:0:{}s:9:\"footer-w3\";N;s:9:\"footer-w4\";N;s:9:\"footer-w5\";N;s:9:\"footer-w6\";N;}}}', 'yes'),
(671, 'theme_switched', '', 'yes'),
(676, 'theme_mods_edsbootstrap', 'a:6:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;s:12:\"header_image\";s:74:\"http://localhost/iscomserca/wpiscomserca/wp-content/uploads/2017/10/cropped-ICS-3.png\";s:17:\"header_image_data\";O:8:\"stdClass\":5:{s:13:\"attachment_id\";i:10;s:3:\"url\";s:74:\"http://localhost/iscomserca/wpiscomserca/wp-content/uploads/2017/10/cropped-ICS-3.png\";s:13:\"thumbnail_url\";s:74:\"http://localhost/iscomserca/wpiscomserca/wp-content/uploads/2017/10/cropped-ICS-3.png\";s:6:\"height\";i:699;s:5:\"width\";i:1000;}s:11:\"custom_logo\";s:0:\"\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1508911385;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:6:\"footer\";a:0:{}}}}', 'yes'),
(677, 'edsbootstrap_admin_notice_welcome', '1', 'yes'),
(684, 'theme_mods_iscomserca', 'a:4:{s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:10;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:26:\"edsbootstrap_theme_options\";a:2:{s:6:\"social\";a:2:{s:11:\"fa-facebook\";s:36:\"https://www.facebook.com/ISCOMSERCA/\";s:10:\"fa-twitter\";s:30:\"https://twitter.com/ISCOMSERCA\";}s:6:\"footer\";a:1:{s:9:\"copyright\";s:44:\"Iscomserca CA, todos los derechos reservados\";}}}', 'yes'),
(760, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(794, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(937, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:28:\"christianh_meier@hotmail.com\";s:7:\"version\";s:5:\"5.2.5\";s:9:\"timestamp\";i:1576932385;}', 'no'),
(2027, 'new_admin_email', 'christianh_meier@hotmail.com', 'yes'),
(2028, 'whl_page', 'admin-login', 'yes'),
(2029, 'whl_redirect_admin', '404', 'yes'),
(2040, 'dnh_dismissed_notices', 'a:1:{i:0;s:24:\"wrm_1e278f4992d8bb3f1f0b\";}', 'yes'),
(2177, 'wp_mail_smtp_initial_version', '1.5.2', 'no'),
(2178, 'wp_mail_smtp_version', '1.5.2', 'no'),
(2179, 'wp_mail_smtp', 'a:5:{s:4:\"mail\";a:6:{s:10:\"from_email\";s:28:\"dacostaelecom.2016@gmail.com\";s:9:\"from_name\";s:10:\"Iscomserca\";s:6:\"mailer\";s:4:\"smtp\";s:11:\"return_path\";b:1;s:16:\"from_email_force\";b:1;s:15:\"from_name_force\";b:1;}s:4:\"smtp\";a:7:{s:7:\"autotls\";b:1;s:4:\"auth\";b:1;s:4:\"host\";s:14:\"smtp.gmail.com\";s:10:\"encryption\";s:3:\"ssl\";s:4:\"port\";i:465;s:4:\"user\";s:28:\"dacostaelecom.2016@gmail.com\";s:4:\"pass\";s:11:\"15vacas42&&\";}s:5:\"gmail\";a:2:{s:9:\"client_id\";s:0:\"\";s:13:\"client_secret\";s:0:\"\";}s:7:\"mailgun\";a:3:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";s:6:\"region\";s:2:\"US\";}s:8:\"sendgrid\";a:1:{s:7:\"api_key\";s:0:\"\";}}', 'no'),
(2180, '_amn_smtp_last_checked', '1563408000', 'yes'),
(2181, 'wp_mail_smtp_debug', 'a:0:{}', 'no'),
(2229, 'swpsmtp_options', 'a:9:{s:16:\"from_email_field\";s:23:\"dacostaelecom@gmail.com\";s:15:\"from_name_field\";s:12:\"Dixon Acosta\";s:23:\"force_from_name_replace\";b:0;s:13:\"smtp_settings\";a:10:{s:4:\"host\";s:14:\"smtp.gmail.com\";s:15:\"type_encryption\";s:3:\"tls\";s:4:\"port\";s:3:\"587\";s:13:\"autentication\";s:3:\"yes\";s:8:\"username\";s:23:\"dacostaelecom@gmail.com\";s:8:\"password\";s:24:\"MTUyODc0NDIyMzA2ODImJg==\";s:12:\"enable_debug\";i:1;s:12:\"insecure_ssl\";b:0;s:12:\"encrypt_pass\";b:0;s:13:\"log_file_name\";s:27:\"5d3f028e01e84_debug_log.txt\";}s:15:\"allowed_domains\";s:9:\"localhost\";s:14:\"reply_to_email\";s:0:\"\";s:17:\"email_ignore_list\";s:0:\"\";s:19:\"enable_domain_check\";i:1;s:16:\"block_all_emails\";b:0;}', 'yes'),
(2230, 'smtp_test_mail', 'a:3:{s:10:\"swpsmtp_to\";s:23:\"dacostaelecom@gmail.com\";s:15:\"swpsmtp_subject\";s:6:\"Prueba\";s:15:\"swpsmtp_message\";s:18:\"Esto es una prueba\";}', 'yes'),
(2449, 'recovery_keys', 'a:0:{}', 'yes'),
(2451, 'wp_page_for_privacy_policy', '0', 'yes'),
(2452, 'show_comments_cookies_opt_in', '1', 'yes'),
(2453, 'db_upgraded', '', 'yes'),
(2517, 'can_compress_scripts', '0', 'no'),
(2532, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/es_VE/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"es_VE\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/es_VE/wordpress-5.3.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/es_VE/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"es_VE\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/es_VE/wordpress-5.3.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1578062019;s:15:\"version_checked\";s:5:\"5.2.5\";s:12:\"translations\";a:0:{}}', 'no'),
(2536, '_site_transient_timeout_theme_roots', '1578063818', 'no'),
(2537, '_site_transient_theme_roots', 'a:7:{s:6:\"alante\";s:7:\"/themes\";s:12:\"edsbootstrap\";s:7:\"/themes\";s:10:\"iscomserca\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(2539, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1578062019;s:7:\"checked\";a:7:{s:6:\"alante\";s:5:\"1.1.2\";s:12:\"edsbootstrap\";s:3:\"1.9\";s:10:\"iscomserca\";s:3:\"1.0\";s:13:\"twentyfifteen\";s:3:\"1.8\";s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"1.3\";s:13:\"twentysixteen\";s:3:\"1.3\";}s:8:\"response\";a:5:{s:6:\"alante\";a:6:{s:5:\"theme\";s:6:\"alante\";s:11:\"new_version\";s:5:\"1.1.9\";s:3:\"url\";s:36:\"https://wordpress.org/themes/alante/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/theme/alante.1.1.9.zip\";s:8:\"requires\";s:3:\"4.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"edsbootstrap\";a:6:{s:5:\"theme\";s:12:\"edsbootstrap\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:42:\"https://wordpress.org/themes/edsbootstrap/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/edsbootstrap.2.0.zip\";s:8:\"requires\";s:3:\"4.0\";s:12:\"requires_php\";b:0;}s:13:\"twentyfifteen\";a:6:{s:5:\"theme\";s:13:\"twentyfifteen\";s:11:\"new_version\";s:3:\"2.5\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentyfifteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentyfifteen.2.5.zip\";s:8:\"requires\";s:3:\"4.1\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.2\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.2.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:13:\"twentysixteen\";a:6:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.2.0.zip\";s:8:\"requires\";s:3:\"4.4\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(2540, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1578062019;s:8:\"response\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.3\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"updraftplus/updraftplus.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/updraftplus\";s:4:\"slug\";s:11:\"updraftplus\";s:6:\"plugin\";s:27:\"updraftplus/updraftplus.php\";s:11:\"new_version\";s:7:\"1.16.21\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/updraftplus/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/updraftplus.1.16.21.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/updraftplus/assets/icon-256x256.jpg?rev=1686200\";s:2:\"1x\";s:64:\"https://ps.w.org/updraftplus/assets/icon-128x128.jpg?rev=1686200\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/updraftplus/assets/banner-1544x500.png?rev=1686200\";s:2:\"1x\";s:66:\"https://ps.w.org/updraftplus/assets/banner-772x250.png?rev=1686200\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:21:\"hello-dolly/hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:21:\"hello-dolly/hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:17:\"so-css/so-css.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:20:\"w.org/plugins/so-css\";s:4:\"slug\";s:6:\"so-css\";s:6:\"plugin\";s:17:\"so-css/so-css.php\";s:11:\"new_version\";s:5:\"1.2.4\";s:3:\"url\";s:37:\"https://wordpress.org/plugins/so-css/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/plugin/so-css.1.2.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/so-css/assets/icon-256x256.png?rev=1172601\";s:2:\"1x\";s:59:\"https://ps.w.org/so-css/assets/icon-128x128.png?rev=1172601\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/so-css/assets/banner-772x250.jpg?rev=1172601\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"subtitles/subtitles.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/subtitles\";s:4:\"slug\";s:9:\"subtitles\";s:6:\"plugin\";s:23:\"subtitles/subtitles.php\";s:11:\"new_version\";s:5:\"3.0.0\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/subtitles/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/subtitles.3.0.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/subtitles/assets/icon-256x256.png?rev=1063614\";s:2:\"1x\";s:62:\"https://ps.w.org/subtitles/assets/icon-128x128.png?rev=1063614\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/subtitles/assets/banner-1544x500.png?rev=1063614\";s:2:\"1x\";s:64:\"https://ps.w.org/subtitles/assets/banner-772x250.png?rev=1063614\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"wps-hide-login/wps-hide-login.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/wps-hide-login\";s:4:\"slug\";s:14:\"wps-hide-login\";s:6:\"plugin\";s:33:\"wps-hide-login/wps-hide-login.php\";s:11:\"new_version\";s:7:\"1.5.4.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wps-hide-login/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/wps-hide-login.1.5.4.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wps-hide-login/assets/icon-256x256.png?rev=1820667\";s:2:\"1x\";s:67:\"https://ps.w.org/wps-hide-login/assets/icon-128x128.png?rev=1820667\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wps-hide-login/assets/banner-1544x500.jpg?rev=1820667\";s:2:\"1x\";s:69:\"https://ps.w.org/wps-hide-login/assets/banner-772x250.jpg?rev=1820667\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 5, '_wp_attached_file', '2017/10/3635627339_aea35a6edf.jpg'),
(3, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:375;s:4:\"file\";s:33:\"2017/10/3635627339_aea35a6edf.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"3635627339_aea35a6edf-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"3635627339_aea35a6edf-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:33:\"3635627339_aea35a6edf-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(4, 2, '_edit_last', '1'),
(5, 2, '_edit_lock', '1562184067:1'),
(8, 9, '_wp_attached_file', '2017/10/ICS-3.png'),
(9, 9, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1886;s:6:\"height\";i:1319;s:4:\"file\";s:17:\"2017/10/ICS-3.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"ICS-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"ICS-3-300x210.png\";s:5:\"width\";i:300;s:6:\"height\";i:210;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"ICS-3-768x537.png\";s:5:\"width\";i:768;s:6:\"height\";i:537;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"ICS-3-1024x716.png\";s:5:\"width\";i:1024;s:6:\"height\";i:716;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(10, 10, '_wp_attached_file', '2017/10/cropped-ICS-3.png'),
(11, 10, '_wp_attachment_context', 'custom-header'),
(12, 10, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:699;s:4:\"file\";s:25:\"2017/10/cropped-ICS-3.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"cropped-ICS-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"cropped-ICS-3-300x210.png\";s:5:\"width\";i:300;s:6:\"height\";i:210;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"cropped-ICS-3-768x537.png\";s:5:\"width\";i:768;s:6:\"height\";i:537;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(13, 10, '_wp_attachment_custom_header_last_used_edsbootstrap', '1508904302'),
(14, 10, '_wp_attachment_is_custom_header', 'edsbootstrap'),
(17, 13, '_wp_attached_file', '2017/10/cropped-ICS-3-1.png'),
(18, 13, '_wp_attachment_context', 'site-icon'),
(19, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:27:\"2017/10/cropped-ICS-3-1.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"cropped-ICS-3-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"cropped-ICS-3-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:27:\"cropped-ICS-3-1-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:27:\"cropped-ICS-3-1-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:27:\"cropped-ICS-3-1-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:25:\"cropped-ICS-3-1-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(30, 29, '_edit_lock', '1562146162:1'),
(31, 29, '_edit_last', '1'),
(32, 32, '_menu_item_type', 'post_type'),
(33, 32, '_menu_item_menu_item_parent', '0'),
(34, 32, '_menu_item_object_id', '2'),
(35, 32, '_menu_item_object', 'page'),
(36, 32, '_menu_item_target', ''),
(37, 32, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(38, 32, '_menu_item_xfn', ''),
(39, 32, '_menu_item_url', ''),
(50, 34, '_menu_item_type', 'post_type'),
(51, 34, '_menu_item_menu_item_parent', '0'),
(52, 34, '_menu_item_object_id', '29'),
(53, 34, '_menu_item_object', 'page'),
(54, 34, '_menu_item_target', ''),
(55, 34, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(56, 34, '_menu_item_xfn', ''),
(57, 34, '_menu_item_url', ''),
(71, 43, '_wp_attached_file', '2018/02/atencion-cliente.png'),
(72, 43, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:638;s:6:\"height\";i:299;s:4:\"file\";s:28:\"2018/02/atencion-cliente.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"atencion-cliente-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"atencion-cliente-300x141.png\";s:5:\"width\";i:300;s:6:\"height\";i:141;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(73, 44, '_wp_attached_file', '2018/02/reparamos.png'),
(74, 44, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1023;s:6:\"height\";i:647;s:4:\"file\";s:21:\"2018/02/reparamos.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"reparamos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"reparamos-300x190.png\";s:5:\"width\";i:300;s:6:\"height\";i:190;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"reparamos-768x486.png\";s:5:\"width\";i:768;s:6:\"height\";i:486;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(76, 51, '_wp_attached_file', '2018/05/imagen-3-copia.png'),
(77, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:585;s:6:\"height\";i:280;s:4:\"file\";s:26:\"2018/05/imagen-3-copia.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"imagen-3-copia-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"imagen-3-copia-300x144.png\";s:5:\"width\";i:300;s:6:\"height\";i:144;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(78, 52, '_wp_attached_file', '2018/05/imagen-4-copia.png'),
(79, 52, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:585;s:6:\"height\";i:280;s:4:\"file\";s:26:\"2018/05/imagen-4-copia.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"imagen-4-copia-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"imagen-4-copia-300x144.png\";s:5:\"width\";i:300;s:6:\"height\";i:144;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(80, 53, '_wp_attached_file', '2018/05/logos-redes-copia-facebook.png'),
(81, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:38:\"2018/05/logos-redes-copia-facebook.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"logos-redes-copia-facebook-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"logos-redes-copia-facebook-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(82, 54, '_wp_attached_file', '2018/05/logos-redes-copia-instagram.png'),
(83, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:39:\"2018/05/logos-redes-copia-instagram.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:39:\"logos-redes-copia-instagram-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:39:\"logos-redes-copia-instagram-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(84, 55, '_wp_attached_file', '2018/05/logos-redes-copia-twitter.png'),
(85, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:37:\"2018/05/logos-redes-copia-twitter.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"logos-redes-copia-twitter-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"logos-redes-copia-twitter-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(86, 56, '_wp_attached_file', '2018/05/slider-copia.png'),
(87, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1289;s:6:\"height\";i:705;s:4:\"file\";s:24:\"2018/05/slider-copia.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"slider-copia-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"slider-copia-300x164.png\";s:5:\"width\";i:300;s:6:\"height\";i:164;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"slider-copia-768x420.png\";s:5:\"width\";i:768;s:6:\"height\";i:420;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"slider-copia-1024x560.png\";s:5:\"width\";i:1024;s:6:\"height\";i:560;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(88, 57, '_wp_attached_file', '2018/05/fondo-copia-e1525969633386.png'),
(89, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:206;s:6:\"height\";i:200;s:4:\"file\";s:38:\"2018/05/fondo-copia-e1525969633386.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"fondo-copia-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"fondo-copia-300x291.png\";s:5:\"width\";i:300;s:6:\"height\";i:291;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"fondo-copia-768x745.png\";s:5:\"width\";i:768;s:6:\"height\";i:745;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"fondo-copia-1024x993.png\";s:5:\"width\";i:1024;s:6:\"height\";i:993;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(90, 58, '_wp_attached_file', '2018/05/imagen-1-copia.png'),
(91, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:585;s:6:\"height\";i:280;s:4:\"file\";s:26:\"2018/05/imagen-1-copia.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"imagen-1-copia-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"imagen-1-copia-300x144.png\";s:5:\"width\";i:300;s:6:\"height\";i:144;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(92, 59, '_wp_attached_file', '2018/05/imagen-2-copia.png'),
(93, 59, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:585;s:6:\"height\";i:280;s:4:\"file\";s:26:\"2018/05/imagen-2-copia.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"imagen-2-copia-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"imagen-2-copia-300x144.png\";s:5:\"width\";i:300;s:6:\"height\";i:144;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(94, 56, '_edit_lock', '1525965009:1'),
(95, 57, '_wp_attachment_backup_sizes', 'a:1:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:1349;s:6:\"height\";i:1308;s:4:\"file\";s:15:\"fondo-copia.png\";}}'),
(96, 63, '_wp_attached_file', '2018/05/pieza-fondo.png'),
(97, 63, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:225;s:6:\"height\";i:110;s:4:\"file\";s:23:\"2018/05/pieza-fondo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"pieza-fondo-150x110.png\";s:5:\"width\";i:150;s:6:\"height\";i:110;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(98, 63, '_edit_lock', '1525970982:1'),
(99, 64, '_wp_attached_file', '2018/05/facebook.png'),
(100, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:252;s:6:\"height\";i:253;s:4:\"file\";s:20:\"2018/05/facebook.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"facebook-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(101, 65, '_wp_attached_file', '2018/05/instagram.png'),
(102, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:252;s:6:\"height\";i:253;s:4:\"file\";s:21:\"2018/05/instagram.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"instagram-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(103, 66, '_wp_attached_file', '2018/05/twitter.png'),
(104, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:252;s:6:\"height\";i:253;s:4:\"file\";s:19:\"2018/05/twitter.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"twitter-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(105, 4, '_edit_lock', '1562237287:1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-08-14 04:54:29', '2017-08-14 10:54:29', 'Bienvenido a WordPress. Esta es tu primera entrada. EdÃ­tala o bÃ³rrala, Â¡y comienza a publicar!.', 'Â¡Hola mundo!', '', 'publish', 'open', 'open', '', 'hola-mundo', '', '', '2017-08-14 04:54:29', '2017-08-14 10:54:29', '', 0, 'http://localhost/iscomserca/?p=1', 0, 'post', '', 1),
(2, 1, '2017-08-14 04:54:29', '2017-08-14 10:54:29', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'publish', 'closed', 'closed', '', 'inicio', '', '', '2018-06-05 09:40:38', '2018-06-05 15:40:38', '', 0, 'http://localhost/iscomserca/?page_id=2', 0, 'page', '', 0),
(4, 1, '2017-08-14 04:58:42', '2017-08-14 10:58:42', '[iscomserca-estado]', 'Status de Equipo', '', 'publish', 'closed', 'closed', '', 'iscomserca-estado', '', '', '2017-08-14 04:58:42', '2017-08-14 10:58:42', '', 0, 'http://localhost/iscomserca/iscomserca-estado/', 0, 'page', '', 0),
(5, 1, '2017-10-06 16:48:14', '2017-10-06 22:48:14', '', '3635627339_aea35a6edf', '', 'inherit', 'open', 'closed', '', '3635627339_aea35a6edf', '', '', '2017-10-06 16:48:14', '2017-10-06 22:48:14', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2017/10/3635627339_aea35a6edf.jpg', 0, 'attachment', 'image/jpeg', 0),
(6, 1, '2017-10-24 21:17:35', '2017-10-25 03:17:35', 'Esta es una pÃ¡gina de ejemplo, Es diferente a una entrada de un blog porque se mantiene estÃ¡tica y, en la mayorÃ­a de temas, se mostrarÃ¡ en la barra de navegaciÃ³n. Casi todo el mundo comienza con una pÃ¡gina Sobre mÃ­ para presentarse a los potenciales visitantes. Puede decir algo asÃ­:\n\n<blockquote>Â¡Hola!: Soy fÃ­sico durante el dÃ­a, lector de manga por las noches y este es mi blog. Vivo en Albacete y tengo un gato llamado Alex. Me encantan los mojitos (y mirar a la gente corriendo en los parques)</blockquote>\n\nO algo asÃ­:\n\n<blockquote>La empresa Calcetines XYC se fundÃ³ en 1973, y ha estado produciendo calcetines de calidad para sus clientes desde entonces. Se encuentra en Vetusta, tiene unos 2.000 empleados e intenta ayudar en lo que puede para mejorar la vida en Vestusta</blockquote>\n\nDeberÃ­as ir a <a href=\"http://gomibako-touch/wpiscomserca/wp-admin/\">tu escritorio</a>, borrar esta pÃ¡gina y crear algunas nuevas con tu contenido. Â¡A divertirse!', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-10-24 21:17:35', '2017-10-25 03:17:35', '', 2, 'http://localhost/iscomserca/2017/10/24/2-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2017-10-24 22:04:43', '2017-10-25 04:04:43', '', 'ICS-3', '', 'inherit', 'open', 'closed', '', 'ics-3', '', '', '2017-10-24 22:04:43', '2017-10-25 04:04:43', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2017/10/ICS-3.png', 0, 'attachment', 'image/png', 0),
(10, 1, '2017-10-24 22:04:57', '2017-10-25 04:04:57', '', 'cropped-ICS-3.png', '', 'inherit', 'open', 'closed', '', 'cropped-ics-3-png', '', '', '2017-10-24 22:04:57', '2017-10-25 04:04:57', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2017/10/cropped-ICS-3.png', 0, 'attachment', 'image/png', 0),
(13, 1, '2017-10-24 22:06:43', '2017-10-25 04:06:43', 'http://localhost/iscomserca/wp-content/uploads/2017/10/cropped-ICS-3-1.png', 'cropped-ICS-3-1.png', '', 'inherit', 'open', 'closed', '', 'cropped-ics-3-1-png', '', '', '2017-10-24 22:06:43', '2017-10-25 04:06:43', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2017/10/cropped-ICS-3-1.png', 0, 'attachment', 'image/png', 0),
(15, 1, '2018-06-05 09:32:50', '2018-06-05 15:32:50', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px; width:300px; color: blue; margin-right: 20px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-autosave-v1', '', '', '2018-06-05 09:32:50', '2018-06-05 15:32:50', '', 2, 'http://localhost/iscomserca/2017/10/24/2-autosave-v1/', 0, 'revision', '', 0),
(16, 1, '2017-10-24 22:37:03', '2017-10-25 04:37:03', '<div class=\"container\">\r\n<div class=\"row\"></div>\r\n<div class=\"row\"></div>\r\n<div class=\"row\">\r\n<h3 class=\"col-md-4\">FILOSOFIA DE GESTIÃ“N</h3>\r\n<p class=\"col-md-4\"><strong>ISRAEL COMPUTERS SERVICES, C.A</strong> es una compaÃ±Ã­a Venezolana de Suministros de Insumos de Ãºltima tecnologÃ­a dirigida a la prestaciÃ³n de servicios a organizaciones pÃºblicas y privadas.</p>\r\n\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-10-24 22:37:03', '2017-10-25 04:37:03', '', 2, 'http://localhost/iscomserca/2017/10/24/2-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2017-10-24 22:38:21', '2017-10-25 04:38:21', '<div class=\"container\">\r\n<div class=\"row\"></div>\r\n<div class=\"row\"></div>\r\n<div class=\"row\">\r\n<div  class=\"col-md-4\">\r\n<h3>FILOSOFIA DE GESTIÃ“N</h3>\r\n<p><strong>ISRAEL COMPUTERS SERVICES, C.A</strong> es una compaÃ±Ã­a Venezolana de Suministros de Insumos de Ãºltima tecnologÃ­a dirigida a la prestaciÃ³n de servicios a organizaciones pÃºblicas y privadas.</p>\r\n</div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-10-24 22:38:21', '2017-10-25 04:38:21', '', 2, 'http://localhost/iscomserca/2017/10/24/2-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2017-10-24 22:40:31', '2017-10-25 04:40:31', '<div class=\"container\">\r\n<div class=\"row\"></div>\r\n<div class=\"row\"></div>\r\n<div class=\"row\">\r\n<div  class=\"col-md-4\">\r\n<h3>FILOSOFIA DE GESTIÃ“N</h3>\r\n<p><b>ISRAEL COMPUTERS SERVICES, C.A</b> es una compaÃ±Ã­a Venezolana de Suministros de Insumos de Ãºltima tecnologÃ­a dirigida a la prestaciÃ³n de servicios a organizaciones pÃºblicas y privadas.</p>\r\n</div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-10-24 22:40:31', '2017-10-25 04:40:31', '', 2, 'http://localhost/iscomserca/2017/10/24/2-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2017-10-24 23:26:45', '2017-10-25 05:26:45', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div  class=\"col-md-4\">\r\n<h3>Quienes Somos</h3>\r\n<p><b>ISRAEL COMPUTERS SERVICES, C.A</b> es una compaÃ±Ã­a Venezolana de Suministros de Insumos de Ãºltima tecnologÃ­a dirigida a la prestaciÃ³n de servicios a organizaciones pÃºblicas y privadas.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3>MisiÃ³n</h3>\r\n<p>Somos una compaÃ±Ã­a Venezolana operadora y proveedora de soluciones integrales en tecnologÃ­a, que ofrece principalmente el soporte, servicio y mantenimiento de computadoras, impresoras, televisores, y demÃ¡s equipos electrÃ³nicos. Con el fin de brindarles a nuestros clientes la asesorÃ­a, la soluciÃ³n y el servicio necesario para optimizar sus procesos tecnolÃ³gicos, a la vanguardia del mercado contando con personal capacitado, y a su vez cumpliendo con el desarrollo de la sociedad y comprometidos con el medio ambiente.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3>VisiÃ³n</h3>\r\n<p>Ser LÃ­der a nivel nacional en el Ã¡rea de ventas y servicio tÃ©cnico a equipos de ComputaciÃ³n y ElectrÃ³nica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovaciÃ³n tecnolÃ³gica, con el propÃ³sito de posicionarse como lÃ­der en el mercado, siendo referencia de excelencia, eficacia, eficiencia, efectividad y responsabilidad, satisfaciendo asÃ­ las necesidades de nuestros clientes.</p>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3>Objetivo General</h3>\r\n<p>Engranar el desarrollo Comercial, Empresarial y Financiero, para ofrecer un servicio tecnolÃ³gico e innovador que supere las necesidades de los clientes.</p>\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3>Objetivos EspecÃ­ficos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2;\">\r\n<li>Incorporar nuevas gamas de servicios, tales como: La creaciÃ³n de sistemas administrativos, y diseÃ±o de pÃ¡ginas web, para ser una empresa de multiservicios.</li>\r\n<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrÃ³nicos.</li>\r\n<li>Incrementar la capacidad econÃ³mica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-10-24 23:26:45', '2017-10-25 05:26:45', '', 2, 'http://localhost/iscomserca/2017/10/24/2-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2017-10-24 23:29:05', '2017-10-25 05:29:05', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div  class=\"col-md-4\">\r\n<h3>Quienes Somos</h3>\r\n<p><b>ISRAEL COMPUTERS SERVICES, C.A</b> es una compaÃ±Ã­a Venezolana de Suministros de Insumos de Ãºltima tecnologÃ­a dirigida a la prestaciÃ³n de servicios a organizaciones pÃºblicas y privadas.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3>MisiÃ³n</h3>\r\n<p>Somos una compaÃ±Ã­a Venezolana operadora y proveedora de soluciones integrales en tecnologÃ­a, que ofrece principalmente el soporte, servicio y mantenimiento de computadoras, impresoras, televisores, y demÃ¡s equipos electrÃ³nicos. Con el fin de brindarles a nuestros clientes la asesorÃ­a, la soluciÃ³n y el servicio necesario para optimizar sus procesos tecnolÃ³gicos, a la vanguardia del mercado contando con personal capacitado, y a su vez cumpliendo con el desarrollo de la sociedad y comprometidos con el medio ambiente.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3>VisiÃ³n</h3>\r\n<p>Ser LÃ­der a nivel nacional en el Ã¡rea de ventas y servicio tÃ©cnico a equipos de ComputaciÃ³n y ElectrÃ³nica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovaciÃ³n tecnolÃ³gica, con el propÃ³sito de posicionarse como lÃ­der en el mercado, siendo referencia de excelencia, eficacia, eficiencia, efectividad y responsabilidad, satisfaciendo asÃ­ las necesidades de nuestros clientes.</p>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3>Objetivo General</h3>\r\n<p>Engranar el desarrollo Comercial, Empresarial y Financiero, para ofrecer un servicio tecnolÃ³gico e innovador que supere las necesidades de los clientes.</p>\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3>Objetivos EspecÃ­ficos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px;  column-gap: 40px;\">\r\n<li>Incorporar nuevas gamas de servicios, tales como: La creaciÃ³n de sistemas administrativos, y diseÃ±o de pÃ¡ginas web, para ser una empresa de multiservicios.</li>\r\n<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrÃ³nicos.</li>\r\n<li>Incrementar la capacidad econÃ³mica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-10-24 23:29:05', '2017-10-25 05:29:05', '', 2, 'http://localhost/iscomserca/2017/10/24/2-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2017-10-24 23:54:41', '2017-10-25 05:54:41', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div  class=\"col-md-4\">\r\n<h3>Quienes Somos</h3>\r\n<p><b>ISRAEL COMPUTERS SERVICES, C.A</b> es una compaÃ±Ã­a Venezolana de Suministros de Insumos de Ãºltima tecnologÃ­a dirigida a la prestaciÃ³n de servicios a organizaciones pÃºblicas y privadas.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3>MisiÃ³n</h3>\r\n<p>Somos una compaÃ±Ã­a Venezolana operadora y proveedora de soluciones integrales en tecnologÃ­a, que ofrece principalmente el soporte, servicio y mantenimiento de computadoras, impresoras, televisores, y demÃ¡s equipos electrÃ³nicos. Con el fin de brindarles a nuestros clientes la asesorÃ­a, la soluciÃ³n y el servicio necesario para optimizar sus procesos tecnolÃ³gicos, a la vanguardia del mercado contando con personal capacitado, y a su vez cumpliendo con el desarrollo de la sociedad y comprometidos con el medio ambiente.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3>VisiÃ³n</h3>\r\n<p>Ser LÃ­der a nivel nacional en el Ã¡rea de ventas y servicio tÃ©cnico a equipos de ComputaciÃ³n y ElectrÃ³nica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovaciÃ³n tecnolÃ³gica, con el propÃ³sito de posicionarse como lÃ­der en el mercado, siendo referencia de excelencia, eficacia, eficiencia, efectividad y responsabilidad, satisfaciendo asÃ­ las necesidades de nuestros clientes.</p>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3>Objetivo General</h3>\r\n<p>Engranar el desarrollo Comercial, Empresarial y Financiero, para ofrecer un servicio tecnolÃ³gico e innovador que supere las necesidades de los clientes.</p>\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3>Objetivos EspecÃ­ficos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px;  column-gap: 40px;\">\r\n<li>Incorporar nuevas gamas de servicios, tales como: La creaciÃ³n de sistemas administrativos, y diseÃ±o de pÃ¡ginas web, para ser una empresa de multiservicios.</li>\r\n<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrÃ³nicos.</li>\r\n<li>Incrementar la capacidad econÃ³mica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h3>Valores</h3>\r\n<ul>\r\n<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la Ã©tica de los empleados.</li>\r\n<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y pÃºblico en general.</li>\r\n<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-10-24 23:54:41', '2017-10-25 05:54:41', '', 2, 'http://localhost/iscomserca/2017/10/24/2-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2017-10-24 23:56:03', '2017-10-25 05:56:03', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div  class=\"col-md-4\">\r\n<h3 class=\"text-center\">Quienes Somos</h3>\r\n<p><b>ISRAEL COMPUTERS SERVICES, C.A</b> es una compaÃ±Ã­a Venezolana de Suministros de Insumos de Ãºltima tecnologÃ­a dirigida a la prestaciÃ³n de servicios a organizaciones pÃºblicas y privadas.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3 class=\"text-center\">MisiÃ³n</h3>\r\n<p>Somos una compaÃ±Ã­a Venezolana operadora y proveedora de soluciones integrales en tecnologÃ­a, que ofrece principalmente el soporte, servicio y mantenimiento de computadoras, impresoras, televisores, y demÃ¡s equipos electrÃ³nicos. Con el fin de brindarles a nuestros clientes la asesorÃ­a, la soluciÃ³n y el servicio necesario para optimizar sus procesos tecnolÃ³gicos, a la vanguardia del mercado contando con personal capacitado, y a su vez cumpliendo con el desarrollo de la sociedad y comprometidos con el medio ambiente.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3 class=\"text-center\">VisiÃ³n</h3>\r\n<p>Ser LÃ­der a nivel nacional en el Ã¡rea de ventas y servicio tÃ©cnico a equipos de ComputaciÃ³n y ElectrÃ³nica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovaciÃ³n tecnolÃ³gica, con el propÃ³sito de posicionarse como lÃ­der en el mercado, siendo referencia de excelencia, eficacia, eficiencia, efectividad y responsabilidad, satisfaciendo asÃ­ las necesidades de nuestros clientes.</p>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Objetivo General</h3>\r\n<p>Engranar el desarrollo Comercial, Empresarial y Financiero, para ofrecer un servicio tecnolÃ³gico e innovador que supere las necesidades de los clientes.</p>\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos EspecÃ­ficos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px;  column-gap: 40px;\">\r\n<li>Incorporar nuevas gamas de servicios, tales como: La creaciÃ³n de sistemas administrativos, y diseÃ±o de pÃ¡ginas web, para ser una empresa de multiservicios.</li>\r\n<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrÃ³nicos.</li>\r\n<li>Incrementar la capacidad econÃ³mica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la Ã©tica de los empleados.</li>\r\n<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y pÃºblico en general.</li>\r\n<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '0000-00-00 00:00:00', '2017-10-25 05:56:03', '', 2, 'http://localhost/iscomserca/2017/10/24/2-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2017-11-18 10:33:25', '2017-11-18 16:33:25', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div  class=\"col-md-4\">\r\n<h3 class=\"text-center\">Quienes Somos</h3>\r\n<p><b>ISRAEL COMPUTERS SERVICES, C.A</b> es una compaÃ±Ã­a Venezolana de Suministros de Insumos de Ãºltima tecnologÃ­a dirigida a la prestaciÃ³n de servicios a organizaciones pÃºblicas y privadas.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3 class=\"text-center\">MisiÃ³n</h3>\r\n<p>Somos una compaÃ±Ã­a Venezolana operadora y proveedora de soluciones integrales en tecnologÃ­a, que ofrece principalmente el soporte, servicio y mantenimiento de computadoras, impresoras, televisores, y demÃ¡s equipos electrÃ³nicos. Con el fin de brindarles a nuestros clientes la asesorÃ­a, la soluciÃ³n y el servicio necesario para optimizar sus procesos tecnolÃ³gicos, a la vanguardia del mercado contando con personal capacitado, y a su vez cumpliendo con el desarrollo de la sociedad y comprometidos con el medio ambiente.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3 class=\"text-center\">VisiÃ³n</h3>\r\n<p>Ser LÃ­der a nivel nacional en el Ã¡rea de ventas y servicio tÃ©cnico a equipos de ComputaciÃ³n y ElectrÃ³nica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovaciÃ³n tecnolÃ³gica, con el propÃ³sito de posicionarse como lÃ­der en el mercado, siendo referencia de excelencia, eficacia, eficiencia, efectividad y responsabilidad, satisfaciendo asÃ­ las necesidades de nuestros clientes.</p>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Objetivo General</h3>\r\n<p>Engranar el desarrollo Comercial, Empresarial y Financiero, para ofrecer un servicio tecnolÃ³gico e innovador que supere las necesidades de los clientes.</p>\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos EspecÃ­ficos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px;  column-gap: 40px;\">\r\n<li>Incorporar nuevas gamas de servicios, tales como: La creaciÃ³n de sistemas administrativos, y diseÃ±o de pÃ¡ginas web, para ser una empresa de multiservicios.</li>\r\n<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrÃ³nicos.</li>\r\n<li>Incrementar la capacidad econÃ³mica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la Ã©tica de los empleados.</li>\r\n<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y pÃºblico en general.</li>\r\n<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-9\">\r\n[iscomserca-login]\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-11-18 10:33:25', '2017-11-18 16:33:25', '', 2, 'http://localhost/iscomserca/2017/11/18/2-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2017-11-18 10:33:46', '2017-11-18 16:33:46', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div  class=\"col-md-4\">\r\n<h3 class=\"text-center\">Quienes Somos</h3>\r\n<p><b>ISRAEL COMPUTERS SERVICES, C.A</b> es una compaÃ±Ã­a Venezolana de Suministros de Insumos de Ãºltima tecnologÃ­a dirigida a la prestaciÃ³n de servicios a organizaciones pÃºblicas y privadas.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3 class=\"text-center\">MisiÃ³n</h3>\r\n<p>Somos una compaÃ±Ã­a Venezolana operadora y proveedora de soluciones integrales en tecnologÃ­a, que ofrece principalmente el soporte, servicio y mantenimiento de computadoras, impresoras, televisores, y demÃ¡s equipos electrÃ³nicos. Con el fin de brindarles a nuestros clientes la asesorÃ­a, la soluciÃ³n y el servicio necesario para optimizar sus procesos tecnolÃ³gicos, a la vanguardia del mercado contando con personal capacitado, y a su vez cumpliendo con el desarrollo de la sociedad y comprometidos con el medio ambiente.</p>\r\n</div>\r\n<div  class=\"col-md-4\">\r\n<h3 class=\"text-center\">VisiÃ³n</h3>\r\n<p>Ser LÃ­der a nivel nacional en el Ã¡rea de ventas y servicio tÃ©cnico a equipos de ComputaciÃ³n y ElectrÃ³nica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovaciÃ³n tecnolÃ³gica, con el propÃ³sito de posicionarse como lÃ­der en el mercado, siendo referencia de excelencia, eficacia, eficiencia, efectividad y responsabilidad, satisfaciendo asÃ­ las necesidades de nuestros clientes.</p>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Objetivo General</h3>\r\n<p>Engranar el desarrollo Comercial, Empresarial y Financiero, para ofrecer un servicio tecnolÃ³gico e innovador que supere las necesidades de los clientes.</p>\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos EspecÃ­ficos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px;  column-gap: 40px;\">\r\n<li>Incorporar nuevas gamas de servicios, tales como: La creaciÃ³n de sistemas administrativos, y diseÃ±o de pÃ¡ginas web, para ser una empresa de multiservicios.</li>\r\n<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrÃ³nicos.</li>\r\n<li>Incrementar la capacidad econÃ³mica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la Ã©tica de los empleados.</li>\r\n<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y pÃºblico en general.</li>\r\n<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-3\">\r\n[iscomserca-login]\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-11-18 10:33:46', '2017-11-18 16:33:46', '', 2, 'http://localhost/iscomserca/2017/11/18/2-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2017-12-20 05:35:04', '2017-12-20 11:35:04', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"col-md-4\">Aquí irá la galería</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n<div class=\"col-md-4\">Imágenes aquí</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-3\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-md-3\">[iscomserca-login]</div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-12-20 05:35:04', '2017-12-20 11:35:04', '', 2, 'http://localhost/iscomserca/2017/12/20/2-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2017-12-22 15:59:34', '2017-12-22 21:59:34', '<div class=\"container\">\r\n<div class=\"row fondo-texto-nosotros\">\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 fondo-texto\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'publish', 'closed', 'closed', '', 'nosotros', '', '', '2019-07-03 03:28:48', '2019-07-03 09:28:48', '', 0, 'http://localhost/iscomserca/?page_id=29', 0, 'page', '', 0),
(30, 1, '2017-12-22 15:59:34', '2017-12-22 21:59:34', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2017-12-22 15:59:34', '2017-12-22 21:59:34', '', 29, 'http://localhost/iscomserca/2017/12/22/29-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2017-12-22 15:59:57', '2017-12-22 21:59:57', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"></div>\r\n<div class=\"col-md-6\">[iscomserca-login]</div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-12-22 15:59:57', '2017-12-22 21:59:57', '', 2, 'http://localhost/iscomserca/2017/12/22/2-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2017-12-22 16:00:22', '2017-12-22 22:00:22', ' ', '', '', 'publish', 'closed', 'closed', '', '32', '', '', '2017-12-22 16:01:09', '2017-12-22 22:01:09', '', 0, 'http://localhost/iscomserca/?p=32', 1, 'nav_menu_item', '', 0),
(34, 1, '2017-12-22 16:00:23', '2017-12-22 22:00:23', ' ', '', '', 'publish', 'closed', 'closed', '', '34', '', '', '2017-12-22 16:01:09', '2017-12-22 22:01:09', '', 0, 'http://localhost/iscomserca/?p=34', 2, 'nav_menu_item', '', 0),
(38, 1, '2018-01-07 02:43:09', '2018-01-07 08:43:09', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\"><h2 class=\"text-left\">Lo que te Ofrecemos</h2></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3>VENTA, MANTENIMIENTO, Y REPARACI&Oacute;N DE EQUIPOS DE OFICINA Y ELECTRODOM&Eacute;STICOS</h3>\r\n<a href=\"#\" class=\"btn btn-primary\">Haz Click</a></div>\r\n<div class=\"col-md-6\"><div class=\"row\"><div class=\"col-md-6\"><h3>INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3></div><div class=\"col-md-6\">[iscomserca-login]</div></div></div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"><h3 class=\"text-center\">S&Iacute;GUENOS A TRAV&Eacute;S DE</h3>\r\n<h2 class=\"text-center\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" class=\"fa fa-facebook></a> <a href=\"https://twitter.com/ISCOMSERCA\" class=\"fa fa-twitter></a> <a href=\"https://www.instagram.com/iscomserca/\" class=\"fa fa-instagram></a></h2></div>\r\n<div class=\"col-md-3 embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-01-07 02:43:09', '2018-01-07 08:43:09', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2018-01-07 02:45:37', '2018-01-07 08:45:37', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\"><h2 class=\"text-left\">Lo que te Ofrecemos</h2></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">VENTA, MANTENIMIENTO, Y REPARACI&Oacute;N DE EQUIPOS DE OFICINA Y ELECTRODOM&Eacute;STICOS</h3>\r\n<a href=\"#\" class=\"btn btn-primary\">Haz Click</a></div>\r\n<div class=\"col-md-6\"><div class=\"row\"><div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3></div><div class=\"col-md-6\">[iscomserca-login]</div></div></div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"><h3 class=\"text-center\">S&Iacute;GUENOS A TRAV&Eacute;S DE</h3>\r\n<h2 class=\"text-center\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" class=\"fa fa-facebook\"></a> <a href=\"https://twitter.com/ISCOMSERCA\" class=\"fa fa-twitter\"></a> <a href=\"https://www.instagram.com/iscomserca/\" class=\"fa fa-instagram\"></a></h2></div>\r\n<div class=\"col-md-3 embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-01-07 02:45:37', '2018-01-07 08:45:37', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2018-01-07 02:50:36', '2018-01-07 08:50:36', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\"><h2 class=\"text-left\">Lo que te Ofrecemos</h2></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">VENTA, MANTENIMIENTO, Y REPARACI&Oacute;N DE EQUIPOS DE OFICINA Y ELECTRODOM&Eacute;STICOS</h3>\r\n<a href=\"#\" class=\"btn btn-primary\">Haz Click</a></div>\r\n<div class=\"col-md-6\"><div class=\"row\"><div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3></div><div class=\"col-md-6\">[iscomserca-login]</div></div></div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"><h3 class=\"text-center\">S&Iacute;GUENOS A TRAV&Eacute;S DE</h3>\r\n<h2 class=\"text-center\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" class=\"fa fa-facebook-square fa-lg\"></a> <a href=\"https://twitter.com/ISCOMSERCA\" class=\"fa fa-twitter-square fa-lg\"></a> <a href=\"https://www.instagram.com/iscomserca/\" class=\"fa fa-instagram fa-lg\"></a></h2></div>\r\n<div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-01-07 02:50:36', '2018-01-07 08:50:36', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2018-01-07 02:56:18', '2018-01-07 08:56:18', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\"><h2 class=\"text-left\">Lo que te Ofrecemos</h2></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">VENTA, MANTENIMIENTO, Y REPARACI&Oacute;N DE EQUIPOS DE OFICINA Y ELECTRODOM&Eacute;STICOS</h3>\r\n<a href=\"#\" class=\"btn btn-primary\">Haz Click</a></div>\r\n<div class=\"col-md-6\"><div class=\"row\"><div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3></div><div class=\"col-md-6\">[iscomserca-login]</div></div></div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"><h3 class=\"text-center\">S&Iacute;GUENOS A TRAV&Eacute;S DE</h3>\r\n<h2 class=\"text-center\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" class=\"fa fa-facebook-square fa-4x\"></a> <a href=\"https://twitter.com/ISCOMSERCA\" class=\"fa fa-twitter-square fa-4x\"></a> <a href=\"https://www.instagram.com/iscomserca/\" class=\"fa fa-instagram fa-4x\"></a></h2></div>\r\n<div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-01-07 02:56:18', '2018-01-07 08:56:18', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2018-02-04 23:36:04', '2018-02-05 05:36:04', '', 'atencion-cliente', '', 'inherit', 'open', 'closed', '', 'atencion-cliente', '', '', '2018-02-04 23:36:04', '2018-02-05 05:36:04', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/02/atencion-cliente.png', 0, 'attachment', 'image/png', 0),
(44, 1, '2018-02-04 23:36:18', '2018-02-05 05:36:18', '', 'reparamos', '', 'inherit', 'open', 'closed', '', 'reparamos', '', '', '2018-02-04 23:36:18', '2018-02-05 05:36:18', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/02/reparamos.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(45, 1, '2018-02-05 04:49:33', '2018-02-05 10:49:33', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\"><h2 class=\"text-left\">Lo que te Ofrecemos</h2></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\" id=\"reparamos\">\r\n<a href=\"#\" class=\"btn btn-primary\">Haz Click</a></div>\r\n<div class=\"col-md-6\" id=\"atencion-cliente\"></div>\r\n</div>\r\n<div class=\"row\"><div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3></div><div class=\"col-md-6\">[iscomserca-login]</div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"><h3 class=\"text-center\">S&Iacute;GUENOS A TRAV&Eacute;S DE</h3>\r\n<h2 class=\"text-center\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" class=\"fa fa-facebook-square fa-4x\"></a> <a href=\"https://twitter.com/ISCOMSERCA\" class=\"fa fa-twitter-square fa-4x\"></a> <a href=\"https://www.instagram.com/iscomserca/\" class=\"fa fa-instagram fa-4x\"></a></h2></div>\r\n<div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-02-05 04:49:33', '2018-02-05 10:49:33', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2018-02-22 13:17:08', '2018-02-22 19:17:08', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\"><h2 class=\"text-left\">Lo que te Ofrecemos</h2></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\" id=\"reparamos\">\r\n<a href=\"#\" class=\"btn btn-primary\">Haz Click</a></div>\r\n<!--<div class=\"col-md-6\" id=\"atencion-cliente\"></div>\r\n</div>-->\r\n<div class=\"row\"><div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3></div><div class=\"col-md-6\">[iscomserca-login]</div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"><h3 class=\"text-center\">S&Iacute;GUENOS A TRAV&Eacute;S DE</h3>\r\n<h2 class=\"text-center\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" class=\"fa fa-facebook-square fa-4x\"></a> <a href=\"https://twitter.com/ISCOMSERCA\" class=\"fa fa-twitter-square fa-4x\"></a> <a href=\"https://www.instagram.com/iscomserca/\" class=\"fa fa-instagram fa-4x\"></a></h2></div>\r\n<div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-02-22 13:17:08', '2018-02-22 19:17:08', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-02-22 13:18:55', '2018-02-22 19:18:55', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\"><h2 class=\"text-left\">Lo que te Ofrecemos</h2></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\" id=\"reparamos\">\r\n<a href=\"#\" class=\"btn btn-primary\">Haz Click</a></div>\r\n<div class=\"col-md-6\" id=\"atencion-cliente\" style=\"border-radius: 5px;\"></div>\r\n</div>\r\n<div class=\"row\"><div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3></div><div class=\"col-md-6\">[iscomserca-login]</div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9\"><h3 class=\"text-center\">S&Iacute;GUENOS A TRAV&Eacute;S DE</h3>\r\n<h2 class=\"text-center\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" class=\"fa fa-facebook-square fa-4x\"></a> <a href=\"https://twitter.com/ISCOMSERCA\" class=\"fa fa-twitter-square fa-4x\"></a> <a href=\"https://www.instagram.com/iscomserca/\" class=\"fa fa-instagram fa-4x\"></a></h2></div>\r\n<div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-02-22 13:18:55', '2018-02-22 19:18:55', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2018-05-10 08:35:23', '2018-05-10 14:35:23', '', 'imagen 3 copia', '', 'inherit', 'open', 'closed', '', 'imagen-3-copia', '', '', '2018-05-10 08:35:23', '2018-05-10 14:35:23', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png', 0, 'attachment', 'image/png', 0),
(52, 1, '2018-05-10 08:35:26', '2018-05-10 14:35:26', '', 'imagen 4 copia', '', 'inherit', 'open', 'closed', '', 'imagen-4-copia', '', '', '2018-05-10 08:35:26', '2018-05-10 14:35:26', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-4-copia.png', 0, 'attachment', 'image/png', 0),
(53, 1, '2018-05-10 08:35:28', '2018-05-10 14:35:28', '', 'logos redes copia facebook', '', 'inherit', 'open', 'closed', '', 'logos-redes-copia-facebook', '', '', '2018-05-10 08:35:28', '2018-05-10 14:35:28', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/logos-redes-copia-facebook.png', 0, 'attachment', 'image/png', 0),
(54, 1, '2018-05-10 08:35:30', '2018-05-10 14:35:30', '', 'logos redes copia instagram', '', 'inherit', 'open', 'closed', '', 'logos-redes-copia-instagram', '', '', '2018-05-10 08:35:30', '2018-05-10 14:35:30', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/logos-redes-copia-instagram.png', 0, 'attachment', 'image/png', 0),
(55, 1, '2018-05-10 08:35:31', '2018-05-10 14:35:31', '', 'logos redes copia twitter', '', 'inherit', 'open', 'closed', '', 'logos-redes-copia-twitter', '', '', '2018-05-10 08:35:31', '2018-05-10 14:35:31', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/logos-redes-copia-twitter.png', 0, 'attachment', 'image/png', 0),
(56, 1, '2018-05-10 08:35:33', '2018-05-10 14:35:33', '', 'slider copia', '', 'inherit', 'open', 'closed', '', 'slider-copia', '', '', '2018-05-10 08:35:33', '2018-05-10 14:35:33', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/slider-copia.png', 0, 'attachment', 'image/png', 0),
(57, 1, '2018-05-10 08:35:35', '2018-05-10 14:35:35', '', 'fondo copia', '', 'inherit', 'open', 'closed', '', 'fondo-copia', '', '', '2018-05-10 08:35:35', '2018-05-10 14:35:35', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/fondo-copia.png', 0, 'attachment', 'image/png', 0),
(58, 1, '2018-05-10 08:35:37', '2018-05-10 14:35:37', '', 'imagen 1 copia', '', 'inherit', 'open', 'closed', '', 'imagen-1-copia', '', '', '2018-05-10 08:35:37', '2018-05-10 14:35:37', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png', 0, 'attachment', 'image/png', 0),
(59, 1, '2018-05-10 08:35:39', '2018-05-10 14:35:39', '', 'imagen 2 copia', '', 'inherit', 'open', 'closed', '', 'imagen-2-copia', '', '', '2018-05-10 08:35:39', '2018-05-10 14:35:39', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png', 0, 'attachment', 'image/png', 0),
(60, 1, '2018-05-10 09:26:42', '2018-05-10 15:26:42', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\"><h2 class=\"text-left\">Lo que te Ofrecemos</h2></div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\" id=\"reparamos\">\r\n<a href=\"#\" class=\"btn btn-primary\">Haz Click</a></div>\r\n<div class=\"col-md-6\" id=\"atencion-cliente\" style=\"border-radius: 5px;\"></div>\r\n</div>\r\n<div class=\"row\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div>[iscomserca-login]</div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-10 09:26:42', '2018-05-10 15:26:42', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2018-05-10 10:16:29', '2018-05-10 16:16:29', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div>[iscomserca-login]</div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-10 10:16:29', '2018-05-10 16:16:29', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2018-05-10 10:19:35', '2018-05-10 16:19:35', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-6 front-login\">[iscomserca-login]</div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-10 10:19:35', '2018-05-10 16:19:35', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2018-05-10 10:47:01', '2018-05-10 16:47:01', '', 'pieza-fondo', '', 'inherit', 'open', 'closed', '', 'pieza-fondo', '', '', '2018-05-10 10:47:01', '2018-05-10 16:47:01', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/pieza-fondo.png', 0, 'attachment', 'image/png', 0),
(64, 1, '2018-05-10 11:14:58', '2018-05-10 17:14:58', '', 'facebook', '', 'inherit', 'open', 'closed', '', 'facebook', '', '', '2018-05-10 11:14:58', '2018-05-10 17:14:58', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png', 0, 'attachment', 'image/png', 0),
(65, 1, '2018-05-10 11:15:00', '2018-05-10 17:15:00', '', 'instagram', '', 'inherit', 'open', 'closed', '', 'instagram', '', '', '2018-05-10 11:15:00', '2018-05-10 17:15:00', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2018-05-10 11:15:01', '2018-05-10 17:15:01', '', 'twitter', '', 'inherit', 'open', 'closed', '', 'twitter', '', '', '2018-05-10 11:15:01', '2018-05-10 17:15:01', '', 0, 'http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png', 0, 'attachment', 'image/png', 0),
(68, 1, '2018-05-10 11:26:50', '2018-05-10 17:26:50', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-6 front-login\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-10 11:26:50', '2018-05-10 17:26:50', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2018-05-28 14:39:02', '2018-05-28 20:39:02', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-6 front-login\" style=\"text-align:center;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:39:02', '2018-05-28 20:39:02', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2018-05-28 14:39:53', '2018-05-28 20:39:53', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-6 front-login\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:39:53', '2018-05-28 20:39:53', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2018-05-28 14:47:13', '2018-05-28 20:47:13', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-6 front-login\" style=\"background-color: red;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:47:13', '2018-05-28 20:47:13', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2018-05-28 14:48:15', '2018-05-28 20:48:15', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: red;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:48:15', '2018-05-28 20:48:15', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2018-05-28 14:49:00', '2018-05-28 20:49:00', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #ff0;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:49:00', '2018-05-28 20:49:00', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2018-05-28 14:50:09', '2018-05-28 20:50:09', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:50:09', '2018-05-28 20:50:09', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(76, 1, '2018-05-28 14:53:19', '2018-05-28 20:53:19', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:53:19', '2018-05-28 20:53:19', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2018-05-28 14:54:34', '2018-05-28 20:54:34', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:54:34', '2018-05-28 20:54:34', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(78, 1, '2018-05-28 14:57:37', '2018-05-28 20:57:37', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 10px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:57:37', '2018-05-28 20:57:37', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(79, 1, '2018-05-28 14:58:27', '2018-05-28 20:58:27', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 12px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:58:27', '2018-05-28 20:58:27', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(80, 1, '2018-05-28 14:59:13', '2018-05-28 20:59:13', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 14:59:13', '2018-05-28 20:59:13', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(81, 1, '2018-05-28 15:00:47', '2018-05-28 21:00:47', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\">INICIA SESI&Oacute;N Y CONSULTA TUS EQUIPOS AQU&Iacute;</h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-28 15:00:47', '2018-05-28 21:00:47', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2018-05-29 10:25:08', '2018-05-29 16:25:08', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><h3 class=\"text-center\"></h3><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-29 10:25:08', '2018-05-29 16:25:08', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(83, 1, '2018-05-29 10:41:32', '2018-05-29 16:41:32', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6 fondo-texto\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 fondo-texto\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-05-29 10:41:32', '2018-05-29 16:41:32', '', 29, 'http://localhost/iscomserca/29-revision-v1/', 0, 'revision', '', 0),
(84, 1, '2018-05-29 10:46:34', '2018-05-29 16:46:34', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 fondo-texto\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6 fondo-texto\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 fondo-texto\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-05-29 10:46:34', '2018-05-29 16:46:34', '', 29, 'http://localhost/iscomserca/29-revision-v1/', 0, 'revision', '', 0),
(85, 1, '2018-05-29 10:49:23', '2018-05-29 16:49:23', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 fondo-texto-izq\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6 fondo-texto\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 fondo-texto\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-05-29 10:49:23', '2018-05-29 16:49:23', '', 29, 'http://localhost/iscomserca/29-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(86, 1, '2018-05-29 10:57:54', '2018-05-29 16:57:54', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 fondo-texto-izq\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6 fondo-texto-der\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 fondo-texto\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-05-29 10:57:54', '2018-05-29 16:57:54', '', 29, 'http://localhost/iscomserca/29-revision-v1/', 0, 'revision', '', 0),
(87, 1, '2018-05-29 11:27:52', '2018-05-29 17:27:52', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 fondo-texto\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-05-29 11:27:52', '2018-05-29 17:27:52', '', 29, 'http://localhost/iscomserca/29-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2018-05-29 11:31:23', '2018-05-29 17:31:23', '<div class=\"container\">\r\n<div class=\"row fondo-texto-izq\">\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 fondo-texto\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-05-29 11:31:23', '2018-05-29 17:31:23', '', 29, 'http://localhost/iscomserca/29-revision-v1/', 0, 'revision', '', 0),
(89, 1, '2018-05-29 11:51:17', '2018-05-29 17:51:17', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-29 11:51:17', '2018-05-29 17:51:17', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2018-05-29 12:03:12', '2018-05-29 18:03:12', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px; opacity: 0.9;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-05-29 12:03:12', '2018-05-29 18:03:12', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2018-06-05 09:27:39', '2018-06-05 15:27:39', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-06-05 09:27:39', '2018-06-05 15:27:39', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2018-06-05 09:28:45', '2018-06-05 15:28:45', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px; width:300px\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-06-05 09:28:45', '2018-06-05 15:28:45', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2018-06-05 09:31:02', '2018-06-05 15:31:02', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px; width:300px; color: blue;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-06-05 09:31:02', '2018-06-05 15:31:02', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(95, 1, '2018-06-05 09:32:12', '2018-06-05 15:32:12', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px; width:300px; color: blue; margin-right: 20px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-06-05 09:32:12', '2018-06-05 15:32:12', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2018-06-05 09:33:16', '2018-06-05 15:33:16', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\" style=\"background-color: #f5f5f5; float:right; text-align: center !important; padding: 15px 0px 0px 15px; width:300px; color: blue; margin: 10px;\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-06-05 09:33:16', '2018-06-05 15:33:16', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2018-06-05 09:40:38', '2018-06-05 15:40:38', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-1-copia.png\" alt=\"Reparamos y Vendemos\" title=\"Reparamos y Vendemos\" /></div><div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-2-copia.png\" alt=\"Ubicaci&oacute;n\" title=\"Ubicaci&oacute;n\" /></div></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-6\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/imagen-3-copia.png\" alt=\"Inicia Sesi&oacute;n\" title=\"Inicia Sesi&oacute;n\" /></div><div class=\"col-md-5 front-login\">[iscomserca-login]</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-9 redes\"><a href=\"https://www.facebook.com/ISCOMSERCA/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/facebook.png\" alt=\"Facebook\" title=\"Facebook\" /></a><a href=\"https://twitter.com/ISCOMSERCA\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/twitter.png\" alt=\"Twitter\" title=\"Twitter\" /></a><a href=\"https://www.instagram.com/iscomserca/\" target=\"_blank\"><img src=\"http://localhost/iscomserca/wp-content/uploads/2018/05/instagram.png\" alt=\"Instagram\" title=\"Instagram\" /></a></div><div class=\"col-md-3\"><iframe src=\"//www.youtube.com/embed/or6fjP5c9Bg?rel=0\"></iframe></div>\r\n</div>\r\n</div>', 'Inicio', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-06-05 09:40:38', '2018-06-05 15:40:38', '', 2, 'http://localhost/iscomserca/2-revision-v1/', 0, 'revision', '', 0),
(98, 1, '2018-06-05 09:47:23', '2018-06-05 15:47:23', '<div class=\"container\">\r\n<div class=\"row fondo-texto-nosotros\">\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 fondo-texto\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-06-05 09:47:23', '2018-06-05 15:47:23', '', 29, 'http://localhost/iscomserca/29-revision-v1/', 0, 'revision', '', 0),
(103, 1, '2019-07-03 03:28:58', '2019-07-03 09:28:58', '<div class=\"container\">\r\n<div class=\"row fondo-texto-nosotros\">\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Somos</h3>\r\n<b>ISRAEL COMPUTERS SERVICES, C.A</b> compañía Venezolana especializada en la prestación de Servicios y Suministros de Insumos de última tecnología a organizaciones públicas y privadas.\r\n\r\n</div>\r\n<div class=\"col-md-6\">\r\n<h3 class=\"text-center\">Ofrecemos</h3>\r\nSoluciones integrales en tecnología, principalmente el soporte, servicio y mantenimiento de los equipos electronicos de tu oficina u hogar, entre ellos; computadoras, impresoras, televisores, y demás. Tambien brindamos a nuestros clientes la asesoría, la solución y los servicios necesarios para optimizar sus procesos tecnológicos a la vanguardia del mercado.\r\n\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-12 fondo-texto\">\r\n<h3 class=\"text-center\">Propósito</h3>\r\nSer Líder a nivel nacional en el área de ventas y servicio técnico a equipos de Computación y Electrónica. Teniendo como lineamientos principales la calidad, precio, tiempo e innovación tecnológica. Convirtiendonos en referencia de excelencia, eficacia, efectividad y responsabilidad, satisfaciendo así las necesidades de nuestros clientes.\r\n\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Buscamos</h3>\r\nOfrecer servicios tecnológicos e innovadores que superen las necesidades de nuestros clientes engranando el desarrollo Comercial, Empresarial y Financiero.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Objetivos Específicos</h3>\r\n<ol style=\"webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-gap: 40px; -moz-column-gap: 40px; column-gap: 40px;\">\r\n 	<li>Incorporar nuevas gamas de servicios, tales como: La creación de sistemas administrativos, y diseño de páginas web, para ser una empresa de multiservicios.</li>\r\n 	<li>Aperturar a mediano plazo sucursales en distintas localidades y estados, con la finalidad de abarcar ampliamente el mercado.</li>\r\n 	<li>Establecer alianzas comerciales con marcas reconocidas de equipos electrónicos.</li>\r\n 	<li>Incrementar la capacidad económica y contar con suficiente liquidez para prevalecer en el mercado.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"col-md-4\">\r\n<h3 class=\"text-center\">Nosotros</h3>\r\nSiempre estamos en constante crecimiento personal y profesional, ejecutamos nuestras labores de manera comprometida, transparente, respetuosa y responsable. Estamos altamente capacitados para brindarte la mejor atencion y un excelente servicio, actuando siempre en beneficio del desarrollo de la sociedad y el mejoramiento del medio ambiente.\r\n\r\n</div>\r\n<div class=\"col-md-8\">\r\n<h3 class=\"text-center\">Valores</h3>\r\n<ul>\r\n 	<li><b>Compromiso:</b> Principalmente con los apreciados clientes, al brindarles un servicio de calidad. De igual manera ofreciendo estabilidad a las familias del personal, y con el medio ambiente al respetar y cumplir todas las normas establecidas para el cuidado de este.</li>\r\n 	<li><b>Confianza:</b> Brindar transparencia y calidad en el servicio ofrecido, contando con la ética de los empleados.</li>\r\n 	<li><b>Respeto:</b> Establecer relaciones interpersonales equilibradamente dentro del equipo de trabajo, con los clientes y público en general.</li>\r\n 	<li><b>Responsabilidad:</b> Realizar con calidad y esmero cada requerimiento de nuestros clientes, cumpliendo los tiempos establecidos.</li>\r\n 	<li><b>Excelencia:</b> Ejecutar nuestro servicio de forma eficiente, eficaz y efectivamente superando las expectativas de los clientes marcando la diferencia.</li>\r\n</ul>\r\n</div>', 'Nosotros', '', 'inherit', 'closed', 'closed', '', '29-autosave-v1', '', '', '2019-07-03 03:28:58', '2019-07-03 09:28:58', '', 29, 'http://localhost/iscomserca/29-autosave-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sin categorÃ­a', 'sin-categoria', 0),
(2, 'Menu 1', 'menu-1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(32, 2, 0),
(34, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', 'theme_editor_notice,wp496_privacy'),
(14, 1, 'show_welcome_panel', '1'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '121'),
(17, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"190.6.7.0\";}'),
(18, 1, 'cedula', '16781338'),
(19, 1, 'celular', '04127938666'),
(20, 2, 'first_name', 'Pedro'),
(21, 2, 'last_name', 'Perez'),
(22, 2, 'wp_capabilities', 'a:1:{s:6:\"author\";b:1;}'),
(23, 2, 'wp_user_level', '2'),
(24, 2, 'cedula', '12345678'),
(25, 2, 'celular', '04147777777'),
(26, 1, 'wp_user-settings', 'editor=tinymce&libraryContent=upload&mfold=o'),
(27, 1, 'wp_user-settings-time', '1566835181'),
(28, 1, 'cedula_prefijo', 'V'),
(29, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(30, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(32, 1, 'nav_menu_recently_edited', '2'),
(55, 1, 'default_password_nag', ''),
(58, 2, 'cedula_prefijo', 'V'),
(59, 2, 'nickname', 'pperez'),
(60, 2, 'description', ''),
(61, 2, 'rich_editing', 'true'),
(62, 2, 'syntax_highlighting', 'true'),
(63, 2, 'comment_shortcuts', 'false'),
(64, 2, 'admin_color', 'fresh'),
(65, 2, 'use_ssl', '0'),
(66, 2, 'show_admin_bar_front', 'true'),
(67, 2, 'locale', ''),
(71, 2, 'wp_dashboard_quick_press_last_post_id', '115'),
(72, 2, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"190.6.1.0\";}'),
(74, 3, 'nickname', 'mgonzalez'),
(75, 3, 'first_name', 'Maria'),
(76, 3, 'last_name', 'Gonzalez'),
(77, 3, 'description', ''),
(78, 3, 'rich_editing', 'true'),
(79, 3, 'syntax_highlighting', 'true'),
(80, 3, 'comment_shortcuts', 'false'),
(81, 3, 'admin_color', 'fresh'),
(82, 3, 'use_ssl', '0'),
(83, 3, 'show_admin_bar_front', 'true'),
(84, 3, 'locale', ''),
(85, 3, 'wp_capabilities', 'a:1:{s:11:\"contributor\";b:1;}'),
(86, 3, 'wp_user_level', '1'),
(87, 3, 'dismissed_wp_pointers', 'wp496_privacy'),
(89, 3, 'wp_dashboard_quick_press_last_post_id', '106'),
(90, 3, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"190.6.1.0\";}'),
(92, 4, 'nickname', 'ajimenez'),
(93, 4, 'first_name', 'Alfredo'),
(94, 4, 'last_name', 'Jimenez'),
(95, 4, 'description', ''),
(96, 4, 'rich_editing', 'true'),
(97, 4, 'syntax_highlighting', 'true'),
(98, 4, 'comment_shortcuts', 'false'),
(99, 4, 'admin_color', 'fresh'),
(100, 4, 'use_ssl', '0'),
(101, 4, 'show_admin_bar_front', 'true'),
(102, 4, 'locale', ''),
(103, 4, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(104, 4, 'wp_user_level', '0'),
(105, 4, 'dismissed_wp_pointers', 'wp496_privacy'),
(107, 4, 'celular', ''),
(108, 4, 'session_tokens', 'a:1:{s:64:\"7943a042b5fac97cb2d8dbc80642e47c3c8620e15232f463019fdcc192b5ad95\";a:4:{s:10:\"expiration\";i:1562189068;s:2:\"ip\";s:10:\"190.6.1.75\";s:2:\"ua\";s:102:\"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1562016268;}}'),
(109, 4, 'wp_dashboard_quick_press_last_post_id', '102'),
(110, 4, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"190.6.1.0\";}'),
(115, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(116, 1, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(117, 2, 'wp_user-settings', 'mfold=f'),
(118, 2, 'wp_user-settings-time', '1565811740'),
(119, 2, 'closedpostboxes_dashboard', 'a:1:{i:0;s:19:\"dashboard_right_now\";}'),
(120, 2, 'metaboxhidden_dashboard', 'a:3:{i:0;s:18:\"dashboard_activity\";i:1;s:21:\"dashboard_quick_press\";i:2;s:17:\"dashboard_primary\";}'),
(124, 5, 'first_name', 'Dixon'),
(125, 5, 'last_name', 'Acosta'),
(126, 5, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(127, 5, 'wp_user_level', '0'),
(128, 5, 'prefijo_cedula', 'V'),
(129, 5, 'cedula', '15287442'),
(130, 5, 'celular', '04246870849'),
(131, 1, 'syntax_highlighting', 'true'),
(139, 4, 'cedula_prefijo', 'V'),
(140, 4, 'cedula', '12345633'),
(141, 5, 'cedula_prefijo', 'V'),
(142, 5, 'nickname', 'dacosta'),
(143, 5, 'description', ''),
(144, 5, 'rich_editing', 'true'),
(145, 5, 'syntax_highlighting', 'true'),
(146, 5, 'comment_shortcuts', 'false'),
(147, 5, 'admin_color', 'fresh'),
(148, 5, 'use_ssl', '0'),
(149, 5, 'show_admin_bar_front', 'true'),
(150, 5, 'locale', ''),
(154, 6, 'nickname', 'coordinador'),
(155, 6, 'first_name', 'Coordinador'),
(156, 6, 'last_name', ''),
(157, 6, 'description', ''),
(158, 6, 'rich_editing', 'true'),
(159, 6, 'syntax_highlighting', 'true'),
(160, 6, 'comment_shortcuts', 'false'),
(161, 6, 'admin_color', 'fresh'),
(162, 6, 'use_ssl', '0'),
(163, 6, 'show_admin_bar_front', 'true'),
(164, 6, 'locale', ''),
(165, 6, 'wp_capabilities', 'a:1:{s:6:\"editor\";b:1;}'),
(166, 6, 'wp_user_level', '7'),
(167, 6, 'dismissed_wp_pointers', 'wp496_privacy'),
(168, 6, 'celular', ''),
(170, 6, 'wp_dashboard_quick_press_last_post_id', '117'),
(173, 6, 'wp_user-settings', 'mfold=o'),
(174, 6, 'wp_user-settings-time', '1565099476'),
(175, 7, 'nickname', 'tecnico'),
(176, 7, 'first_name', 'Técnico'),
(177, 7, 'last_name', ''),
(178, 7, 'description', ''),
(179, 7, 'rich_editing', 'true'),
(180, 7, 'syntax_highlighting', 'true'),
(181, 7, 'comment_shortcuts', 'false'),
(182, 7, 'admin_color', 'fresh'),
(183, 7, 'use_ssl', '0'),
(184, 7, 'show_admin_bar_front', 'true'),
(185, 7, 'locale', ''),
(186, 7, 'wp_capabilities', 'a:1:{s:6:\"author\";b:1;}'),
(187, 7, 'wp_user_level', '2'),
(188, 7, 'dismissed_wp_pointers', 'wp496_privacy'),
(189, 7, 'celular', ''),
(191, 7, 'wp_dashboard_quick_press_last_post_id', '118'),
(192, 7, 'closedpostboxes_dashboard', 'a:0:{}'),
(193, 7, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(194, 8, 'nickname', 'receptor'),
(195, 8, 'first_name', 'Receptor'),
(196, 8, 'last_name', ''),
(197, 8, 'description', ''),
(198, 8, 'rich_editing', 'true'),
(199, 8, 'syntax_highlighting', 'true'),
(200, 8, 'comment_shortcuts', 'false'),
(201, 8, 'admin_color', 'fresh'),
(202, 8, 'use_ssl', '0'),
(203, 8, 'show_admin_bar_front', 'true'),
(204, 8, 'locale', ''),
(205, 8, 'wp_capabilities', 'a:1:{s:11:\"contributor\";b:1;}'),
(206, 8, 'wp_user_level', '1'),
(207, 8, 'dismissed_wp_pointers', 'wp496_privacy'),
(209, 8, 'wp_dashboard_quick_press_last_post_id', '120'),
(211, 8, 'celular', ''),
(217, 8, 'closedpostboxes_dashboard', 'a:0:{}'),
(218, 8, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(219, 8, 'wp_user-settings', 'mfold=o'),
(220, 8, 'wp_user-settings-time', '1565811217'),
(222, 7, 'wp_user-settings', 'mfold=o'),
(223, 7, 'wp_user-settings-time', '1565795222'),
(228, 9, 'nickname', 'tecnico2'),
(229, 9, 'first_name', 'Tecnico 2'),
(230, 9, 'last_name', ''),
(231, 9, 'description', ''),
(232, 9, 'rich_editing', 'true'),
(233, 9, 'syntax_highlighting', 'true'),
(234, 9, 'comment_shortcuts', 'false'),
(235, 9, 'admin_color', 'fresh'),
(236, 9, 'use_ssl', '0'),
(237, 9, 'show_admin_bar_front', 'true'),
(238, 9, 'locale', ''),
(239, 9, 'wp_capabilities', 'a:1:{s:6:\"author\";b:1;}'),
(240, 9, 'wp_user_level', '2'),
(241, 9, 'dismissed_wp_pointers', 'wp496_privacy'),
(242, 9, 'celular', ''),
(245, 9, 'wp_dashboard_quick_press_last_post_id', '114'),
(247, 10, 'first_name', 'Andreína'),
(248, 10, 'last_name', 'Rodríguez'),
(249, 10, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(250, 10, 'wp_user_level', '0'),
(251, 10, 'prefijo_cedula', 'V'),
(252, 10, 'cedula', '17089721'),
(253, 10, 'celular', '04147777777'),
(256, 11, 'first_name', 'María'),
(257, 11, 'last_name', 'Rincon'),
(258, 11, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(259, 11, 'wp_user_level', '0'),
(260, 11, 'prefijo_cedula', 'V'),
(261, 11, 'cedula', '17089722'),
(262, 11, 'celular', '04246870849'),
(264, 3, 'celular', ''),
(266, 12, 'first_name', 'Antonio'),
(267, 12, 'last_name', 'Rivero'),
(268, 12, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(269, 12, 'wp_user_level', '0'),
(270, 12, 'prefijo_cedula', 'V'),
(271, 12, 'cedula', '15287444'),
(272, 12, 'celular', '04143669988'),
(273, 2, 'session_tokens', 'a:1:{s:64:\"f6b8a9301f45b3b09c23f18730ee537bbe51c4ee13c4d3ab7a88c34285821a08\";a:4:{s:10:\"expiration\";i:1565984162;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:102:\"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\";s:5:\"login\";i:1565811362;}}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$B.1zGQfWqe0xWrCWShOq.QbhGyqCjz.', 'admin', 'christianh_meier@hotmail.com', '', '2017-08-14 10:54:28', '', 0, 'admin'),
(2, 'pperez', '$P$B8jWOW4k1X8Sm3kn58NRXSpJ49NZnR0', 'pperez', 'maracunator@yahoo.com', '', '0000-00-00 00:00:00', '', 0, 'pperez'),
(3, 'mgonzalez', '$P$Bp4Pr9qKB1/cY0/ckkdesRDQ3FNN/d.', 'mgonzalez', 'mariagonzalez@gmail.com', '', '2019-07-01 21:15:34', '', 0, 'Maria Gonzalez'),
(4, 'ajimenez', '$P$BeXEmm/Gb69ClOXhoOykVF/cctwtLR/', 'ajimenez', 'dacostaelecom.2016@gmail.com', '', '2019-07-01 21:21:30', '1562016090:$P$BX4c3P7dD3N6NUsPWgr9Q1ivjRo3.H1', 0, 'Alfredo Jimenez'),
(5, 'dacosta', '$P$B6lDxjMsfWZDk7a1896oQiLCXi3LMc0', 'dacosta', 'dacostaelecom.2016@gmail.com', '', '0000-00-00 00:00:00', '', 0, 'dacosta'),
(6, 'coordinador', '$P$BXvpld27Qu5zE7XJTcZYxlR.txlzPJ.', 'coordinador', 'coordinador@gmail.com', '', '2019-08-03 18:24:41', '1564856684:$P$BM.QKrXvoDVcCk7k3XOIZnvvFtvzo51', 0, 'Coordinador'),
(7, 'tecnico', '$P$BvEY5bihPGFCgErg4BDbtej.GZBUUA1', 'tecnico', 'tecnico@gmail.com', '', '2019-08-06 15:00:39', '1565103655:$P$B6eIKrv1qxZQHdFaU622l3ApWNu5T81', 0, 'Técnico'),
(8, 'receptor', '$P$BUJMc9oZVikikDubatTJMj1u5HcDd30', 'receptor', 'receptor@gmail.com', '', '2019-08-06 15:21:39', '1565104904:$P$BdnZiXyJmCaAGlBOPSEanbtX8ZxHHA.', 0, 'Receptor'),
(9, 'tecnico2', '$P$BL.Lv4p93GWeGyd52SuvbdHA2p85F/0', 'tecnico2', 'tecnico2@gmail.com', '', '2019-08-14 15:15:00', '1565795709:$P$BRaIuCzryg8IwX7D63lpTy8GqGJ61R/', 0, 'Tecnico 2'),
(10, 'arodriguez', '$P$BF5SPa8WPKS33A5xgh9o7WnXMqa26F1', 'arodriguez', 'andreina@gmail.com', '', '0000-00-00 00:00:00', '', 0, ''),
(11, 'mrincon', '$P$BzvWPZgw0zEoKixHWokA4mEM4yEGtJ0', 'mrincon', 'maria@gmail.com', '', '2019-08-14 10:24:19', '', 0, ''),
(12, 'arivero', '$P$B8hkBIgS5vORNFm/Au4zWcVbhR5dQV1', 'arivero', 'antoniorivero@gmail.com', '', '2019-08-14 13:34:58', '', 0, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indices de la tabla `wp_icsc_almacen`
--
ALTER TABLE `wp_icsc_almacen`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wp_icsc_categoria`
--
ALTER TABLE `wp_icsc_categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wp_icsc_equipo`
--
ALTER TABLE `wp_icsc_equipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_equipo_categoria1_idx` (`categoria_id`),
  ADD KEY `fk_equipo_marca_id1_idx` (`marca_id`);

--
-- Indices de la tabla `wp_icsc_equipo_movimientos`
--
ALTER TABLE `wp_icsc_equipo_movimientos`
  ADD PRIMARY KEY (`id`,`equipo_id`),
  ADD KEY `fk_equipo_movimientos_renglon1_idx` (`almacen_id`,`estante_id`,`renglon_id`),
  ADD KEY `fk_equipo_movimientos_equipo1_idx` (`equipo_id`);

--
-- Indices de la tabla `wp_icsc_equipo_notas`
--
ALTER TABLE `wp_icsc_equipo_notas`
  ADD PRIMARY KEY (`equipo_id`,`id`);

--
-- Indices de la tabla `wp_icsc_estante`
--
ALTER TABLE `wp_icsc_estante`
  ADD PRIMARY KEY (`almacen_id`,`id`);

--
-- Indices de la tabla `wp_icsc_marca`
--
ALTER TABLE `wp_icsc_marca`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wp_icsc_renglon`
--
ALTER TABLE `wp_icsc_renglon`
  ADD PRIMARY KEY (`almacen_id`,`estante_id`,`id`);

--
-- Indices de la tabla `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indices de la tabla `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indices de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indices de la tabla `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indices de la tabla `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indices de la tabla `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indices de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `wp_icsc_almacen`
--
ALTER TABLE `wp_icsc_almacen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `wp_icsc_categoria`
--
ALTER TABLE `wp_icsc_categoria`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `wp_icsc_equipo`
--
ALTER TABLE `wp_icsc_equipo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `wp_icsc_marca`
--
ALTER TABLE `wp_icsc_marca`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2541;

--
-- AUTO_INCREMENT de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT de la tabla `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=280;

--
-- AUTO_INCREMENT de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `wp_icsc_equipo`
--
ALTER TABLE `wp_icsc_equipo`
  ADD CONSTRAINT `fk_equipo_categoria1` FOREIGN KEY (`categoria_id`) REFERENCES `wp_icsc_categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_equipo_marca_id1` FOREIGN KEY (`marca_id`) REFERENCES `wp_icsc_marca` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `wp_icsc_equipo_movimientos`
--
ALTER TABLE `wp_icsc_equipo_movimientos`
  ADD CONSTRAINT `fk_equipo_movimientos_equipo1` FOREIGN KEY (`equipo_id`) REFERENCES `wp_icsc_equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_equipo_movimientos_renglon1` FOREIGN KEY (`almacen_id`,`estante_id`,`renglon_id`) REFERENCES `wp_icsc_renglon` (`almacen_id`, `estante_id`, `id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `wp_icsc_equipo_notas`
--
ALTER TABLE `wp_icsc_equipo_notas`
  ADD CONSTRAINT `fk_equipo_notas_equipo1` FOREIGN KEY (`equipo_id`) REFERENCES `wp_icsc_equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `wp_icsc_estante`
--
ALTER TABLE `wp_icsc_estante`
  ADD CONSTRAINT `fk_estante_almacen` FOREIGN KEY (`almacen_id`) REFERENCES `wp_icsc_almacen` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `wp_icsc_renglon`
--
ALTER TABLE `wp_icsc_renglon`
  ADD CONSTRAINT `fk_renglon_estante1` FOREIGN KEY (`almacen_id`,`estante_id`) REFERENCES `wp_icsc_estante` (`almacen_id`, `id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
